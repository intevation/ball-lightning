import Vue from "vue";
import Vuex from "vuex";

import alarmmanagement from "./modules/alarmmanagement";
import application from "./modules/application";
import geosearch from "./modules/geosearch";
import map from "./modules/map";
import usermanagement from "./modules/usermanagement";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    alarmmanagement,
    application,
    geosearch,
    map,
    usermanagement
  }
});
