const getInitialState = () => {
  return {
    alarmTemplatesFax: [],
    alarmTemplatesSms: [],
    alarmTemplatesMail: [],
    alarmTemplatesMqtt: []
  };
};

const state = getInitialState();
state.resetFn = getInitialState;

const mutations = {
  fetchTemplates(state) {
    fetch(window.apiprefix + "template/mail").then(response => {
      response.json()
      .then(data => {
        if (response.status !== 200){
          state.alarmTemplatesMail = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
          state.alarmTemplatesMail = data || [];
        }
      })
    });
    fetch(window.apiprefix + "template/fax").then(response => {
      response.json()
      .then(data => {
        if(response.status !== 200) {
          state.alarmTemplatesFax = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
          state.alarmTemplatesFax = data || [];
        }
      })
    });
    fetch(window.apiprefix + "template/mqtt").then(response => {
      response.json()
      .then(data => {
        if (response.status !== 200) {
          state.alarmTemplatesMqtt = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
            state.alarmTemplatesMqtt = data || [];
        }
      })
    });
    fetch(window.apiprefix + "template/sms").then(response => {
      response.json()
      .then(data => {
        if(response.status !== 200) {
          state.alarmTemplatesSms = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
          state.alarmTemplatesSms = data || [];
        }
      })
    });
  }
};

const actions = {
  saveAlarmSettings(ctx, settings) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "user/" + settings.id + "/alarmsettings", {
          method: "PUT",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(settings)
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            } else {
              reject({ statusText: response.statusText || response });
            }
          } else {
            resolve();
          }
        });
      });
    });
  },

  // alarmObj: {groupId, alarm}
  deleteGroupAlarm(ctx, alarmObj) {
    return new Promise((resolve, reject) => {
      fetch(`/group/${alarmObj.groupId}/alarm/${alarmObj.alarm.id}`,
        { method: "DELETE" })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText});
          } else {
            resolve();
          }
        });
      });
    });
  },

  deleteUserAlarm(ctx, alarmObj) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "user/" + alarmObj.userId + "/alarm/" + alarmObj.alarm.id,
        { method: "DELETE" })
      .then(response => {
        response.json()
        .then(data =>{
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  addUserAlarm(ctx, alarmObj) {
    return new Promise((resolve, reject) => {
      fetch(`/user/${alarmObj.userId}/alarm`, {
        method: "PUT",
        headers: {
        "Content-Type": "application/json"
        },
        body: JSON.stringify(alarmObj.alarm)
      })
      .then(response => {
        response.json()
        .then(data =>{
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText});
          } else {
            resolve();
          }
        });
      });
    });
  },

  saveGroupAlarm(ctx, alarmObj) {
    return new Promise((resolve, reject) => {
      fetch(`/group/${alarmObj.groupId}/alarm`, {
        method: alarmObj.alarm.id ? "PUT": "POST",
        headers: {
        "Content-Type": "application/json"
        },
        body: JSON.stringify(alarmObj.alarm)
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  saveGroupAlarmArea(ctx, areaObj) {
    return new Promise((resolve, reject) => {
      fetch(`group/${areaObj.groupId}/alarm/${areaObj.alarmId}/area`, {
        method: areaObj.area.id ? "PUT": "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(areaObj.area)
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  deleteGroupAlarmArea(ctx, areaObj) {
    return new Promise((resolve, reject) => {
      fetch(
        window.apiprefix + "group/" + areaObj.groupId +
        "/alarm/" + areaObj.alarmId + "/area/" +
        areaObj.areaId, {
        method: "DELETE"})
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  }
};

export default {
  namespaced: true,
  actions,
  state,
  mutations
};
