const getInitialState = () => {
  return {
    groups: {},
    grouproles: [],
    currentGroup: undefined,
    currentChildren: [],
    userList: [],
    userSubmitFields: [ 'name', 'firstName', 'lastName', 'company', 'division',
        'email', 'street', 'zip', 'city','phonenumber','mobilenumber',
        'annotation']
  };
};

const state = getInitialState();
state.resetFn = getInitialState;

const groupGetUpdate = async function(groups, groupId) {
  return new Promise(resolve =>
    fetch(window.apiprefix + "group/"+ groupId)
    .then(response => {
      response.json()
      .then (data => {
        if (response.status !== 200) {
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
          resolve(groups);
        } else {
          groups[data.group.id] = data;
          if (data.children) {
            data.children.map(child => groups[child.id] = { group: child });
          }
          groups[data.group.id].users = data.users ? data.users : [];
          resolve(groups);
        }
      })
    })
  );
};

const mutations = {
  addGroup(state) {
    const grp = {
      parent: state.currentGroup.group.id,
      name: '',
      themeId: state.currentGroup.group.themeId,
      maxAlarmArea: state.currentGroup.group.maxAlarmArea || 0,
      roleId: state.grouproles.length ? state.grouproles[0].id : null
    };
    state.currentGroup = { group: grp };
  },

  fetchRoles(state){
    fetch(window.apiprefix + "role", { method: "GET" })
    .then(response => {
      response.json()
      .then(data => {
        if (response.status !== 200) {
          state.grouproles = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
          state.grouproles = Array.isArray(data) ? data: [];
        }
      });
    });
  }
};

const actions = {
  activateGroup(ctx, groupId) {
    return new Promise((resolve) => {
      groupGetUpdate(ctx.state.groups, groupId).then(grp => {
        ctx.state.groups = grp;
        ctx.state.currentGroup = ctx.state.groups[groupId];
        ctx.state.userList = ctx.state.currentGroup.users || [];
        ctx.state.currentChildren = ctx.state.currentGroup.children || [];
        resolve(ctx.state.currentGroup);
      });
    });
  },
  saveUser(ctx, user) {
    return new Promise((resolve, reject) =>{
      fetch(window.apiprefix + "user", {
        method: user.id ? "PUT" : "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
      }).then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject({
              statusText: data.message || response.statusText });
          } else {
            // TODO update state/application/user.userInfo, saved user is currentUser
            ctx.dispatch("activateGroup", ctx.state.currentGroup.group.id)
            .then(() => resolve());
          }
        });
      });
    });
  },

  deleteUser(ctx, user) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "user/"+ user.id, { method: "DELETE" })
      .then(response => {
        response.json()
        .then (data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({statusText: response.statusText || response });
          } else {
            ctx.dispatch("activateGroup", ctx.state.currentGroup.group.id)
            .then(()=> resolve());
          }
        });
      });
    });
  },

  saveGroup(ctx, group) {
    if (typeof group.maxAlarmArea === 'string') {
      group.maxAlarmArea = parseInt(group.maxAlarmArea, 10);
    }
    return new Promise ((resolve, reject) => {
      fetch(window.apiprefix + "group", {
        method: group.id ? "PUT": "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(group)
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            ctx.dispatch("activateGroup", data.id).then(()=> resolve());
          }
        });
      });
    });
  },

  saveGroupSettings(ctx, settings) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "group/" + settings.id + "/settings", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(settings)
      }).then(response => {
        response.json()
        .then (data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject({ statusText: data.message || response.statusText });
          } else {
            ctx.dispatch("activateGroup", settings.id).then(() => resolve());
          }
        });
      });
    });
  },

  deleteGroup(ctx, group) {
    return new Promise((resolve, reject) => {
      const parent = group.parent;
      fetch(window.apiprefix + "group/" + group.id, { method: "DELETE" })
      .then(response => {
        response.json()
        .then(data =>{
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject({ statusText: data.message || response.statusText });
          } else {
            ctx.dispatch("activateGroup", parent).then(() => resolve());
          }
        });
      });
    });
  },

  saveGroupStrokeArea(ctx, newStrokeArea) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "group/" + newStrokeArea.groupId + '/strokearea', {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(newStrokeArea)
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  changeRolePermission(ctx, perm) {
    if (!perm.role) {
      return Promise.reject({ statusText: 'No role selected'});
    }
    return new Promise((resolve, reject) => {
      const callback = response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      };
      if (perm.newState) {
        fetch(`/role/${perm.role.id}/permission`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(perm.permission)
        }).then(callback);
      } else {
        fetch(`/role/${perm.role.id}/permission/${perm.permission.id}`, {
          method: "DELETE",
        }).then(callback);
      }
    });
  },

  saveRole(ctx, role) {
    return new Promise((resolve, reject) => {
      const url = role.id ? `/role/${role.id}`: `role`;
      const method = role.id ? `PUT` : `POST`;
      fetch(url, {
        method: method,
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(role)
      }).then(response => {
        response.json()
        .then (data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  deleteRole(ctx, role) {
    return new Promise((resolve, reject) => {
      fetch(`/role/${role.id}`, {
        method: "DELETE"
      })
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  saveLayer(ctx, layer) {
    return new Promise((resolve, reject) => {
      fetch(`/layer`, {
        method: layer.id ? "PUT" : "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(layer)
      }).then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  deleteLayer(ctx, layer) {
    return new Promise((resolve, reject) => {
      fetch(`/layer/${layer.id}`, { method: "DELETE"})
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve()
          }
        });
      });
    });
  },
  saveGroupLayer(ctx, layerObj) {
    return new Promise((resolve, reject) => {
      fetch(`/group/${layerObj.groupId}/maplayer`, {
        method: layerObj.layer.id ? "PUT" : "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(layerObj.layer)
      }).then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            resolve();
          }
        });
      });
    });
  },

  deleteGroupLayer(ctx, layerObj) {
    return new Promise((resolve, reject) => {
      fetch(`/group/${layerObj.groupId}/maplayer/${layerObj.layer.id}`,
        { method: "DELETE"} )
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText});
          } else {
            resolve();
          }
        });
      });
    });
  }
}

export default {
  namespaced: true,
  actions,
  state,
  mutations
};
