import Vuetify from '../../plugins/vuetify';

const emptyUserSettings = {
  sound: false,
  minZoom: 2,
  maxZoom: 8,
  liveBlids: false,
  animation: false,
  maxDisplayTime: 0,
  archiveDays: 0,
  statisticWindowed: false
};

const profileProperties = [
  "playSound",
  "toolbar",
  "layout",
  "layerState"
];

const getInitialState = () => {
  return {
    sidebar: true,
    toolbar: "vertical",
    playSound: false,
    user: {
      userInfo: {},
      strokeArea: [], // Lon/Lat!
      userLayers: []
    },
    layerState: [],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: {
            base: "#000999",
            darken1: "#008888",
            lighten1: "#a0dcdc"
          },
          secondary: "#8585F4",
          accent: "#82B1FF",
          error: "#444444",
          info: "#33b5e5",
          success: "#00C851",
          warning: "#ffbb33"
        },
        dark: {
          primary: {
            base: "#009999",
            darken1: "#008888",
            lighten1: "#a0dcdc"
          },
          secondary: "#8585F4",
          accent: "#82B1FF",
          error: "#444444",
          info: "#33b5e5",
          success: "#00C851",
          warning: "#ffbb33"
        }
      }
    },
    logo: {
      mimetype: "image/png",
      source: "base64",
      data: `iVBORw0KGgoAAAANSUhEUgAAAJMAAAAjCAYAAACQLzhgAAAABmJLR0QA/wD/AP+gvaeTAAAACXBI
  WXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4wkFDCk67Zru8gAACgNJREFUeNrtm3uUTdcdxz/7PgYz
  XvWIegalgqap1YoWKQkjpY+koUlMMFlNO/diPKZLEFmUYRCPIMbMnIaK0uVREVJSlUZidVGatkkt
  UqKJYbyHGWYw19x7z+kfZ9+6c+859565rqyRdb5rnWXuOfv89v7t89u/x3dvYMOGDRs2bNiwYcOG
  DRs27j2IuC0UBaAJ8C3gAeA+wA3cBEqBU8B/gQsI4SMrS0oWUFRUD2hn0I8KnMbjCYT1kyLbOhLQ
  QwPO4vH4pCyzfkMoIxAoY/z4eLo7gA6Ay6TFRbzeSjQt1F4AbYH6CX6PK1RVlTN5ckhWeyDFoF1Q
  zl/QYMxGYyjF6732/3GGo39/GD069E4v4GtAU/m0ArgAnAQ+B65QXR1gwgTDwbtM1SoqAiG6AlOB
  p4CvxPg4QaAUTcumX7832b9fNyb4JrAfcEa0vwY8BJSE3esEfAikJfARqoGBwCH5uwfwN5MPAbAT
  l+spIGAqcfhw5ALaZzImDcikXr0N+Hyhe25gK9A7QWOaRYMGefLvBsAuoKdBOz/wDAUF2xk3LsI9
  iBQ0bTPwvbBxZuNyFeL3Gy2WdGAK0D/GItDkHH9KSsqPpQOJgsPEkARCZAAHgV8AzeJ4MSfwVaAl
  TqdRH5GXMPGSjgQvM3lm10CgG926mWs0ZIgAngcaxpFlRd9E9TDrMwWYgdPZMEbEEWFzWhOtWoGi
  pAKvATuBwXG8qQDqAR1jtYvuqKAAhBgGvC6NyMxStXs4vDcCMsjJMV4gLVoAtAaG12Edvg2MpKio
  9m/m5jqAPGCcSXTSEvnG0YKczlQgF0iNeBIAtgBvAKflu21luBoA9LtLk1YF/EuGUjP4ZXyvDZ5G
  iCVAedSTefOQhtQ6iXocBq7GaXPaMK8xdwQvIsRbwGXLo3C7kbnRLw084VVgObAbKJNeuZMM248B
  nWtnTHq+0cPg/ttAJl5vIEzho3TsuIfp0xcjRHMghYsXk21MZ3E4HicQuBGzVWQ+EB+dgSGsWLGZ
  SZMi845UGeJEknRQgRxUdW/sMkKD3NzayO0CeFCU+Xg81qxw5UqAoQZ5oAq8iKquZuzY8PsfMWXK
  Nrp2DRUjpdaMqXdvgDYyPkYiDXCTlhbg+vXbd4uLwevVK5G7iZoKJgMO4Hnq198mPZuO+fORnvbB
  e0AHAYwHNuB0niIYtPCGEDL3MZLVWD6vaZhLloSMrTjehN7GiRMAPvliVEoK7GDp0kdQFCcu15eB
  GnkE6EHfvrfvNG/ulEWH+x7RoTXwKwoKakOp3DQxpjyEWIaidEwkF6s5gKtXAU4AlSadpQN7gd2s
  WpWOorgYMOBemPDrUi8jb/scmZl6OGvdGsmlDY6gPT6qI3pcMLk/BviGjBCx4fNpwMcmyXV9YBJw
  GCEKUJSuFBVZDvVG7qUEeBf4WYx3BgOPAvvJyHiFjIw9rFsX4MCBuzGBLVHVJSiKWVJUDizC47kR
  h4daC8wzqGBHAK8AV5g9WwCjgcZhz48De2TSeifhKBtFeTJGdbyGadMOywVt1qYAmAC0jHjWFJhO
  r15j4o4kJwcKC3cDZyQpalbtjgXGIMQmFGUpmnYMr1ernTGdOROgXbuZQB+ZcMXilr4vq7h3ycyc
  zKhRx6NItDtHE8AbswLS+ZIbceTsBn4uk9Zw3A8MJT9/A9ACeDbiA/6OxFj5SGP6aZwEfS8pKYfj
  yPkMKARmGhQHTwJ9uU3cmvSkgqadQ4iZwG8wJ3ZDnvsF4FmEWIWizGPjxko++MAizzR3LqjqcXTW
  +z8WJsoJ/AB4H6dzQEK8xxeDMmCzyRxk4na7gZ9ErNZSYFMd0iHknU4aPGsAvGxSPNWEHg7XA9no
  uxHxkIa+E/I2I0e2MkttHKZVR1nZP9Ep9vkWeYzWwHqE6MyIEcmcwKD8qJdMrismBYMRfm/CR/UF
  HpaeK3xOtqNpp5Okx9UYOpQC1RY5povAEhOdH0XThlmiNDweFZ2Y7g9sA25Z6HsgUEhGRj3rxgTw
  0kvg8ZShaS9L3mky8AmxWdH2wGTS00USjakYIb6OvnFrdD2MppVblPWpzAcjkQosBb4Tdq8KWENl
  ZTKYfhV4JoYO7YE/c9nCmr10CWCDTKKjKElghiQbsWBQ4PUeQYgR6Iz6axYcx4+Ax1i2zFICbuQS
  S5k/fwXNmxfJ5Dtb/mv0frr8ODeSZEwaQvjJyvLfsaTKyiCNGq0FnjAYe5+I339NchUXwOPxJ0VS
  MFiJ05knQ3AkhfFgLTy1TpRmZWnAUQoLJ+FwzJV541igu4GXcwPDSE39k3XPFIkZM8DjuYXXu0ta
  50SMd92bWF4ZXzSmTAH9FMBRC6H1dXw+f53UQy9ydgHvW6J8rGLsWPB4LnPpUr5cXFtNWrY1CqXR
  nU6bBsuXh/ZwjC1ZP0ezQ/I3RmW4r45zTuvjhOtjwJ6obZa6hGvXbkmqo6rW7776KixaZP585kw4
  ePA6sN3Ey1VYowY6dWqAELPJzz8iXf1ZVNXPhQt6pbd4MTRs6AZGSj4iEkdMjCxRpKCq3VGUeJN2
  luLiqyxYELtVbi7MmvUHYJoBXxOqmNahqhVJ/vwdUZSecdpUEAyWWKJXpk4FRTkAvAmMsjyK+vUh
  Le0BIAtF2QP8A/2woMq+fXDokC7b5Woqw53DYH4OoaqalZzJKUvkqeh7VudwOE7Qps15CgtvyhD2
  kEzKIzvyA6vx+YJJ/AgdgL9bKJlfoEWLN+JKO3sWSdj9UVZvUSkusCnJ+2gOYLWFdltlom4t6X/n
  nSDDhi0Efoh+eNEC4yVCqcgEIEfmtsW4XJ8zaFApgwYF5CLrg75PG4kSYIfRKdV4Cbhbknr3W6xY
  FITYyaRJ4HAk82OIJLWRNOdplQ4d1gLPGfAyb0ljSzaSqwPAjh0wdOgnCLEG/bRkIkhDP83Z00Lb
  G7KqP4eqJilRM+5kDjClxrnuuoq8PKR7/zDiyU3gt7z33r1z8C87WwNW3KUFEI6TwNPAdjwerOVM
  egK9Bf3MSxfpEo2OqAbQtzJ2AkXAMYMzNX7gPNFnwCuIPuwWQN/ITE2IPoCbYaSfX8pyhxGGNftT
  VR8Ox1rgu2GLah/wMVu21Cw49BzwXMQcRPYZQqnUORGUh/WrIUS4LA2oivIIgUAobC+WeaAwnJua
  7c9JWqG35LgamLznkznwRmAd1dVlZv+ZwNiYvN5qFiz4Nc2azZYu8D4ZQxvLkKCiU/AlwHmqq/2G
  HehK/xuz03maFoj4/RlCdL8T9oU5c0KyjiJEl5j9jRsHRUUbgL+ETeQ1Kipq0gHr1sHEiavkgonu
  89atcJ2rcTieIPFDdSrl5ZqUVYXTmR4hK8jatcbko6KsRN+3M56b0OFBvx88nhLy80fjdjuls2iF
  vi+ZJm3CL8nLU0AZJ0+qLFyIDRs2bNiwYcOGDRs2bNj4MuJ/L38JGB6/q28AAAAASUVORK5CYII=`
    },
    title: "Kugelblitz",
    font: "",
    layout: [],
    loggedIn: false,
    UserPermissions: [],
    UserActivities: {
      roles: {},
      permissions: {},
      activities: {}
    },
    settings: emptyUserSettings,
    hostTheme: {},
    version: ""
  };
};

const state = getInitialState();
state.resetFn = getInitialState;

const mutations = {
  sidebar(state, visibile) {
    state.sidebar = visibile;
  },
  toolbar(state, direction) {
    state.toolbar = direction;
  },
  theme(state, theme) {
    state.theme = theme;
    Vuetify.framework.theme.themes.light = theme.themes.light;
    Vuetify.framework.theme.themes.dark = theme.themes.dark;
    Vuetify.framework.theme.dark = theme.dark;
  },
  title(state, title) {
    state.title = title;
  },
  logo(state, logo) {
    state.logo = logo;
  },
  font(state, font) {
    state.font = font;
  },
  layout(state, layout) {
    state.layout = layout;
  },
  logIn(state, json) {
    if (json) {
      state.UserPermissions = [];
      for (const role in json.UserActivities.roles) {
        state.UserPermissions = state.UserPermissions.concat(
          json.UserActivities.roles[role].permissions
        );
      }
      if (state.UserPermissions.includes(0)) {
        fetch(window.apiprefix + "version")
          .then(response => {
            response.json()
            .then(data => {
              if (response.status !== 200) {
                state.version = "unknown";
                if (response.status !== 401) {
                  throw new Error(data.message || response.statusText);
                }
              } else {
                state.version = data;
              }
            });
          });
        state.UserActivities = json.UserActivities;
        state.user.userInfo = json.User;
        if (json.strokeArea) {
          state.user.strokeArea = JSON.parse(json.strokeArea).coordinates;
        }
        state.settings = {...emptyUserSettings, ...json.settings};
        if (json.profile) {
          for (const prop of profileProperties) {
            if (json.profile[prop] !== undefined) {
              state[prop] = json.profile[prop];
            }
          }
        }
      // TODO: better/centralized method of chosing "display name"
        let name = json.User.name;
        if (json.User.firstName) {
          name = json.User.firstName;
          if (json.User.lastName) {
            name += " " + json.User.lastName;
          }
        }
        if (json.User.email) {
          name += " (" + json.User.email + ")";
        }
        state.user.displayName = name;
        if (json.layers) {
          state.user.userLayers = json.layers;
        } else {
          state.user.userLayers = [];
        }
        state.loggedIn = true;
      } else {
        const resetFn = state.resetFn;
        state = resetFn();
        state.resetFn = resetFn;
      }
    } else {
      const resetFn = state.resetFn;
      state = resetFn();
      state.resetFn = resetFn;
    }
  },

  hostTheme(state, theme) {
    state.hostTheme = theme;
    state.theme = theme.theme;
  },
  version(state, version) {
    state.version = version;
  },
  toggleSound(state) {
    if (!state.settings.sound) {
      state.playSound = false;
    } else {
      state.playSound = !state.playSound;
    }
  },
  toggleLayerState(state, stateObj) {
    const oldstateIdx = state.layerState.findIndex(os => os.id === stateObj.id);
    if (oldstateIdx > -1) {
      state.layerState[oldstateIdx] = stateObj;
    } else {
      state.layerState.push(stateObj);
    }
  }
};

const actions = {
  logOut(ctx){
      const logoutFn = () => {
        ctx.state.loggedIn = false;

        // try to reset all states to initial value

        let resetFn = ctx.rootState.alarmmanagement.resetFn;
        if (resetFn) {
          ctx.rootState.alarmmanagement = resetFn();
          ctx.rootState.alarmmanagement.resetFn = resetFn;
        }

        resetFn = ctx.rootState.geosearch.resetFn;
        if (resetFn) {
          ctx.rootState.geosearch = resetFn();
          ctx.rootState.geosearch = resetFn;
        }

        resetFn = ctx.rootState.map.resetFn;
        if (resetFn) {
          ctx.rootState.map = resetFn();
          ctx.rootState.map.resetFn = resetFn;
        }

        resetFn = ctx.rootState.usermanagement.resetFn;
        if (resetFn) {
          ctx.rootState.usermanagement = resetFn();
          ctx.rootState.usermanagement.resetFn = resetFn;
        }

        resetFn = ctx.state.resetFn;
        if (resetFn) {
          ctx.state = resetFn();
          ctx.state.resetFn = resetFn;
        }
        fetch(window.apiprefix + "auth/logout");
      };
      const saveProfileFn = () => {
        const profile = {};
        for (const prop of profileProperties) {
          profile[prop] = state[prop];
        }
        fetch(`user/${state.user.userInfo.id}/profile`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(profile)
        });
        // .then(response => {
        //   response.json()
        //   .then(data => {
        //     if (response.status !== 200) {
        //       // TODO some error information? But logging out should not stop,
        //       // so this shouldn't throw an error
        //       console.error(data.message);
        //     }
        //   });
        // });
      };
      if (state.hostTheme.theme && state.hostTheme.theme.themes) {
        Vuetify.framework.theme.themes.light = state.hostTheme.theme.themes.light;
        Vuetify.framework.theme.themes.dark = state.hostTheme.theme.themes.dark;
        Vuetify.framework.theme.dark = state.hostTheme.theme.dark;
      }
      if (state.loggedIn){
        // ensuring to logout even if there is some error (e.g. in the profile)
        try {
          saveProfileFn().then(logoutFn, logoutFn);
        } catch {
          logoutFn();
        }
      } else {
        logoutFn();
      }
  },
  getStrokeArea(ctx) {
    return new Promise ((resolve, reject) => {
      fetch(window.apiprefix + "group/" + ctx.state.user.userInfo.groupId + "/strokearea")
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error( data.message || response.statusText );
            }
            reject ({
              statusText: data.message || response.statusText });
          } else {
            if (data && data.geom) {
              ctx.state.user.strokeArea = JSON.parse(data.geom).coordinates;
            } else {
              ctx.state.user.strokeArea = [];
            }
            resolve(ctx.state.user.strokeArea);
          }
        });
      });
    });
  },
  reloadUserLayers(ctx) {
    return new Promise((resolve, reject) => {
      fetch(window.apiprefix + "group/" + ctx.state.user.userInfo.groupId + "/maplayer")
      .then(response => {
        response.json()
        .then (data => {
          if (response.status !== 200) {
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            reject ({ statusText: data.message || response.statusText });
          } else {
            ctx.state.user.userLayers = data;
            resolve();
          }
        })
      });
    });
  }
};

const getters = {
  // TODO: better doc
  // usage: this.$store.getters['application/permCheck'](['permission', 'permission2']);
  // returns true if any one of the permissions/activity is met
  permCheck: state => permArray =>
    state.UserPermissions.some(pkey =>
      permArray.includes(state.UserActivities.permissions[pkey].name)
    ),
  activityCheck: state => activityArray =>
    state.UserPermissions.some(pkey =>
      state.UserActivities.permissions[pkey].activities.some(akey =>
        activityArray.includes(state.UserActivities.activities[akey].name)
      )
    )
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
