const getInitialState = () => {
  return {
    hasResult: false,
    display: '',
    latlng: [0,0],
    raw: {},
    resultOptions: [],
    geocodeURL: '' // TODO: replace with proper URL
  };
};
const state = getInitialState();
state.resetFn = getInitialState;

const mutations = {
  // TODO: still uses hardcode dummy data, always returning "Hanover"
  search(state) { //, searchString) {
    fetch("geocodeDummy.json")
    // state.geocodeURL, {
      // method: "POST",
      // headers: {
      //   "Content-Type": "application/json"
      // },
      // body: JSON.stringify({
      //   address: searchString,
      //   sensor: false
      // })
    .then(response => {
      response.json()
      .then(data => {
        if (response.status !== 200) {
          state.resultOptions = [];
          if (response.status !== 401) {
            throw new Error(data.message || response.statusText);
          }
        } else {
          state.resultOptions = data || [];
        }
      });
    });
  },

  select(state, geocodeObject) {
    if (geocodeObject) {
      state.display = geocodeObject.formatted_address;
      state.latlng = [
        geocodeObject.geometry.location.lat,
        geocodeObject.geometry.location.lng];
      state.raw= geocodeObject;
      state.hasResult = (state.lat !== null && state.lng !== null);
    } else {
      state.display = '';
      state.latlng = [null, null];
      state.raw = {};
      state.hasResult = false;
    }
  }
};

export default {
  namespaced: true,
  state,
  mutations
};
