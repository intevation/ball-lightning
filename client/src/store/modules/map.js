import * as L from "leaflet";
import "../../../node_modules/leaflet-draw/dist/leaflet.draw";
import circle from "turf-circle";
import difference from "turf-difference";
// import intersect from "turf-intersect";
import "../../plugins/leaflet-google";
import "../../plugins/leaflet-wmst";

/**
latlon <-> lonlat flipping for coordinate pairs (independent of depth)
*/
const flipCoordinates = function (arr) {
  if (arr.length === 2 && typeof arr[0] === "number" && typeof arr[1] === "number") {
    return [arr[1], arr[0]];
  }
  return arr.map(sub => flipCoordinates(sub));
};

const geoJSONMultiToLeafletPoly = function(layer){
  const fGroup = L.featureGroup();
  if (layer.geom && layer.geom.type === "MultiPolygon") {
    for (const coords of layer.geom.coordinates) {
      fGroup.addLayer(L.polygon(flipCoordinates(coords)));
    }
  }
  return fGroup;
};

const getInitialState = () => { return {
    statisticsVisible: false,
    archiveVisible: false,
    archive: {
      startTime: new Date().toLocaleTimeString("de", { hour12: false }),
      endTime: new Date().toLocaleTimeString("de", { hour12: false }),
      startDate: new Date(new Date().setDate(new Date().getDate() - 1)),
      endDate: new Date()
    },
    timeRange: {
      selectedRange: [
        new Date(new Date().setDate(new Date().getDate() - 1)).getTime(),
        new Date().getTime()
      ],
      start: new Date(new Date().setDate(new Date().getDate() - 1)).getTime(),
      end: new Date().getTime(),
      currentTimeTick: new Date(
        new Date().setDate(new Date().getDate() - 1)
      ).getTime() // the current time slice to be displayed
    },
    leaflet: {
      LMap: {},
      LMapParams: {
        minZoom: 5,
        maxZoom: 14,
        center: [51.5, 8.5], // lat,lon
        zoom: 4,
        zoomControl: false,
        zoomAnimation: false,
        maxBoundsViscosity: 0.5
      },
      maxBounds: [],
      layerswitcher: false,
      mapruler: false,
      availableLayers: [],
      embedded: {
        LMap: {},
        LFeatureGroup: {},
        LFeatureStyle: {fillColor: '#FFCCFF'},
        LTempFeatureStyle: {fillColor: '#FFCCCC'},
        LDraw: {
          featureGroup: null,
          control: null,
          drawEvent: null,
          editing: false
        },
        LDrawnFeatures: [],
        LTempFeatures: [],
        LTempFeatureGroup: null,
      }
    },
    leafletTime: {
      // the current time slice to be displayed
      currentTimeTick: new Date(
        new Date().setDate(new Date().getDate() - 1)).getTime(),
      tickMinutes: 30, // minutes per tick. Should fit into 60 % x = 0 for nice display
      totalTicks: 48, // total amount of ticks,
      maxFPS: 1 // limit speed for playback
    }
  };
};

const state = getInitialState();
state.resetFn = getInitialState;

const mutations = {
  archiveVisible(state, value) {
    state.archiveVisible = value;
  },
  startTime(state, value) {
    state.archive.startTime = value;
    let { hour, min } = value.split(":");
    let tmp = state.archive.endDate;
    tmp.setHours(hour);
    tmp.setMinutes(min);
    state.timeRange.start = tmp;
  },
  endTime(state, value) {
    state.archive.endTime = value;
    let { hour, min } = value.split(":");
    let tmp = state.archive.endDate;
    tmp.setHours(hour);
    tmp.setMinutes(min);
    state.timeRange.end = tmp;
  },
  startDate(state, value) {
    state.archive.startDate = value;
    let [hour, min] = state.archive.startTime.split(":");
    value.setHours(hour);
    value.setMinutes(min);
    state.timeRange.start = value;
  },
  endDate(state, value) {
    state.archive.endDate = value;
    let [hour, min] = state.archive.startTime.split(":");
    value.setHours(hour);
    value.setMinutes(min);
    if (state.timeRange.currentTimeTick > value.getTime()) {
      state.timeRange.currentTimeTick = state.timeRange.start;
    }
    state.timeRange.end = value;
  },
  currentTimeTick(state, value) {
    state.timeRange.currentTimeTick = value;
  },
  selectedRange(state, value) {
    state.timeRange.selectedRange = value;
  },
  /**
   * Creates a Leaflet map bound to the given target element. It will be
   * available in the state as state.leaflet.LMap. On destruction
   * saveAndRemoveMap() should be called, to ensure proper saving of the map's
   * state
   * @param {*} state
   * @param {Object} params
   * @param {*} params.target DOM element
   * @param {Number} params.minZoom (optional)
   * @param {Number} params.maxZoom (optional)
   */
  createMap(state, params) {
    if (params.minZoom) {
      state.leaflet.LMapParams.minZoom = params.minZoom;
    }
    if (params.maxZoom) {
      state.leaflet.LMapParams.maxZoom = params.maxZoom;
    }
    state.leaflet.LMap = L.map(params.target, state.leaflet.LMapParams);
    // unset the z-index of 400 to avoid overshadowing information from google
    state.leaflet.LMap.getPane("mapPane").style.zIndex = "auto";
  },

  zoomMapLatLng(state, latlng) {
    state.leaflet.LMap.panTo(latlng);
  },
  zoomEmbeddedMapLatLng(state, latlng) {
    state.leaflet.embedded.LMap.panTo(latlng);
  },

  /**
   * Creates a map for forms, with a baselayer and featureGroups to show/edit.
   * This map does not listen to limits/states of the 'main' map.
   * @param {*} state
   * @param {*} target DOM- Element
   * TODO google layer is still hardcoded as state.availabelLayers[0]
   */
  createEmbeddedMap(state, target) {
    state.leaflet.embedded.LMap = L.map(target, state.leaflet.LMapParams);
    state.leaflet.embedded.LMap.getPane("mapPane").style.zIndex = "auto";
    const baseLayer = state.leaflet.availableLayers.find(l => l.isBaseLayer);
    if (baseLayer) {
      state.leaflet.embedded.LMap.addLayer(baseLayer.layer);
    }
    state.leaflet.embedded.LFeatureGroup = L.featureGroup();
    state.leaflet.embedded.LFeatureGroup.addTo(state.leaflet.embedded.LMap);
  },

  /**
   * Sets (reomving remaining old) Polygons given in featureObjects for the
   * embeddedMap. Expects an embeddedMap to currently exist.
   * See LFeatureStyle (above) for style definitions, st.LDrawnFeatures for a
   * list of the currently drawn features
   * @param {*} state
   * @param {*} featureObjects Objects as received from server backends; having
   * a geojson- geometry definition as the 'geom' property
   */
  setEmbeddedMapFeatures(state, featureObjects) {
    const st = state.leaflet.embedded;
    st.LFeatureGroup.clearLayers();
    st.LDrawnFeatures = [];
    for (const f in featureObjects ) {
      const object = {
        ...featureObjects[f],
        geojson: L.geoJSON(featureObjects[f].geom)
      };
      st.LDrawnFeatures.push(object);
      const layer = object.geojson.getLayers()[0];
      layer.setStyle(st.LFeatureStyle);
      st.LFeatureGroup.addLayer(layer);
    }
  },

  /**
   * Sets temporary leafletLayers (that have no connection to the server yet).
   * They differ from 'embeddedMapFeatures' in not being valid object
   * recognized by the server, and also not having "geojson" geometries.
   * However, they are ,e.g. editable by leaflet
   * @param {*} state
   * @param {*} leafletLayers Leaflet layers
   */
  setEmbeddedMapTempFeatures(state, leafletLayers) {
    const st = state.leaflet.embedded;
    if (!st.LTempFeatureGroup || !st.LMap.hasLayer(st.LTempFeatureGroup)) {
      st.LTempFeatureGroup = L.featureGroup();
      st.LTempFeatureGroup.addTo(st.LMap);
    }
    st.LTempFeatureGroup.clearLayers();
    st.LTempFeatures = [];
    if (leafletLayers.length) {
      for (const layer of leafletLayers) {
        layer.setStyle(st.LTempFeatureStyle);
        st.LTempFeatures.push(layer);
        st.LTempFeatureGroup.addLayer(layer);
      }
    }
  },

  zoomEmbeddedMap(state, jsonFeature) {
    if (!jsonFeature && state.leaflet.maxBounds.length) {
      state.leaflet.embedded.LMap.setMaxBounds(state.leaflet.maxBounds);
    }
    else if (jsonFeature) {
      const poly = jsonFeature.getBounds ? jsonFeature: L.geoJSON(jsonFeature);
      state.leaflet.embedded.LMap.fitBounds(poly.getBounds());
    }
  },

  /**
   * Set the main map's area of interest.
   * Adds a semi-transparent overlay to all areas outside the given polygon,
   * restricts the map bounds to the given coordinates, and
   * resticts the zoom to one level above the area of interest's extend
   * @param {*} state
   * @param {*} lonLatCoords Array of long/lat pairs describing a polygon
   */
  setAOI(state, lonLatCoords) {
    if (state.leaflet.AOILayer) {
      state.leaflet.LMap.removeLayer(state.leaflet.AOILayer);
    }
    const outerRing = [
      [90, -180],
      [90, 180],
      [-90, 180],
      [-90, -180],
      [90, -180]
    ];
    const latLonCoords = flipCoordinates(lonLatCoords);
    state.leaflet.AOILayer = new L.Polygon([outerRing, latLonCoords], {
      weight: 1,
      color: "#CCCCCC",
      fillOpacity: 0.6,
      interactive: false
    });
    const limitPoly = new L.Polygon(latLonCoords);
    state.leaflet.LMap.addLayer(state.leaflet.AOILayer);
    const bounds = limitPoly.getBounds();
    state.leaflet.LMap.setMaxBounds(bounds);
    state.leaflet.maxBounds = bounds;
  },

  /**
   * Enables/disables a layer.
   * Expects a payload containing:
   * layerObj: {
   *  layer: <L.Layer>,
   *  isActive: <Boolean>,
   *  isTimeLayer: <Boolean> | <Number> // number if the
   * },
   * newState: <Boolean>
   * the isActive toggle in the LayerObj will be modified
   */
  toggleLayer(state, payload) {
    if (!payload || !payload.layerObj) {
      return;
    }
    const currentState = state.leaflet.LMap.hasLayer(payload.layerObj.layer);
    if (payload.newState !== currentState) {
      if (!currentState) {
        payload.layerObj.layer.addTo(state.leaflet.LMap);
        if (payload.layerObj.isTimeLayer) {
          state.leaflet.LMap.on(
            "zoomstart",
            payload.layerObj.layer.invalidateCache
          );
        }
        if (payload.layerObj.layer.setZIndex && payload.layerObj.zIndex) {
          payload.layerObj.layer.setZIndex(payload.layerObj.zIndex);
        }
      } else {
        if (payload.layerObj.isTimeLayer) {
          state.leaflet.LMap.off("zoomstart", payload.layerObj.layer.invalidateCache);
        }
        state.leaflet.LMap.removeLayer(payload.layerObj.layer);
      }
      payload.layerObj.isActive = state.leaflet.LMap.hasLayer(
        payload.layerObj.layer
      );
    }
  },

  /**
   * Saves the currently active zoom and center, and cleanly removes all layers
   */
  saveAndRemoveMap(state) {
    state.leaflet.LMapParams = {
      ...state.leaflet.LMapParams,
      ...{
        center: state.leaflet.LMap.getCenter ? state.leaflet.LMap.getCenter(): [0,0],
        zoom: state.leaflet.LMap.getZoom ? state.leaflet.LMap.getZoom (): 0,
        minZoom: state.leaflet.LMap.getMinZoom ? state.leaflet.LMap.getMinZoom(): 1
      }
    };
    if (state.leaflet.LMap) {
      for (const layer of state.leaflet.availableLayers) {
        state.leaflet.LMap.removeLayer(layer.layer);
      }
    }
  },

  /**
   * opens/closes the blitze chart
   * @param forceShow if not set, the chart will be toggled
   */
  toggleDiagram(state, forceShow) {
    const idx = this.state.application.layout.findIndex(
      item => item.type === "chart"
    );
    if (idx > -1) {
      if (!forceShow) {
        this.state.application.layout.splice(idx, 1);
      }
    } else {
      this.state.application.layout.push({
        isResizable: true,
        uid: 2,
        x: 0,
        y: 0,
        w: 13,
        h: 8,
        i: 2,
        type: "chart",
        title: "Blitze pro Monat",
        closeable: true,
        collapsible: true,
        dragItem: true
      });
    }
  },

  /**
   * Displays/hides the LayerSwitcher
   * @param forceShow if set, the layer switcher be opened (not toggled)
   */
  toggleLayerSwitcher(state, forceShow) {
    const idx = this.state.application.layout.findIndex(
      item => item.type === "layerswitcher"
    );
    if (idx > -1) {
      if (!forceShow) {
        this.state.application.layout.splice(idx, 1);
      }
    } else {
      this.state.application.layout.push({
        isResizable: true,
        uid: 3,
        x: 0,
        y: 6,
        w: 9,
        minW: 9,
        minH: this.state.map.leaflet.availableLayers.length > 10 ? this.state.map.leaflet.availableLayers.length*2 : 20,
        h: this.state.map.leaflet.availableLayers.length > 10 ? this.state.map.leaflet.availableLayers.length*2 : 20,
        i: 3,
        type: "layerswitcher",
        title: "layers",
        closeable: true,
        collapsible: true
      });
    }
  },

  /**
   * Displays/hides the TimeSlider
   * @param forceShow if set, the timeSlider will be opened (not toggled)
   */
  toggleArchive(state, forceShow) {
    const idx = this.state.application.layout.findIndex(
      item => item.type === "archive"
    );
    if (idx > -1) {
      if (!forceShow) {
        this.state.application.layout.splice(idx, 1);
        this.state.map.archiveVisible = false;
      }
    } else {
      this.state.map.archiveVisible = true;
      this.state.application.layout.push({
        isResizable: true,
        uid: 4,
        x: 8,
        y: 0,
        w: 12,
        minW: 12,
        minH: 8,
        h: 8,
        i: 4,
        title: "archive",
        type: "archive",
        closeable: true,
        collapsible: true
      });
    }
  },

  /**
   * Displays/hides the MapRuler tool
   * @param forceShow if set, the mapruler will be opened (not toggled)
   */
  toggleStatistics(state, forceShow) {
    if (forceShow) {
      this.state.map.statisticsVisible = true;
    } else {
      this.state.map.statisticsVisible = !this.state.map.statisticsVisible;
    }
  },

  /**
   * Displays/hides the MapRuler tool
   * @param forceShow if set, the mapruler will be opened (not toggled)
   */
  toggleMapRuler(state, forceShow) {
    const idx = this.state.application.layout.findIndex(
      item => item.type === "mapruler"
    );
    if (idx > -1) {
      if (!forceShow) {
        this.state.application.layout.splice(idx, 1);
      }
    } else {
      this.state.application.layout.push({
        isResizable: true,
        uid: 5,
        x: 0,
        y: 6,
        w: 8,
        minW: 8,
        minH: 2,
        maxH: 2,
        h: 2,
        i: 5,
        type: "mapruler",
        dragItem: true,
        closeable: true,
        collapsible: false
      });
    }
  },

  /**
   * (re-) sets the time frames to the options
   * totalTicks: <Number> (default: 10),
   * tickMinutes <Number> (default: 18),
   * endDate <Date> (default: now)
   * Time frames will be snapped to round numbers ( 60 % x = 0)
   */
  setTimeFrame(state, options) {
    if (!options) {
      return;
    }
    const timeLayers = state.leaflet.availableLayers.filter(
      l => l.isTimeLayer && state.leaflet.LMap.hasLayer(l.layer)
    );
    for (const layer of timeLayers) {
      layer.layer.setParams(options);
    }
  }
};

const actions = {
  /**
   * activates the draw toolbar and returns a Promise which resolves with
   * the shape drawn (leaflet layer)
   */
  drawShape(ctx) {
    const st = ctx.state.leaflet.embedded;
    return new Promise(resolve => {
      if (!st.LDrawControl) {
        st.LDrawControl = new L.Control.Draw({
          draw: {
            marker: false,
            circlemarker: false,
            polyline: false
          },
          edit: false
        });
        st.LDrawControl.addTo(st.LMap);
      }
      st.LMap.once(L.Draw.Event.CREATED, e => {
        st.LDrawControl.remove();
        st.LDrawControl = null;
        resolve(e.layer);
      });
    });
  },

  /**
   * Activates the edit/draw toolbar editing a given single geojson
   * multiPolygon (e.g. alarm area)
   * @param {*} ctx, geoJSON (expecting a multiPolygon)
   */
  multiEditJSON(ctx, geoJSON) {
    const st = ctx.state.leaflet.embedded;
    if (st.LDraw.editing) {
      return;
    }
    st.LDraw.featureGroup = geoJSONMultiToLeafletPoly(geoJSON);
    st.LMap.addLayer(st.LDraw.featureGroup);
    st.LDraw.control = new L.Control.Draw({
      draw: {
        marker: false,
        circlemarker: false,
        polyline: false
      },
      edit: {
        featureGroup: st.LDraw.featureGroup,
        poly: {
          allowIntersection: false
        }
      }
    });
    st.LDraw.drawEvent = e => {
      st.LDraw.featureGroup.addLayer(e.layer);
    };
    st.LMap.on(L.Draw.Event.CREATED, st.LDraw.drawEvent);
    st.LDraw.control.addTo(st.LMap);
    st.LDraw.editing = true;
  },

  // TODO handler stub for "intersecting" on add of a shape.
  // Should be a handler of the edited layer, and aims to "cut" holes etc. Uses
  // "turf-intersect"; untested (e.g.: could trigger itself twice?)
  // layerAddHandler = (newLayer) => {
  //   const jNewLayer = newLayer.toGeoJSON();
  //   for (const layer of map.getLayers()) {
  //     if (layer !== newLayer) {
  //       jOldLayer = oldLayer.toGeoJSON();
  //       intersection = intersect(oldLayer, newLayer)
  //       if (intersection) {
  //         oldLayer = L.geoJSON(turf.difference(jOldLayer, intersection));
  //         newLayer = L.geoJSON(turf.difference(jNewLayer, intersection));
  //       }
  //     }
  //   }
  // }

  /**
   * stops a current edit/draw interaction
   * @param {*} ctx
   * @param {*} save boolean if set to true, it will resolve with a geojson
   * containing the edited multipolygon. Else, it will resolve null
   */
  multiEditStop(ctx, save) {
    const st = ctx.state.leaflet.embedded;
    st.LDraw.editing = false;
    st.LDraw.control.remove();
    st.LMap.off(L.Draw.Event.CREATED, st.LDraw.drawEvent);
    if (save === true) {
      const multiPoly = {
        type: "Feature",
        geometry: {type: "MultiPolygon", coordinates: [] }
      };
      st.LDraw.featureGroup.getLayers().map(l => {
        const json = l.toGeoJSON();
        if (json.type === "FeatureCollection"){
          for (const feature of json.features) {
            multiPoly.geometry.coordinates.push(
              feature.geometry.coordinates);
            }
        } else {
          multiPoly.geometry.coordinates.push(json.geometry.coordinates);
        }
      });
      return multiPoly;
    } else {
      return null;
    }
  },


  drawSquare(ctx, NESW) {
    const st = ctx.state.leaflet.embedded;
    st.LDraw.featureGroup.addLayer(
      L.rectangle(
        [[ NESW.S, NESW.W ],[ NESW.N, NESW.E ]]));
  },

  drawCircle(ctx, params) {
    const center = [params.lng, params.lat];
    let result;
    const steps = 64;
    const units = "meters";
    const circ1 = circle(center, params.radius, steps, units);
    if (params.innerRadius) {
      const circ2 = circle(center, params.innerRadius, steps, units);
      result = L.geoJSON(difference(circ1, circ2));
    } else {
      result = L.geoJSON(circ1);
    }
    ctx.state.leaflet.embedded.LDraw.featureGroup.addLayer(result);
    return result;
  },

  getJsonBounds(ctx, geojson ) {
    const layer = L.geoJSON(geojson);
    const bounds = layer.getBounds();
    return {
      S: bounds.getSouth(),
      W: bounds.getWest(),
      N: bounds.getNorth(),
      E: bounds.getEast()
    };
  },

  /**
   * Get a square based on center and distance from a coordinate
   * @param params.lat
   * @param params.lng
   * @param params.dist size of one side of the square (in meters)
   */
  getJsonSquare(ctx, params) {
    const layer = L.latLng(params.lat, params.lng);
    return L.rectangle(layer.toBounds(params.dist));
  },

  /**
   * Get a rectangle based on latitude/longtitude corners
   * (geographical coordinates)
   * @param {*} ctx
   * @param {*} NESW parameters N, S, E, W need to be set.
   */
  getJsonRectangle(ctx, NESW) {
    return new Promise(resolve => {
      resolve(L.rectangle([
        [ NESW.S, NESW.W ],
        [ NESW.N, NESW.E ]
      ]));
    });
  },

  /**
   *
   * @param {*} ctx
   * @param {*} markerInfo lat, lng
   */
  addMarker(ctx, markerInfo) {
    markerInfo.icon = L.divIcon({
      iconSize: [10, 10],
      iconUrl: null,
      shadowUrl: null,
      html: "",
      className: "animated-icon"
    });

    L.marker(
      [markerInfo.lat, markerInfo.lng], {icon: markerInfo.icon}
    ).addTo(ctx.state.leaflet.LMap);
    return;
  },

  /**
   * Retrieve and set the layers to be displayed
   * @param {*} ctx
   * @param {*} userLayerObj further information needed from application state
   * @param {*} userLayerObj.layers array of group/maplayers displayable
   * @param {*} userLayerObj.state any saved profile states (properties id, state)
   */
  fetchLayers(ctx, userLayerObj) {
    return new Promise(resolve => {
      fetch(window.apiprefix + "layer")
      .then(response => {
        response.json()
        .then(data => {
          if (response.status !== 200) {
            ctx.state.leaflet.availableLayers = [];
            if (response.status !== 401) {
              throw new Error(data.message || response.statusText);
            }
            resolve();
          } else {
            ctx.state.leaflet.availableLayers = [];
            if (!data || !Array.isArray(data)) {
              resolve();
            }
            for (const layer of data) {
              const userLayer = userLayerObj.layers.find(
                ul => ul.layerID === layer.id);
              if (userLayer) {
                const availableLayer = {
                  layerID: layer.id,
                  name: layer.name,
                  displayName: userLayer.displayName || layer.defaultName,
                  isActive: userLayer.checkedOnLogin,
                  zIndex: userLayer.zIndex,
                  isTimeLayer: layer.timeLayer,
                  isBaseLayer: layer.baseLayer
                };
                // restore saved states from profile, if available
                const saveState = userLayerObj.layerStates.find(
                  s => s.id === layer.id);
                if (saveState) {
                  availableLayer.isActive = saveState.state;
                }

                // setting default values
                const layerType = layer.layerType || "wms";
                const layerUrl = layer.externalServer || "/wms";
                const layerParameter = layer.externalParameter || {format: 'image/png', transparent: true, tiled: false};

                if (layerType === "google") {
                  availableLayer.layer = new L.Google("SATELLITE");
                } else if (
                  layerType === "wmst" &&
                  availableLayer.isTimeLayer === true
                ) {
                  availableLayer.layer = L.tileLayer.wmst(layerUrl,
                    { ...layerParameter,
                      ...{
                        //TODO needs communication with backend/ further settings
                        // e.g. "singleTile" to "tileSize"
                        layers: layer.geoserverName,
                        tileSize: 1024,
                        snapTo: 15,
                        startTime: "00:00", // TODO unused?
                        timeSliceWidth: layer.tickWidth,
                        startDate: new Date(new Date().setDate(new Date().getDate() - 1)).getTime()
                      }
                    }
                  );
                } else if (layerType === "wmts")    {
                    // This is for handling the osm baselayers
                  availableLayer.layer = L.tileLayer.wms(layerUrl,
                    {   format: 'image/png', 
                        transparent: false,
                        tiled: true,
                        layers: layer.geoserverName,
                        tileSize: 256
                    });
                } else if (layerType === "wms") {
                  availableLayer.layer = L.tileLayer.wms(layerUrl,
                    { ...layerParameter,
                      ...{
                        layers: layer.geoserverName,
                        tileSize: 1024
                      }
                    });
                } else {
                  continue;
                }// TODO: further layer types

                ctx.state.leaflet.availableLayers.push(availableLayer);
              }
            }
            resolve();
          }
        });
      });
    });
  },
  resetInitialState(ctx) {
    ctx.state = this.getInitialState();
  }
};

export default {
  namespaced: true,
  actions,
  state,
  mutations
};
