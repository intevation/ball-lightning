import Vue from "vue";
import Router from "vue-router";
import Map from "./views/Map.vue";
import Management from "./views/Management.vue";
import UserProfile from "./views/UserProfile.vue";
import UserLog from "./views/UserLog.vue";
import AlarmLog from "./views/AlarmLog.vue";
import Settings from "./views/Settings.vue";
import About from "./views/About.vue";
import Login from "./views/Login.vue";
import store from "./store/modules/application.js";
Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "map",
      component: Map,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/management",
      name: "management",
      component: Management,
      meta: {
        requiresAuth: true,
        permissions: ["list users", "edit users"]
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: UserProfile,
      meta: {
        requiresAuth: true,
        permissions: ["list users", "edit users"]
      }
    },
    {
      path: "/alarmlog",
      name: "alarmlog",
      component: AlarmLog,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/userlog",
      name: "userlog",
      component: UserLog,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/settings",
      name: "settings",
      component: Settings,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/about",
      name: "about",
      component: About,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        isAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.loggedIn) {
      next({
        path: "/login"
      });
    } else if (
      to.matched.some(
        record => record.meta.permissions && record.meta.permissions.length > 0
      )
    ) {
      if (
        store.state.UserPermissions.some(pKey =>
          to.matched[0].meta.permissions.includes(
            store.state.UserActivities.permissions[pKey].name
          )
        )
      ) {
        next();
      } else {
        next(from);
      }
    } else {
      next();
    }
  } else if (
    to.matched.some(record => record.meta.isAuth) &&
    store.state.loggedIn
  ) {
    next({
      path: "/"
    });
  } else {
    next();
  }
});

export default router;
