import "@fortawesome/fontawesome-free/css/all.css";
import Vue from "vue";
import Vuetify from "vuetify/lib";
import VueResize from "vue-resize";
import "vue-resize/dist/vue-resize.css";
import "vuetify/dist/vuetify.min.css";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);
Vue.use(VueResize);
Vue.use(Vuetify);
export default new Vuetify({
  icons: {
    iconfont: "fa"
  },
  options: {
    customProperties: true
  },
  theme: {
    dark: false,
    themes: {
      light: {
        primary: {
          base: "#009999",
          darken1: "#008888",
          lighten1: "#a0dcdc"
        },
        secondary: "#8585F4",
        accent: "#82B1FF",
        error: "#444444",
        info: "#33b5e5",
        success: "#00C851",
        warning: "#ffbb33"
      }
    }
  }
});
