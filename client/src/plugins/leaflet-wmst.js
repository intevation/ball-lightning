import * as L from "leaflet";

L.tileLayer.wmst = function(url, options) {
  return new L.TileLayer.wmst(url, options);
};

L.TileLayer.wmst = L.LayerGroup.extend({
  initialize: function(url, options) {
    this.options.startDate = this._getSnappedDate(
      this.options.startDate,
      this.options.snapTo
    );
    L.setOptions(this, options);
    L.LayerGroup.prototype.initialize.call(this.options);
    if (!this._layers) {
      // TODO still needed (constructor mess up)?
      this._layers = [];
    }
    this.baseUrl = url;
  },
  /**
   * The last loaded time as timestamp
   */
  activeDate: 0,

  /**
   * The currentlyshown layer
   */
  _currentLayer: 0,

  /**
   * The current cache size.
   */
  _currentCache: 0,

  options: {
    /**
     * To be used in 'rounding' of dates (e.g. some layers only offer data
     * every full half hour mark).
     * Assumed to be minutes
     */
    snapTo: null,

    /**
     * Last date the time references to. currently a timestamp in milliseconds
     */
    endDate: new Date().valueOf(),

    /**
     * Last first the time references to. currently a timestamp in milliseconds
     */
    startDate: null,

    /**
     * The amount of layers preloaded to cache.
     * TODO: should be at least 1; untested how much it can be
     *  expanded (tickAmount?) without messing up leaflet, browser and/or
     *  server connections
     */
    preCache: 5,

    /**
     * Keep all loaded layers in cache until the `maxCache` limit is reached.
     */
    cacheAll: false,

    /**
     * Maximum amount of layers in cache.
     * Do not increase to much! Can extremely slow down the browser!
     */
    maxCache: 10
  },

  /**
   * Displays the layer with the given timestamp.
   * Starts preloading the next layers.
   * @param {number} timeStamp timestamp in milliseconds
   * @returns {Promise<L.TileLayer.wms>} resolves with the new active layer
   * @async
   */
  activateTime: function(timeStamp) {
    const timeParams = this.getTimeParamString(timeStamp);
    const me = this;
    const loadCallBack = function(tp) {
      const layer = me.getLayers().find(l => l.wmsParams.time === tp);
      if (layer) {
        layer.setOpacity(1);
      }
      const otherLayers = me.getLayers().filter(l => l.wmsParams.time !== tp);
      if (otherLayers.length) {
        for (let i = 0; i < otherLayers.length; i++) {
          otherLayers[i].setOpacity(0);
        }
      }
    };
    const newActiveLayer = me
      .getLayers()
      .find(l => l.wmsParams.time === timeParams);
    const ndx = me.getLayers().findIndex(l => l.wmsParams.time === timeParams);
    if (newActiveLayer) {
      loadCallBack(timeParams);
      this.activeLayer = newActiveLayer;
      this.activeDate = timeStamp;
      this.options._currentCache = me.getLayers().length - ndx;
      this._preload(timeStamp);
      return Promise.resolve({
        layer: this.activeLayer,
        cacheCount: this.getLayers().length - ndx
      });
    } else {
      return new Promise(resolve => {
        me._addTime(timeParams).then(layer => {
          loadCallBack(layer.wmsParams.time);
          me.activeLayer = layer;
          this.options._currentCache = me.getLayers().length - ndx;
          this._preload(timeStamp);
          resolve({ layer: layer, cacheCount: me.getLayers().length - ndx });
        });
      });
    }
  },

  /**
   * Load future layers to precache them. Calls itself recursively until maxCache is reached.
   * @param {*} time The time of the layer
   */
  _preload: function(time) {
    if (
      this.getLayers().findIndex(item => {
        return item === this.activeLayer;
      }) <
      this.getLayers().length - this.options.preCache
    ) {
      return;
    } else {
      let next = this._getNextTime(new Date(time).getTime());
      const me = this;
      this._addTime(this.getTimeParamString(next)).then(layer => {
        let prevTime = this.getTimeParamString(
          this._getNextTime(
            new Date(me.activeLayer.wmsParams.time.split('/')[0]).getTime())
        );
        if (layer.wmsParams.time === prevTime) {
          me.checkCacheLoad();
        }
        this.options._currentCache++;
        this.fire("cacheload", { cacheCount: this.options._currentCache });
      });
      this._preload(next);
      let cacheSize = this.options.preCache;
      if (this.options.cacheAll) {
        cacheSize = this.options.maxCache;
      }
      if (this.getLayers().length > cacheSize) {
        this.removeLayer(this.getLayers()[0]);
      }
    }
  },

  invalidateCache: function() {
    if (!this.getLayers) {
      return;
    }
    this.getLayers().clear();
  },

  checkCacheLoad: function() {
    this.getLayers()
      .filter(layer => layer.wmsParams.time > this.activeLayer.wmsParams.time)
      .map(layer => {
        if (!layer.isLoading()) {
          this.fire("cacheload", { cacheCount: this.options._currentCache });
        }
      });
  },

  /**
   * Loads a layer with the given timestamps, without displaying it
   * (setting opacity to 0)
   * @param timeParams stringify time parameters
   * @returns {Promise<L.TileLayer.wms>}
   * TODO: just internal use; currently does not check if the layer is already
   * present
   * @async
   */
  _addTime: function(timeParams) {
    const me = this;
    return new Promise(resolve => {
      const options = {
        format: me.options.format,
        layers: me.options.layers,
        transparent: me.options.transparent,
        time: timeParams,
        opacity: 0,
        tileSize: me.options.tileSize
        // TODO more options to pass? - just don't pass everything in (this)
      };
      const newLayer = L.tileLayer.wms(me.baseUrl, options);
      this.addLayer(newLayer);
      newLayer.once("load", function() {
        resolve(newLayer);
      });
    });
  },

  /**
   * Get the next time for WMS-Time request.
   * @param {*} time Time in milliseconds. Base to calculate the next time for requests
   */
  _getNextTime: function(time) {
    // If the layer has a time interval set, calculate next time to request tiles
    if (this.options.snapTo && this.options.snapTo > 0) {
      return time + 60000 * this.options.snapTo;
    }
    // The layer has no interval. use next minute to request tiles.
    return time + 60000;
  },

  /**
   * gets the wms timeParams for a certain a
   * @param {Number} timeStamp
   * @returns {String} a time string understood by wms-t
   *   (either timestamp or two timestamps)
   * TODO check if this is up to specs
   * TODO: needs not to be exposed?
   */
  getTimeParamString: function(timeStamp) {
      if (!this.options.timeSliceWidth) {
        return new Date(timeStamp).toISOString();
      } else {
        return new Date(timeStamp).toISOString()
        + "/"
        + new Date( timeStamp + this.options.timeSliceWidth).toISOString();
      }
  },

  /**
   * Gets the timestamp rounded to the snapping options
   * @param {Number} timestamp
   * @param {Number} snapTo distinct moments (ms)
   * example: full quarter hour (1000 * 60 * 15) -> 12:15:00.000
   * @returns { String }
   */
  _getSnappedDate: function(timestamp, snapTo) {
    timestamp = timestamp || new Date().valueOf();
    if (snapTo) {
      timestamp = timestamp - (timestamp % (60000 * snapTo));
      timestamp = timestamp + 60000 * 15;
    }
    return timestamp;
  }
});

L.TileLayer.wmst.addInitHook(function() {
  const endTime = this._getSnappedDate(
    this.options.endDate,
    this.options.snapTo
  );
  L.setOptions(this, {
    endDate: endTime
  });
  if (this.options.startDate) {
    this.options.startDate = this._getSnappedDate(
      this.options.startDate,
      this.options.snapTo
    );
    this.activateTime(this.options.startDate);
  }
  return;
});
