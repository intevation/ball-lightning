import "core-js/stable";
import "regenerator-runtime/runtime";
import "whatwg-fetch";
import Vue from "vue";
import vuetify from "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueI18n from "vue-i18n";

import en from "./locales/en";
import de from "./locales/de";

// set the prefix used for the "api" calls"
window.apiprefix = "";
if (process.env.NODE_ENV === "development") {
  window.apiprefix = "api/";
}
Vue.config.productionTip = false;

const messages = {
  de: de,
  en: en
};

const i18n = new VueI18n({
  locale: "de", // set locale
  messages
});

new Vue({
  i18n,
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
