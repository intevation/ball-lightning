module.exports = {
  transpileDependencies: ["vuetify", "dygraphs"],
  lintOnSave: "error",
  devServer: {
    disableHostCheck: true,
    proxy: {
      "/api": {
        pathRewrite: { "^/api": "" },
        target: "http://ball-lightning-backend:5000/"
      }
    }
  }
};
