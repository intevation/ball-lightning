# ball-lightning

A web application to display lightning events on a map in realtime.
This is WIP for a new generation of the kugelblitz.

- [ball-lightning](#ball-lightning)
  - [Requirements](#requirements)
  - [Usage](#usage)
  - [Development](#development)
    - [Getting started client development](#getting-started-client-development)
    - [Building client for production](#building-client-for-production)
  - [HowTo make a new release](#howto-make-a-new-release)
  - [Support](#support)
  - [License](#license)

## Requirements

- Lightning location sensors for providing the lightning data
- [PostgreSQL](https://www.postgresql.org/) / [PostGIS](http://postgis.net/)
  for storing and processing the lightning data from the sensors
- [Go](https://golang.org) for building `kugelblitz`

## Usage

TODO

## Development

Find the development docs in
<https://bitbucket.org/intevation/ball-lightning/src/dev-doc/client/doc/>.

To get started take a look in
<https://bitbucket.org/intevation/ball-lightning/src/dev-doc/client/doc/DevComponent.md>.

### Getting started client development

```sh
git clone git@bitbucket.org:intevation/ball-lightning.git
cd client
yarn
yarn serve
```

Open browser `http://localhost:8080/`.

### Building client for production

```sh
yarn build
```

Move `dist/` directory to the right location.

## HowTo make a new release

Creating a release is handling via Bitbucket pipelines by pushing tags
following [SemVer](https://semver.org/) `vX.Y.Z` scheme to
<https://bitbucket.org/intevation/ball-lightning/>.

```shell
git tag vX.Y.Z
git push origin --tags
```

After pushing the tag, the release can be found in the [Downloads
section](https://bitbucket.org/intevation/ball-lightning/downloads/) as
`ball-lightning-vX.Y.Z.zip`.

The `zip`-archive contains:

```shell
.
├── lightning
└── web
    ├── css
    ├── fonts
    ├── img
    ├── js
    └── media
```

## Support

Please use Bitbucket for questions:
<https://bitbucket.org/intevation/ball-lightning/issues>

## License

This is Free Software governed by the terms of the Apache 2.0 license.
See [LICENSE](LICENSE) for details.
Copyright 2012 - 2019 Siemens AG.
Engineering by Intevation GmbH.
