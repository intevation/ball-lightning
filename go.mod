module bitbucket.org/intevation/ball-lightning

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/securecookie v1.1.1
	github.com/jackc/pgx v3.6.1+incompatible
	github.com/lib/pq v1.2.0
	github.com/mb0/wkt v0.0.0-20170420051526-a30afd545ee1
	github.com/pkg/errors v0.8.1
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
