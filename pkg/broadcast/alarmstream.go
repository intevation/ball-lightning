// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package broadcast

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"strconv"
	"time"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
)

const alarmAreaSQL = `
SELECT get_alarm_geom FROM alarmserver.get_alarm_geom($1)
`

type alarmstreamClient struct {
	id   int64
	ctx  context.Context
	outs []*Queue
}

// AlarmStreamBroker registers to a Receiver and broadcasts
// the transcripted alarm messages to the SSE clients.
type AlarmStreamBroker struct {
	cmds    chan func()
	clients map[string]*alarmstreamClient
}

type AlarmAreaInfo struct {
	count int64
	geom  string
}

// NewAlarmStreamBroker creates a new AlarmStreamBroker.
func NewAlarmStreamBroker() *AlarmStreamBroker {
	return &AlarmStreamBroker{
		cmds:    make(chan func()),
		clients: make(map[string]*alarmstreamClient),
	}
}

// Run starts the dispatching and does not return.
func (asb *AlarmStreamBroker) Run() {
	for cmd := range asb.cmds {
		cmd()
	}
}

type incomingAlarmMessage struct {
	ID       int64   `json:"id"`
	GroupIDs []int64 `json:"groupids"`
}

func (asb *AlarmStreamBroker) find(groupIDs []int64, fn func(*alarmstreamClient)) {

	// No groups -> nothing to deliver.
	if len(groupIDs) == 0 {
		return
	}

	found := make(map[*alarmstreamClient]bool)

	for _, groupID := range groupIDs {
		for _, client := range asb.clients {
			if !found[client] && client.id == groupID {
				found[client] = true
				fn(client)
			}
		}
	}
}

func (client *alarmstreamClient) sendAlarm(
	typ string, id int64,
	area func(int64) (*AlarmAreaInfo, error)) {

	aai, err := area(id)
	if err != nil {
		log.Printf("error: %v\n", err)
		return
	}
	if aai == nil || aai.geom == "" {
		return
	}
	var out = struct {
		Type  string      `json:"type"`
		ID    int64       `json:"id"`
		Count int64       `json:"count"`
		Geom  interface{} `json:"geom"`
	}{
		Type:  typ,
		ID:    id,
		Count: aai.count,
		Geom:  aai.geom,
	}
	encoded, err := json.Marshal(&out)
	if err != nil {
		log.Printf("error: %v\n", err)
		return
	}
	for _, out := range client.outs {
		out.Append(encoded)
	}
}

func (client *alarmstreamClient) sendAllClear(typ string, id int64) {
	var out = struct {
		Type string `json:"type"`
		ID   int64  `json:"id"`
	}{
		Type: typ,
		ID:   id,
	}
	encoded, err := json.Marshal(&out)
	if err != nil {
		log.Printf("error: %v\n", err)
		return
	}
	for _, out := range client.outs {
		out.Append(encoded)
	}
}

func cachedAlarmArea() func(int64) (*AlarmAreaInfo, error) {
	cache := make(map[int64]*AlarmAreaInfo)
	return func(id int64) (*AlarmAreaInfo, error) {
		if aai, ok := cache[id]; ok {
			return aai, nil
		}
		var aai *AlarmAreaInfo
		var err error
		err = database.QueryWithoutCtx(func(row *sql.Rows) error {
			err = row.Scan(aai)
			return err
		},
			alarmAreaSQL, id)
		if err != nil {
			cache[id] = aai
		}
		return aai, err
	}
}

func (asb *AlarmStreamBroker) handleAlarm(msg *incomingAlarmMessage) {
	//log.Println("AlarmStreamBroker.handleAlarm")
	cache := cachedAlarmArea()
	asb.find(msg.GroupIDs, func(client *alarmstreamClient) {
		client.sendAlarm("Alarm", msg.ID, cache)
	})
}

func (asb *AlarmStreamBroker) handleAllClear(msg *incomingAlarmMessage) {
	//log.Println("AlarmStreamBroker.handleAllClear")
	asb.find(msg.GroupIDs, func(client *alarmstreamClient) {
		client.sendAllClear("Entwarnung", msg.ID)
	})
}

func (asb *AlarmStreamBroker) handleTestAlarm(msg *incomingAlarmMessage) {
	//log.Println("AlarmStreamBroker.handleTestAlarm")
	cache := cachedAlarmArea()
	asb.find(msg.GroupIDs, func(client *alarmstreamClient) {
		client.sendAlarm("Testalarm", msg.ID, cache)
	})
}

func (asb *AlarmStreamBroker) handleTestAllClear(msg *incomingAlarmMessage) {
	//log.Println("AlarmStreamBroker.handleTestAlarm")
	asb.find(msg.GroupIDs, func(client *alarmstreamClient) {
		client.sendAllClear("Testentwarnung", msg.ID)
	})
}

func (asb *AlarmStreamBroker) handle(
	msg json.RawMessage, fn func(*AlarmStreamBroker, *incomingAlarmMessage)) error {

	var in incomingAlarmMessage
	if err := json.Unmarshal(msg, &in); err != nil {
		return err
	}
	// Move to alarm main.
	asb.cmds <- func() { fn(asb, &in) }
	return nil
}

func (asb *AlarmStreamBroker) heartbeat() {
	// Send hearbeat with timestamp
	hbData := []byte(`{"heartbeat":` + strconv.FormatInt(time.Now().UTC().UnixNano(), 10) + `}`)
	for _, client := range asb.clients {
		for _, out := range client.outs {
			out.Append(hbData)
		}
	}
}

// HandleHeartbeat handles the hearbeat in the alarmstream
func (asb *AlarmStreamBroker) HandleHeartbeat(json.RawMessage) error {
	asb.cmds <- func() { asb.heartbeat() }
	return nil
}

// HandleAlarm is the entry point for "Alarm" events.
func (asb *AlarmStreamBroker) HandleAlarm(msg json.RawMessage) error {
	return asb.handle(msg, (*AlarmStreamBroker).handleAlarm)
}

// HandleAllClear is the entry point for "Entwarnung" events.
func (asb *AlarmStreamBroker) HandleAllClear(msg json.RawMessage) error {
	return asb.handle(msg, (*AlarmStreamBroker).handleAllClear)
}

// HandleTestAlarm is the entry point for "Testalarm" events.
func (asb *AlarmStreamBroker) HandleTestAlarm(msg json.RawMessage) error {
	return asb.handle(msg, (*AlarmStreamBroker).handleTestAlarm)
}

// HandleTestAllClear is the entry point for "Testentwarnung" events.
func (asb *AlarmStreamBroker) HandleTestAllClear(msg json.RawMessage) error {
	return asb.handle(msg, (*AlarmStreamBroker).handleTestAllClear)
}

func (asb *AlarmStreamBroker) sessionDied(key string) {
	client := asb.clients[key]
	if client == nil {
		return
	}
	delete(asb.clients, key)
	for _, out := range client.outs {
		out.Close()
	}
	client.outs = nil
}

func (asb *AlarmStreamBroker) registerClient(ctx context.Context, key string, id int64, out *Queue) {
	client := asb.clients[key]
	if client != nil {
		client.outs = append(client.outs, out)
		return
	}
	client = &alarmstreamClient{
		id:   id,
		ctx:  ctx,
		outs: []*Queue{out},
	}
	asb.clients[key] = client
}

func (asb *AlarmStreamBroker) unregisterClient(key string, out *Queue) {
	client := asb.clients[key]
	if client == nil {
		return
	}

	outs := client.outs

	for i, o := range outs {
		if o == out {
			out.Close()
			copy(outs[i:], outs[i+1:])
			outs[len(outs)-1] = nil
			outs = outs[:len(outs)-1]
			break
		}
	}
	if len(outs) == 0 {
		delete(asb.clients, key)
	} else {
		client.outs = outs
	}
}

// SessionDied is a called when a session died.
func (asb *AlarmStreamBroker) SessionDied(key string) {
	asb.cmds <- func() { asb.sessionDied(key) }
}

// RegisterClient is called to register a SSE client.
func (asb *AlarmStreamBroker) RegisterClient(ctx context.Context, key string, id int64, queue *Queue) {
	asb.cmds <- func() { asb.registerClient(ctx, key, id, queue) }
}

// UnregisterClient is called to remove a SSE client previously registered.
func (asb *AlarmStreamBroker) UnregisterClient(key string, queue *Queue) {
	asb.cmds <- func() { asb.unregisterClient(key, queue) }
}
