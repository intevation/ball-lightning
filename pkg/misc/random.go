// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package misc

import (
	"bytes"
	"crypto/rand"
	"io"
	"log"
	"math/big"
)

// GenerateRandomKey generates a cryptographically secure random key
// of a given length.
func GenerateRandomKey(length int) []byte {
	k := make([]byte, length)
	if _, err := io.ReadFull(rand.Reader, k); err != nil {
		return nil
	}
	return k
}

// RandomString generates a cryptographically secure password
// of a given length which consists of alpha numeric characters
// and at least one 'special' one.
func RandomString(n int) string {

	const (
		special  = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
		alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
			"abcdefghijklmnopqrstuvwxyz" +
			"0123456789" +
			special
	)

	max := big.NewInt(int64(len(alphabet)))
	out := make([]byte, n)

	for i := 0; i < 1000; i++ {
		for i := range out {
			v, err := rand.Int(rand.Reader, max)
			if err != nil {
				log.Panicf("error: %v\n", err)
			}
			out[i] = alphabet[v.Int64()]
		}
		// Ensure at least one special char.
		if bytes.ContainsAny(out, special) {
			return string(out)
		}
	}
	log.Println("warn: Your random generator may be broken.")
	out[0] = special[0]
	return string(out)
}
