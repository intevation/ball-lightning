// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package misc

import (
	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"gopkg.in/gomail.v2"
)

// SendMail sends an email to a given address with a given subject
// and body.
// The credentials to contact the SMPT server are taken from the
// configuration.
func SendMail(cfg database.Configuration, address, subject, body string) error {

	var (
		host     = cfg.String("pw-reset-mail-host", "localhost")
		port     = int(cfg.Int64("pw-reset-mail-port", 25))
		username = cfg.String("pw-reset-mail-user", "")
		password = cfg.String("pw-reset-mail-pw", "")
		helo     = cfg.String("pw-reset-mail-helo", "")
		mailFrom = cfg.String("pw-reset-mail-from", "")
	)

	m := gomail.NewMessage()
	m.SetHeader("From", mailFrom)
	m.SetHeader("To", address)
	m.SetHeader("Subject", subject)
	m.SetBody("text/plain", body)

	d := gomail.Dialer{
		Host:      host,
		Port:      int(port),
		Username:  username,
		Password:  password,
		LocalName: helo,
		SSL:       port == 465,
	}

	return d.DialAndSend(m)
}
