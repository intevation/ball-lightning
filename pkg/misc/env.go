package misc

import "os"

// Env looks up a value for environment variable and returns it.
// If not found it returns the given default value.
func Env(key, def string) string {
	if value, found := os.LookupEnv(key); found {
		return value
	}
	return def
}
