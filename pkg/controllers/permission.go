// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

// getPermissions controls the endpoint of /permission (GET) and returns
// the permissions of the currently logged in user.
func getPermissions(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	userID, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No user for session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	var permissions []*models.Permission
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		permissions, err = models.LoadUserPermissions(ctx, conn, userID)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: permissions,
		Code:   http.StatusOK,
	}, nil
}

func getPermissionsByRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var permissions []*models.Permission
	var roleID int64
	var err error
	if roleID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			permissions, err = models.LoadPermissionsByRole(ctx, conn, roleID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: permissions,
			Code:   http.StatusOK,
		}, nil
	}
	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

func getPermission(req *http.Request) (m.JSONResult, error) {
	// TODO: Implement me!
	return m.JSONResult{}, fmt.Errorf("Not implemented, yet")
}

// getPermissionByID controls the endpoint of /permission/$ID (GET) and returns
// a single permission identified by id.
func getPermissionByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["pid"], 10, 64); err == nil {
		var permission *models.Permission
		var err error

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			permission, err = models.LoadPermissionByID(ctx, conn, id)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: permission,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}
