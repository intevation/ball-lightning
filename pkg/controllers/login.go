// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/jackc/pgx/pgtype"
)

const (
	loginSQL = `
SELECT id FROM users.users
WHERE
  now() BETWEEN start_date AND stop_date AND
  name = $1 AND password = md5($2)`
)

// TODO: Move to models.
func checkUser(ctx context.Context, user, password string) (int64, error) {
	var id int64
	err := database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		return conn.QueryRowContext(ctx, loginSQL, user, password).Scan(&id)
	})
	return id, err
}

func loadUserActivities(ctx context.Context, userID int64) (*models.UserActivities, error) {
	var userActivities *models.UserActivities
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		userActivities, err = models.LoadActivitiesForUser(ctx, conn, userID)
		return err
	})
	return userActivities, err
}

func loadUser(ctx context.Context, userID int64) (*models.User, error) {
	var user *models.User
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		user, err = models.LoadUser(ctx, conn, userID)
		return err
	})
	return user, err
}

func loadTheme(ctx context.Context, userID int64) (*models.Theme, error) {
	var theme *models.Theme
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		theme, err = models.LoadThemeForUser(ctx, conn, userID)
		return err
	})
	return theme, err
}

func loadProfile(ctx context.Context, userID int64) (*models.Profile, error) {
	var profile *models.Profile
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		profile, err = models.LoadProfileForUser(ctx, conn, userID)
		return err
	})
	return profile, err
}

func loadStrokeArea(ctx context.Context, userID int64) (*models.StrokeArea, error) {
	var area *models.StrokeArea
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		area, err = models.LoadStrokeArea(ctx, conn, userID)
		return err
	})
	return area, err
}

func loadAppSettings(ctx context.Context, userID int64) (*models.AppSettings, error) {
	var settings *models.AppSettings
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		settings, err = models.LoadAppSettings(ctx, conn, userID)
		return err
	})
	return settings, err
}

func loadUserLayers(ctx context.Context, userID int64) ([]models.LayerForGroup, error) {
	var layers []models.LayerForGroup
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		layers, err = models.LoadMapLayersUser(ctx, conn, userID)
		return err
	})
	return layers, err
}

func login(rw http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	s, _ := session.FromContext(ctx)
	_, ok := s.GetUser()
	if ok {
		s.Logout()
	}

	var cred struct {
		Name     *string `json:"name"`
		Password *string `json:"password"`
	}

	defer req.Body.Close()
	if err := json.NewDecoder(req.Body).Decode(&cred); err != nil {
		http.Error(rw, fmt.Sprintf("error: %v\n", err), http.StatusBadRequest)
		return
	}
	if cred.Name == nil {
		SendFailure(rw, "Missing 'name'")
		return
	}

	if cred.Password == nil {
		SendFailure(rw, "Missing 'password'")
		return
	}

	userID, err := checkUser(ctx, *cred.Name, *cred.Password)
	switch {
	case err == sql.ErrNoRows:
		SendFailure(rw, "No such user or invalid password.")
		return
	case err != nil:
		SendFailure(rw, "Failed to load user: %v", err)
		return
	}
	s.SetUser(userID)

	userActivities, err := loadUserActivities(ctx, userID)
	if err != nil {
		SendFailure(rw, "Failed to load Activities: %v", err)
		return
	}

	user, err := loadUser(ctx, userID)
	if err != nil {
		SendFailure(rw, "Failed to load User details: %v", err)
		return
	}

	theme, err := loadTheme(ctx, userID)
	if err != nil {
		theme = &models.Theme{
			Data: &pgtype.JSON{
				Status: pgtype.Null,
			},
		}
	}

	profile, err := loadProfile(ctx, userID)
	if err != nil {
		profile = &models.Profile{
			Data: &pgtype.JSON{
				Status: pgtype.Null,
			},
		}
	}

	strokeArea, err := loadStrokeArea(ctx, userID)
	if err != nil {
		strokeArea = &models.StrokeArea{
			Geom: nil,
		}
	}

	appSettings, err := loadAppSettings(ctx, userID)
	if err != nil {
		appSettings = nil
	}

	layers, err := loadUserLayers(ctx, userID)
	if err != nil {
		layers = []models.LayerForGroup{}
	}

	var result = struct {
		User           *models.User           `json:"User"`
		UserActivities *models.UserActivities `json:"UserActivities"`
		Theme          interface{}            `json:"theme"`
		Profile        interface{}            `json:"profile"`
		StrokeArea     interface{}            `json:"strokeArea"`
		Settings       *models.AppSettings    `json:"settings"`
		Layers         []models.LayerForGroup `json:"layers"`
	}{
		User:           user,
		UserActivities: userActivities,
		Theme:          theme.Data.Get(),
		Profile:        profile.Data.Get(),
		StrokeArea:     strokeArea.Geom,
		Settings:       appSettings,
		Layers:         layers,
	}
	LogUserAction(req, "Login successful")
	SendJSON(rw, http.StatusOK, &result)
}

func logout(rw http.ResponseWriter, req *http.Request) {
	s, _ := session.FromContext(req.Context())
	LogUserAction(req, "Log out")
	s.Logout()
	s.Invalidate()
	SendEmptyJSON(rw, http.StatusOK)
}
