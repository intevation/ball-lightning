// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/hex"
	"io"
	"log"
	"net/http"

	htmlTemplate "html/template"
	textTemplate "text/template"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/misc"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

const (
	insertRequestSQL = `INSERT INTO users.password_reset_requests (hash, user_id)
    SELECT $1, id FROM users.users WHERE name = $2
	ON CONFLICT (user_id) DO UPDATE SET hash = $1`

	countRequestsSQL = `SELECT count(*) FROM users.password_reset_requests`

	deleteRequestSQL = `DELETE FROM users.password_reset_requests
    WHERE hash = $1`

	findRequestSQL = `SELECT u.name
    FROM users.password_reset_requests prr
    JOIN users.users u on prr.user_id = u.id
    WHERE prr.hash = $1`

	userExistsSQL = `SELECT email FROM users.users WHERE name = $1`

	updatePasswordSQL = `UPDATE users.users
    SET password = MD5($1) WHERE id = $2`
)

const (
	hashLength        = 16
	passwordLength    = 20
	maxPasswordResets = 1000
)

var (
	errTooMuchPasswordResets = errors.New("too many password resets")
	errNoSuchUser            = errors.New("user does not exist")
	errInvalidUser           = errors.New("invalid user")
)

var (
	passwordResetRequestMailTmpl = textTemplate.Must(
		textTemplate.New("request").Parse(`You or someone else has requested a password change
for your account {{ .User }} on
{{ .Server }}

Please follow this link to have a new password generated:

{{ .Server }}/passwordreset/{{ .Hash }}

The link is only valid for 12 hours.

If you did not initiate this password reset or do not want to reset the
password, just ignore this email. Logging in with your old password
before following the link will cancel this password reset request, too.

Best regards
    Your service team`))

	passwordResetPage = htmlTemplate.Must(
		htmlTemplate.New("page").Parse(`<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Password reset done</title>
  </head>
  <body>
    <p>The password reset for user <strong><tt>{{ .User }}</tt></strong> successfully done.</p>
    <p>New password: <strong><tt>{{ .Password }}</tt></strong></p>
    <p><a href="/">Go to login page.</a></p>
  </body>
</html>
`))
)

func requestMessageBody(user, hash, server string) string {
	var content = struct {
		User   string
		Server string
		Hash   string
	}{
		User:   user,
		Server: server,
		Hash:   hash,
	}
	var buf bytes.Buffer
	if err := passwordResetRequestMailTmpl.Execute(&buf, &content); err != nil {
		log.Printf("error: %v\n", err)
	}
	return buf.String()
}

func changedMessageBody(w io.Writer, user, password string) error {
	var content = struct {
		User     string
		Password string
	}{
		User:     user,
		Password: password,
	}
	return passwordResetPage.Execute(w, &content)
}

func generateHash() string {
	return hex.EncodeToString(misc.GenerateRandomKey(hashLength))
}

func generateNewPassword() string {
	// Use internal generator.
	return misc.RandomString(passwordLength)
}

func backgroundRequest(host string, user *models.PWResetUser) error {

	if user.User == "" {
		return errInvalidUser
	}

	var hash, email string

	ctx := context.Background()

	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {

			var count int64
			if err := conn.QueryRowContext(
				ctx, countRequestsSQL).Scan(&count); err != nil {
				return err
			}

			// Limit total number of password requests.
			if count >= maxPasswordResets {
				return errTooMuchPasswordResets
			}

			err := conn.QueryRowContext(ctx, userExistsSQL, user.User).Scan(&email)

			switch {
			case err == sql.ErrNoRows:
				return errNoSuchUser
			case err != nil:
				return err
			}

			hash = generateHash()
			_, err = conn.ExecContext(ctx, insertRequestSQL, hash, user.User)
			return err
		},
	); err != nil {
		return err
	}

	body := requestMessageBody(user.User, hash, host)

	// using uncached config
	cfg, err := database.LoadConfigurationPrefix(
		misc.Env("CONFIG", ""), "pw-reset-mail-")
	if err != nil {
		log.Printf("warn: configuration loading failed: %v\n", err)
	}
	return misc.SendMail(
		cfg,
		email, "Password Reset Link", body)
}

func passwordResetRequest(req *http.Request) (middleware.JSONResult, error) {

	// We do the checks and the emailing in background
	// no reduce the risks of timing attacks.
	go func(user *models.PWResetUser) {
		//config.WaitReady()
		host := req.Host
		if err := backgroundRequest(host, user); err != nil {
			log.Printf("error: %v\n", err)
		}
	}(middleware.JSONInput(req).(*models.PWResetUser))

	// Send a neutral message to avoid being an user oracle.
	const neutralMessage = "If this account exists, a reset link will be mailed."

	return middleware.JSONResult{
		Result: &struct {
			Message string `json:"message"`
		}{neutralMessage},
	}, nil
}

func passwordReset(rw http.ResponseWriter, req *http.Request) {

	hash := mux.Vars(req)["hash"]
	if _, err := hex.DecodeString(hash); err != nil {
		http.Error(rw, "invalid hash", http.StatusBadRequest)
		return
	}

	var user, password string

	ctx := req.Context()

	err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			tx, err := conn.BeginTx(ctx, nil)
			if err != nil {
				return err
			}
			defer tx.Rollback()

			err = tx.QueryRowContext(ctx, findRequestSQL, hash).Scan(&user)
			switch {
			case err == sql.ErrNoRows:
				return errors.New("this URL is no longer valid")
			case err != nil:
				return err
			}
			password = generateNewPassword()
			res, err := tx.ExecContext(ctx, updatePasswordSQL, password, user)
			if err != nil {
				return err
			}
			if n, err2 := res.RowsAffected(); err2 == nil && n == 0 {
				return errors.New("user not found")
			}
			if _, err = tx.ExecContext(ctx, deleteRequestSQL, hash); err != nil {
				return err
			}
			return tx.Commit()
		},
	)

	switch {
	case err == sql.ErrNoRows:
		http.Error(rw, "No such request", http.StatusNotFound)
		return
	case err != nil:
		http.Error(rw, "Error: "+err.Error(), http.StatusInternalServerError)
		return
	}

	if err := changedMessageBody(rw, user, password); err != nil {
		log.Printf("error: %v\n", err)
	}
}
