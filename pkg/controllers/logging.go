// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
)

// getUserLogs controls the endpoint of /userlog (GET) and returns
// all logs that belong to the users group.
func getUserLogs(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{}, nil
	}
	userID, ok := s.GetUser()
	if !ok {
		return m.JSONResult{}, nil
	}

	var logs []models.Logging
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		logs, err = models.LoadUserLogs(ctx, conn, userID)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: logs,
		Code:   http.StatusOK,
	}, nil

}
