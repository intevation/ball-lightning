// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

// getUser controls the endpoint of /user (GET) and returns teh currently logged
// in user.
func getUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	id, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("no user found for session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	var user *models.User
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		user, err = models.LoadUser(ctx, conn, id)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}
	return m.JSONResult{
		Result: user,
		Code:   http.StatusOK,
	}, nil
}

// getUserByID controls the endpoint of /user/$ID (GET) and returns the user
// specified by id.
func getUserByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		var user *models.User
		var err error

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			user, err = models.LoadUser(ctx, conn, id)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
		return m.JSONResult{
			Result: user,
			Code:   http.StatusOK,
		}, nil
	}
	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createUser controls the endpoint of /user (POST) and inserts a the user
// defined by the request body into the database.
func createUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.User)

	// TODO create a password for the user!
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.CreateUser(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}
	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// updateUser controls the endpoint of /user (PUT) and updates the attributes
// of an existing user with the values given in the request body.
func updateUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.User)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateUser(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteUser controls the endpoint of /user/$ID and removes the user specified
// by id from the database.
func deleteUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}
	userID, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No user found for session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	vars := mux.Vars(req)

	var err error
	var id int64
	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if userID == id {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			return models.DeleteUser(ctx, conn, id)
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Code: http.StatusOK,
		}, nil
	}
	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// updateProfile controls the endpoint of /user/$ID/profile (PUT) and updates
// the user profile with the values given in the request body.
func updateProfile(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}
	id, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No user found for session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	vars := mux.Vars(req)
	var err error
	var reqUserID int64

	if reqUserID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil && reqUserID == id {
		var err error

		input, _ := m.JSONInput(req).(*interface{})

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			return models.UpdateProfile(ctx, conn, id, input)
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: struct {
				ID int64 `json:"id"`
			}{
				ID: id,
			},
			Code: http.StatusOK,
		}, nil
	}
	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getAlarmSettings controls the endpoint of /user/$ID/alarmsettings (GET) and
// returns the alarm settings for the specified user.
func getAlarmSettings(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var userID int64

	if userID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		var settings *models.AlarmSettings

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			settings, err = models.LoadAlarmSettings(ctx, conn, userID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: settings,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// updateAlarmSettings controls the endpoint of /user/$ID/alarmsettings (PUT)
// and updates the attributes with the values given in the request body.
func updateAlarmSettings(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.AlarmSettings)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateAlarmSettings(ctx, conn, *input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{ID: id},
		Code: http.StatusOK,
	}, nil
}
