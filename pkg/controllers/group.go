// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

// getGroup controls the endpoint of /group (GET) and returns the group the
// currently logged in user belongs to.
func getGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintf("No Session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	id, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintf("No user for Session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	var group *models.Group
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		group, err = models.LoadGroupByID(ctx, conn, id)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: group,
		Code:   http.StatusOK,
	}, nil
}

// getGroupByID controls the endpoint of /group/$ID (GET) and returns the
// group specified by the ID and the child objects (subgroups and users)
// of this group.
func getGroupByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		ret, err := fetchGroup(ctx, id)
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: ret,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createGroup controls the endpoint of /group (POST) and inserts the group
// specified in the request body into the database.
func createGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Group)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.CreateGroup(ctx, conn, input)
		if err != nil {
			return err
		}
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// updateGroup controls the endpoint of /group (PUT) and updates the attributes
// of an existing database item with the object given in the request body.
func updateGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Group)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateGroup(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteGroup controls the endpoint of /group/$ID (DELETE) and deletes an empty
// group from the database.
func deleteGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			return models.DeleteGroup(ctx, conn, id)
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
		return m.JSONResult{
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getAppSettings controls the endpoint of /group/$ID/settings (GET) and
// returns the app settings for the specified group.
func getAppSettings(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var groupID int64
	var err error
	var settings *models.AppSettings

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			settings, err = models.LoadAppSettings(ctx, conn, groupID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: settings,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// updateAppSettings controls the endpoint of /group/$ID/settings (PUT) and
// updates the attributes of the database item with teh values given in the
// request body.
func updateAppSettings(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var groupID int64
	var err error

	input, _ := m.JSONInput(req).(*models.AppSettings)

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			id, err = models.UpdateAppSettings(ctx, conn, groupID, *input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: struct {
				ID int64 `json:"id"`
			}{ID: id},
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// fetchGroup gathers the subtree items of a group specified by id.
func fetchGroup(ctx context.Context, id int64) (map[string]interface{}, error) {
	var group *models.Group
	var children []*models.Group
	var users []*models.User
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		group, err = models.LoadGroupByID(ctx, conn, id)
		children, err = models.LoadGroupChildren(ctx, conn, id)
		users, err = models.LoadUsersByGroup(ctx, conn, id)
		return err
	})
	if err == nil {
		return map[string]interface{}{
			"group":    group,
			"children": children,
			"users":    users,
		}, nil
	}

	return nil, err
}
