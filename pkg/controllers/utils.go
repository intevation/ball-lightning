// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/jackc/pgx/pgtype"
)

// SendEmptyJSON sends an empty JSON document to the client.
func SendEmptyJSON(rw http.ResponseWriter, code int) {
	rw.Header().Add("Content-Type", "application/json")
	rw.Header().Set("X-Content-Type-Options", "nosniff")
	rw.WriteHeader(code)
	fmt.Fprintln(rw, "{}")
}

// SendJSON sends JSON encoded data to client.
func SendJSON(rw http.ResponseWriter, code int, data interface{}) {
	rw.Header().Add("Content-Type", "application/json")
	rw.Header().Set("X-Content-Type-Options", "nosniff")
	rw.WriteHeader(code)
	if err := json.NewEncoder(rw).Encode(data); err != nil {
		log.Printf("error: %v\n", err)
	}
}

// SendFailure sends a failure message to the client.
func SendFailure(rw http.ResponseWriter, format string, args ...interface{}) {
	var result = struct {
		Status  string `json:"status"`
		Message string `json:"message"`
	}{
		Status:  "failure",
		Message: fmt.Sprintf(format, args...),
	}
	SendJSON(rw, http.StatusOK, &result)
}

// IPFromRequest extracts the IP from the given request.

func iPFromRequest(req *http.Request) net.IP {

	addr := realIP(req)
	if addr == "" {
		addr = req.RemoteAddr
	}
	idx := strings.LastIndexByte(addr, ':')
	if idx > 0 {
		addr = addr[:idx]
	}
	addr = strings.Trim(addr, "[]")
	ip := net.ParseIP(addr)
	if ip == nil {
		return nil
	}

	return ip
}

// See https://github.com/zenazn/goji/blob/master/web/middleware/realip.go
var (
	xForwardedFor = http.CanonicalHeaderKey("X-Forwarded-For")
	xRealIP       = http.CanonicalHeaderKey("X-Real-IP")
)

func realIP(r *http.Request) string {
	var ip string

	if xff := r.Header.Get(xForwardedFor); xff != "" {
		i := strings.Index(xff, ", ")
		if i == -1 {
			i = len(xff)
		}
		ip = xff[:i]
	} else if xrip := r.Header.Get(xRealIP); xrip != "" {
		ip = xrip
	}

	return ip
}

func LogUserAction(req *http.Request, text string) error {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		log.Println("logging failed: no session")
	}

	userID, ok := s.GetUser()
	if !ok {
		log.Println("logging failed: no user for session")
	}

	logEntry := models.Logging{
		Text: text,
		Timestamp: &pgtype.Timestamptz{
			Time:   time.Now(),
			Status: pgtype.Present,
		},
		IP: realIP(req),
	}
	if _, err := models.CreateUserLog(logEntry, userID); err != nil {
		log.Println("writing log failed: ", err)
		return err
	}
	return nil
}
