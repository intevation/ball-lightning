// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"net/http"

	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/version"
)

// getVersion controls the endpoint of /version (GET) and returns the version
func getVersion(req *http.Request) (m.JSONResult, error) {
	return m.JSONResult{
		Result: struct {
			Version string `json:"version"`
		}{
			Version: version.Version,
		},
		Code: http.StatusOK,
	}, nil
}
