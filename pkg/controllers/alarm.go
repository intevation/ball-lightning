// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
)

// getAlarmByUser controls the endpoint of /user/$ID/alarm (GET) and returns
// all alarms that belong to the user identified by the id.
func getAlarmByUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var userID int64
	var err error
	var alarms []models.Alarm

	if userID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			alarms, err = models.LoadAlarms(ctx, conn, userID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Code: http.StatusNotFound,
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
			}, nil
		}

		return m.JSONResult{
			Result: alarms,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{Result: struct{}{}, Code: 400}, err
}

// getAlarmByID controls the endpoint of /user/$ID/alarm/$AlarmID (GET) and
// returns a single alarm identified by the combination of user ID and alarm ID.
func getAlarmByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarm *models.Alarm
	var alarmID int64

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			alarm, err = models.LoadAlarm(ctx, conn, alarmID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Code: http.StatusNotFound,
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
			}, nil
		}

		return m.JSONResult{
			Result: alarm,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{Result: struct{ Err error }{Err: err}, Code: 400}, err
}

// getAlarmsByGroup controls the endpoint of /group/$ID/alarm (GET) and returns
// all alarms associated with the specified group.
func getAlarmsByGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var groupID int64
	var err error
	var alarms []models.Alarm

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			alarms, err = models.LoadAlarmByGroup(ctx, conn, groupID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Code: http.StatusNotFound,
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
			}, nil
		}

		return m.JSONResult{
			Result: alarms,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{Result: struct{}{}, Code: 400}, err
}

// getAlarmByGroupByID controls the endpoint of /group/$ID/alarm/$AlarmID (GET) and
// returns a single alarm specified by the combination of groupID and alarmID.
func getAlarmByGroupByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var groupID, alarmID int64
	var err error
	var alarms []models.Alarm

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				alarms, err = models.LoadAlarmByGroupByID(ctx, conn, groupID, alarmID)
				return err
			})
			if err != nil {
				return m.JSONResult{
					Code: http.StatusNotFound,
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
				}, nil
			}

			return m.JSONResult{
				Result: alarms,
				Code:   http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createAlarm controls the endpoint of /group/$ID/alarm (POST) and inserts the
// new alarm into the database.
func createAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error
	var groupID int64

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return m.JSONResult{Result: struct{}{}, Code: 400}, nil
	}

	input, _ := m.JSONInput(req).(*models.Alarm)

	if groupID != input.GroupID {
		return m.JSONResult{Result: struct{}{}, Code: 400}, nil
	}

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.CreateAlarm(ctx, conn, *input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Code: http.StatusNotFound,
			Result: struct {
				Message string
			}{
				Message: err.Error(),
			},
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// updateAlarm controls the endpoint of /group/$ID/alarm (PUT) and updates the
// alarm given in the request body.
func updateAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Alarm)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateAlarm(ctx, conn, *input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Code: http.StatusNotFound,
			Result: struct {
				Message string
			}{
				Message: err.Error(),
			},
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

func addAlarmToUser(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error
	var userID int64

	if userID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return m.JSONResult{Result: struct{ Msg string }{Msg: err.Error()}, Code: 400}, err
	}
	input, _ := m.JSONInput(req).(*models.Alarm)

	var hasAlarm bool
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		hasAlarm = models.HasAlarm(ctx, conn, userID, input.ID)
		return nil
	})
	if hasAlarm {
		return m.JSONResult{
			Code: http.StatusOK,
			Result: struct {
				Message string
			}{
				Message: fmt.Sprintln("User has alarm"),
			},
		}, nil
	}
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.AddAlarmToUser(ctx, conn, userID, *input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Code: http.StatusBadRequest,
			Result: struct {
				Message string
			}{
				Message: err.Error(),
			},
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteAlarm controls the endpoint of /user/$ID/alarm/$AlarmID (DELETE) and
// removes the mapping between the user and the alarm_area_unit.
func deleteAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var userID int64
	var alarmID int64

	if userID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				return models.DeleteAlarm(ctx, conn, alarmID, userID)
			})
			if err != nil {
				return m.JSONResult{
					Code: http.StatusNotFound,
					Result: struct {
						Message string
					}{
						Message: err.Error(),
					},
				}, nil
			}

			return m.JSONResult{
				Result: struct{}{},
				Code:   http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// deleteAlarmByID controls the endpoint of /group/$ID/alarm/$alarmID (DELETE)
// and removes the alarm_area_unit specified by groupID and alarmID
func deleteAlarmByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var groupID int64
	var alarmID int64

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				return models.DeleteAlarmByGroupID(ctx, conn, alarmID, groupID)
			})
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}
