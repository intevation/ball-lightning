// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
)

// getThemes controls the endpoint of /theme (GET) and returns the client theme.
func getThemes(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	type resultTheme struct {
		models.Theme
		Data interface{} `json:"data"`
	}
	var resThemes []resultTheme
	var themes []models.Theme
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		themes, err = models.LoadThemes(ctx, conn)
		for _, t := range themes {
			resThemes = append(resThemes, resultTheme{
				Theme: t,
				Data:  t.Data.Get(),
			})
		}
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}
	return m.JSONResult{
		Result: resThemes,
		Code:   http.StatusOK,
	}, nil
}

// getHostTheme controls the endpoint of /version (GET) and returns the theme for the host.
func getHostTheme(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	host := req.Host

	var theme *models.HostTheme
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		theme, err = models.LoadHostTheme(ctx, conn, host)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}
	return m.JSONResult{
		Result: theme.Data.Get(),
		Code:   http.StatusOK,
	}, nil
}
