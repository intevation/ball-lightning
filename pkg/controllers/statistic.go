// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
)

// getStrokeArea controls the endpoint if /group/$ID/strokearea (GET) and
// returns all stroke areas associated with the specified group.
func getStatistics(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var statistic *models.Statistic
	var err error

	var timerange struct {
		start int64
		end   int64
	}
	for _, tr := range []struct {
		t *int64
		k string
	}{
		{&timerange.start, "start"},
		{&timerange.end, "end"},
	} {
		if *tr.t, err = strconv.ParseInt(req.FormValue(tr.k), 10, 64); err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
	}

	var coordinates struct {
		left   float64
		top    float64
		right  float64
		bottom float64
	}
	for _, co := range []struct {
		c *float64
		k string
	}{
		{&coordinates.left, "left"},
		{&coordinates.top, "top"},
		{&coordinates.right, "right"},
		{&coordinates.bottom, "bottom"},
	} {
		if *co.c, err = strconv.ParseFloat(req.FormValue(co.k), 64); err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

	}

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		statistic, err = models.LoadStatistics(
			ctx,
			conn,
			timerange.start,
			timerange.end,
			coordinates.left,
			coordinates.right,
			coordinates.top,
			coordinates.bottom)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: statistic,
		Code:   http.StatusOK,
	}, nil
}
