// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

// getRoles controls the endpoint of /role (GET) and returns the roles of the
// currently logged in user.
func getRoles(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No session found"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	id, ok := s.GetUser()
	if !ok {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: fmt.Sprintln("No user for session"),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	var roles *[]models.Role
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		roles, err = models.LoadRolesForUser(ctx, conn, id)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: roles,
		Code:   http.StatusOK,
	}, nil
}

// getRoleByID controls the endpoint of /role/$ID (GET) and returns a single
// role identified by id.
func getRoleByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		var role *models.Role
		var err error

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			role, err = models.LoadRoleByID(ctx, conn, id)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: role,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createRole controls the endpoint of /role (POST) and inserts the role given
// in the request body into the database.
func createRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Role)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.CreateRole(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// updateRole controls the endpoint of /role (PUT) and updates the attributes
// of a database item with the values given in the request body.
func updateRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Role)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateRole(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteRole controls the endpoint of /role/$ID (DELETE) and removes the role
// specified by id from the database.
func deleteRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			return models.DeleteRole(ctx, conn, id)
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
		return m.JSONResult{
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// addPermissionToRole controls the endpoint of /role/$ID/permission (PUT) and
// maps a permission to a role.
func addPermissionToRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var roleID int64

	if roleID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		input := m.JSONInput(req)
		if permission, ok := input.(*models.Permission); ok {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				return models.CreateRolePermission(ctx, conn, roleID, permission.ID)
			})
			if err != nil {
				return m.JSONResult{
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
					Code: http.StatusNotFound,
				}, nil
			}

			return m.JSONResult{
				Code: http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// removePermissionFromRole controls the endpoint of /role/$ID/permission/$ID
// (DELETE) and removes the mapping between a role and a permission.
func removePermissionFromRole(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var roleID, permissionID int64

	if roleID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if permissionID, err = strconv.ParseInt(vars["pid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				return models.DeleteRolePermission(ctx, conn, roleID, permissionID)
			})
			if err != nil {
				return m.JSONResult{
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
					Code: http.StatusNotFound,
				}, nil
			}
			return m.JSONResult{
				Code: http.StatusOK,
			}, nil
		}
	}
	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}
