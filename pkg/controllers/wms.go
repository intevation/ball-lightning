// Copyright 2018, 2019, 2020 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license

package controllers

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"bitbucket.org/intevation/ball-lightning/pkg/misc"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
)

const defaultRemote = "http://127.0.0.1:8080/geoserver/"

var (
	wmsMu  sync.Mutex
	wmsURL int

	wmsRemoteURLs = func() []*url.URL {
		servers := misc.Env("GEOSERVER", defaultRemote)
		var urls []*url.URL
		for _, s := range strings.Split(servers, ";") {
			u, err := url.Parse(s)
			if err != nil {
				panic(fmt.Errorf("Invalid GEOSERVER URL %s: %v", s, err))
			}
			urls = append(urls, u)
		}
		return urls
	}()
)

func nexWMSRemoteURL() *url.URL {
	switch len(wmsRemoteURLs) {
	case 0:
		return nil
	case 1:
		return wmsRemoteURLs[0]
	}

	wmsMu.Lock()
	defer wmsMu.Unlock()

	url := wmsRemoteURLs[wmsURL]
	wmsURL = (wmsURL + 1) % len(wmsRemoteURLs)
	return url
}

// parseQuery is a modified version of the internal query
// parser of the url.parseQuery of the standard library.
func parseQuery(
	m url.Values,
	query string,
	keySep, valueSep string,
	unescape func(string) (string, error)) error {

	for query != "" {
		key := query
		if i := strings.Index(key, keySep); i >= 0 {
			key, query = key[:i], key[i+1:]

		} else {
			query = ""
		}
		if key == "" {
			continue
		}
		value := ""
		if i := strings.Index(key, valueSep); i >= 0 {
			key, value = key[:i], key[i+1:]
		}
		key, err := unescape(key)
		if err != nil {
			return err
		}
		value, err = unescape(value)
		if err != nil {
			return err
		}
		m[key] = append(m[key], value)
	}
	return nil
}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}

func concat(params url.Values) string {
	var sb strings.Builder
	for k, v := range params {
		if sb.Len() > 0 {
			sb.WriteByte(';')
		}
		sb.WriteString(k)
		sb.WriteByte(':')
		var s string
		if len(v) > 0 {
			s = v[0]
		}
		sb.WriteString(s)
	}
	return sb.String()
}

func wmsDirector(req *http.Request) {
	log.Println("called /wms (director): GET")

	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		log.Printf("error: no tracked session")
		panic(http.ErrAbortHandler)
	}

	userID, ok := s.GetUser()
	if !ok {
		log.Printf("error: no logged in user")
		panic(http.ErrAbortHandler)
	}

	// VIEWPARAMS contains ';' as key separators.
	// If we would use req.URL.Query() this would be split
	// at the wrong level resulting in broken key/value pairs.
	// So we do the splitting ourselves.

	geoserver := make(url.Values)
	if err := parseQuery(
		geoserver, req.URL.RawQuery, "&", "=", url.QueryUnescape); err != nil {
		log.Printf("error: %v\n", err)
		panic(http.ErrAbortHandler)
	}

	geoserver.Del("source_url")

	viewparam := make(url.Values)

	viewparam.Set("userid", strconv.FormatInt(userID, 10))

	bbox := strings.Split(geoserver.Get("bbox"), ",")
	if len(bbox) < 4 {
		log.Println("error: too less BBOX parts")
		panic(http.ErrAbortHandler)
	}
	viewparam.Set("left", bbox[0])
	viewparam.Set("right", bbox[2])
	viewparam.Set("top", bbox[3])
	viewparam.Set("bottom", bbox[1])

	// log.Printf("left: %s\n", viewparam.Get("left"))
	// log.Printf("right: %s\n", viewparam.Get("right"))
	// log.Printf("top: %s\n", viewparam.Get("top"))
	// log.Printf("bottom: %s\n", viewparam.Get("bottom"))

	// create VIEWPARAMS string
	geoserver.Set("VIEWPARAMS", concat(viewparam))

	req.URL.RawQuery = geoserver.Encode()

	wmsRemoteURL := nexWMSRemoteURL()

	targetQuery := wmsRemoteURL.RawQuery

	req.URL.Scheme = wmsRemoteURL.Scheme
	req.URL.Host = wmsRemoteURL.Host

	req.URL.Path = singleJoiningSlash(wmsRemoteURL.Path, req.URL.Path)

	if targetQuery == "" || req.URL.RawQuery == "" {
		req.URL.RawQuery = targetQuery + req.URL.RawQuery
	} else {
		req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
	}

	if _, ok := req.Header["User-Agent"]; !ok {
		// explicitly disable User-Agent so it's not set to default value
		req.Header.Set("User-Agent", "")
	}

	// log.Println(req.URL.String())
}
