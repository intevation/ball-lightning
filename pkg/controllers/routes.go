// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"net/http"
	"net/http/httputil"

	"github.com/gorilla/mux"

	"bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	s "bitbucket.org/intevation/ball-lightning/pkg/session"
)

// BindRoutes binds all the API endpoints to the exposed router.
func BindRoutes(m *mux.Router) error {

	loggedIn := s.Checking(s.LoggedIn, http.StatusUnauthorized)

	// Standard chain of having a session and being logged in.
	std := func(h http.Handler) http.Handler {
		return s.Chain(middleware.PanicHandler, s.Sessioned, loggedIn, s.Term(h))
	}

	auth := m.PathPrefix("/auth").Subrouter()

	authGET := auth.Methods(http.MethodGet).Subrouter()
	authGET.Handle("/logout", std(http.HandlerFunc(logout)))

	authPOST := auth.Methods(http.MethodPost).Subrouter()
	authPOST.Handle("/login", s.Chain(s.Sessioned, s.Term(http.HandlerFunc(login))))

	proxy := httputil.ReverseProxy{Director: wmsDirector}
	m.PathPrefix("/wms").Methods("GET").Handler(std(http.HandlerFunc(proxy.ServeHTTP)))

	// Password resets.
	m.Handle("/passwordreset", &middleware.JSONHandler{
		Input:  func(*http.Request) interface{} { return new(models.PWResetUser) },
		Handle: passwordResetRequest,
	}).Methods(http.MethodPost)

	m.HandleFunc("/passwordreset/{hash}", passwordReset).
		Methods(http.MethodGet)

	m.Handle("/strokestream", s.Chain(middleware.PanicHandler, s.Sessioned, middleware.PermissionHandler, s.Term(http.HandlerFunc(strokeStream))))
	m.Handle("/strokestreamclose", s.Chain(middleware.PanicHandler, s.Sessioned, middleware.PermissionHandler, s.Term(http.HandlerFunc(strokeStreamClose))))

	m.Handle("/alarmstream", s.Chain(middleware.PanicHandler, s.Sessioned, middleware.PermissionHandler, s.Term(http.HandlerFunc(alarmStream))))
	m.Handle("/alarmstreamclose", s.Chain(middleware.PanicHandler, s.Sessioned, middleware.PermissionHandler, s.Term(http.HandlerFunc(alarmStreamClose))))

	baseURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return &middleware.JSONHandler{
			Handle: handle,
		}
	}
	userIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.User) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	groupIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.Group) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	appSettingIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.AppSettings) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	strokeAreaIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.StrokeArea) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	profileIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(interface{}) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
				Limit:  4096,
			})))
	}
	alarmSettingsIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.AlarmSettings) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	roleIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.RoleHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.Role) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	permissionIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.RoleHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.Permission) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	alarmIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			middleware.AlarmHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.Alarm) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	areaIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Create a new Instance of the requested object. Will be filled using JSONInput
				Input: func(*http.Request) interface{} { return new(models.AlarmArea) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	requestURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	groupURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	roleURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.RoleHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	alarmURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.GroupHandler,
			middleware.AlarmHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	layerURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	layerIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				Input: func(*http.Request) interface{} { return new(models.Layer) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	groupLayerIn := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				Input: func(*http.Request) interface{} { return new(models.LayerForGroup) },
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	statisticURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			middleware.StrokeHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	templateURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}
	loggingURL := func(handle func(*http.Request) (middleware.JSONResult, error)) http.Handler {
		return std(s.Chain(
			middleware.PermissionHandler,
			s.Term(&middleware.JSONHandler{
				// Handler implementing the functionality of the concrete controller
				Handle: handle,
			})))
	}

	rest := map[string]*REST{
		"/user": {
			GET:  groupURL(getUser),
			POST: userIn(createUser),
			PUT:  userIn(updateUser),
		},
		"/user/{id:[0-9]+}": {
			GET:    groupURL(getUserByID),
			DELETE: groupURL(deleteUser),
		},
		"/user/{id:[0-9]+}/profile": {
			PUT: profileIn(updateProfile),
		},
		"/user/{id:[0-9]+}/alarmsettings": {
			GET: groupURL(getAlarmSettings),
			PUT: alarmSettingsIn(updateAlarmSettings),
		},
		"/user/{id:[0-9]+}/alarm": {
			GET: groupURL(getAlarmByUser),
			PUT: alarmIn(addAlarmToUser),
		},
		"/user/{id:[0-9]+}/alarm/{aid:[0-9]+}": {
			GET:    alarmURL(getAlarmByID),
			DELETE: alarmURL(deleteAlarm),
		},
		"/group": {
			GET:  groupURL(getGroup),
			POST: groupIn(createGroup),
			PUT:  groupIn(updateGroup),
		},
		"/group/{id:[0-9]+}": {
			GET:    groupURL(getGroupByID),
			DELETE: groupURL(deleteGroup),
		},
		"/group/{id:[0-9]+}/settings": {
			GET: groupURL(getAppSettings),
			PUT: appSettingIn(updateAppSettings),
		},
		"/group/{id:[0-9]+}/strokearea": {
			GET:  groupURL(getStrokeArea),
			PUT:  strokeAreaIn(updateStrokeArea),
			POST: strokeAreaIn(createStrokeArea),
		},
		"/group/{id:[0-9]+}/alarm": {
			GET:  alarmURL(getAlarmsByGroup),
			POST: alarmIn(createAlarm),
			PUT:  alarmIn(updateAlarm),
		},
		"/group/{id:[0-9]+}/alarm/{aid:[0-9]+}": {
			GET:    alarmURL(getAlarmByGroupByID),
			DELETE: alarmURL(deleteAlarmByID),
		},
		"/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area": {
			GET:  alarmURL(getAlarmAreaByGroup),
			PUT:  areaIn(updateAlarmArea),
			POST: areaIn(createAlarmArea),
		},
		"/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area/{aaid:[0-9]+}": {
			GET:    alarmURL(getAreaByID),
			DELETE: alarmURL(deleteAreaFromAlarm),
		},
		"/role": {
			GET:  requestURL(getRoles),
			POST: roleIn(createRole),
			PUT:  roleIn(updateRole),
		},
		"/role/{id:[0-9]+}": {
			GET:    roleURL(getRoleByID),
			DELETE: roleURL(deleteRole),
		},
		"/permission": {
			GET: requestURL(getPermissions),
		},
		"/permission/{pid:[0-9]+}": {
			GET: requestURL(getPermissionByID),
		},
		"/role/{id:[0-9]+}/permission": {
			GET: roleURL(getPermissionsByRole),
			PUT: permissionIn(addPermissionToRole),
		},
		"/role/{id:[0-9]+}/permission/{pid:[0-9]+}": {
			GET:    roleURL(getPermissionByID), // TODO Respect role id in this request!
			DELETE: roleURL(removePermissionFromRole),
		},
		"/layer": {
			GET:  layerURL(getLayers),
			PUT:  layerIn(updateLayer),
			POST: layerIn(createLayer),
		},
		"/layer/{id:[0-9]+}": {
			GET:    layerURL(getLayerByID),
			DELETE: layerURL(deleteLayer),
		},
		"/group/{id:[0-9]+}/maplayer": {
			GET:  groupURL(getGroupLayers),
			PUT:  groupLayerIn(updateGroupLayer),
			POST: groupLayerIn(createGroupLayer),
		},
		"/group/{id:[0-9]+}/maplayer/{lid:[0-9]+}": {
			GET:    groupURL(getGroupLayerByID),
			DELETE: groupURL(deleteGroupLayer),
		},
		"/statistic": {
			GET: statisticURL(getStatistics),
			Queries: map[string]string{
				"start":  "{start:[0-9]+}",
				"end":    "{end:[0-9]+}",
				"left":   "{left:[0-9]*\\.?[0-9]+}",
				"top":    "{top:[0-9]*\\.?[0-9]+}",
				"bottom": "{bottom:[0-9]*\\.?[0-9]+}",
				"right":  "{right:[0-9]*\\.?[0-9]+}",
			},
		},
		"/template/fax": {
			GET: templateURL(getTemplateFax),
		},
		"/template/mail": {
			GET: templateURL(getTemplateMail),
		},
		"/template/mqtt": {
			GET: templateURL(getTemplateMQTT),
		},
		"/template/phone": {
			GET: templateURL(getTemplatePhone),
		},
		"/template/sms": {
			GET: templateURL(getTemplateSMS),
		},
		"/userlog": {
			GET: loggingURL(getUserLogs),
		},
		"/version": {
			GET: loggingURL(getVersion),
		},
		"/hosttheme": {
			GET: baseURL(getHostTheme),
		},
		"/theme": {
			GET: loggingURL(getThemes),
		},
	}

	return BindREST(m, rest)
}
