// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
)

// getTemplateFax controls the endpoint if /template/fax (GET) and
// returns all templates of type fax.
func getTemplateFax(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var templates []models.Template
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		templates, err = models.LoadTemplateFax(
			ctx,
			conn,
		)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	if templates == nil {
		templates = make([]models.Template, 0)
	}
	return m.JSONResult{
		Result: templates,
		Code:   http.StatusOK,
	}, nil
}

// getTemplateMail controls the endpoint if /template/mail (GET) and
// returns all templates of type mail.
func getTemplateMail(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var templates []models.Template
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		templates, err = models.LoadTemplateMail(
			ctx,
			conn,
		)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	if templates == nil {
		templates = make([]models.Template, 0)
	}
	return m.JSONResult{
		Result: templates,
		Code:   http.StatusOK,
	}, nil
}

// getTemplateMQTT controls the endpoint if /template/mqtt (GET) and
// returns all templates of type mqtt.
func getTemplateMQTT(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var templates []models.Template
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		templates, err = models.LoadTemplateMQTT(
			ctx,
			conn,
		)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	if templates == nil {
		templates = make([]models.Template, 0)
	}
	return m.JSONResult{
		Result: templates,
		Code:   http.StatusOK,
	}, nil
}

// getTemplatePhone controls the endpoint if /template/phone (GET) and
// returns all templates of type phone.
func getTemplatePhone(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var templates []models.Template
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		templates, err = models.LoadTemplatePhone(
			ctx,
			conn,
		)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	if templates == nil {
		templates = make([]models.Template, 0)
	}
	return m.JSONResult{
		Result: templates,
		Code:   http.StatusOK,
	}, nil
}

// getTemplateSMS controls the endpoint if /template/sms (GET) and
// returns all templates of type sms.
func getTemplateSMS(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var templates []models.Template
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		templates, err = models.LoadTemplateSMS(
			ctx,
			conn,
		)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	if templates == nil {
		templates = make([]models.Template, 0)
	}
	return m.JSONResult{
		Result: templates,
		Code:   http.StatusOK,
	}, nil
}
