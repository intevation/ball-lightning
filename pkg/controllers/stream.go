// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"log"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/broadcast"
	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/mb0/wkt"
)

const (
	strokeStreamHeartbeatType = "hb-light"
	alarmHeartbeatType        = "hb-alarm"
	strokeStreamType          = "stroke"
	alarmType                 = "Alarm"
	allClearType              = "Entwarnung"
	testAlarmType             = "Testalarm"
	testAllClearType          = "Testentwarnung"
)

var strokeBroker *broadcast.StrokeStreamBroker
var alarmBroker *broadcast.AlarmStreamBroker

// StartStrokeStream starts the server side streaming service for strokes
func StartStrokeStream() error {
	dsn, err := database.DSN()
	if err != nil {
		return err
	}
	strokeBroker = broadcast.NewStrokeStreamBroker()
	go strokeBroker.Run()
	rcv := broadcast.NewReceiver(dsn)
	rcv.AddHandler(strokeStreamType, strokeBroker.Handle)
	rcv.AddHandler(strokeStreamHeartbeatType, strokeBroker.HandleHeartbeat)
	go rcv.Run()
	session.AddSessionDeathCallback(strokeBroker.SessionDied)
	return nil
}

// StartAlarmStream starts the server side streaming service for alarms
func StartAlarmStream() error {
	dsn, err := database.DSN()
	if err != nil {
		return err
	}
	alarmBroker = broadcast.NewAlarmStreamBroker()
	go alarmBroker.Run()
	rcv := broadcast.NewReceiver(dsn)
	rcv.AddHandler(alarmType, alarmBroker.HandleAlarm)
	rcv.AddHandler(alarmHeartbeatType, alarmBroker.HandleHeartbeat)
	rcv.AddHandler(allClearType, alarmBroker.HandleAllClear)
	rcv.AddHandler(testAlarmType, alarmBroker.HandleTestAlarm)
	rcv.AddHandler(testAllClearType, alarmBroker.HandleTestAllClear)
	go rcv.Run()
	session.AddSessionDeathCallback(alarmBroker.SessionDied)
	return nil
}

// strokeStream: GET
func strokeStream(rw http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		SendFailure(rw, "No session found")
		return
	}

	userID, ok := s.GetUser()
	if !ok {
		SendFailure(rw, "No user found")
		return
	}

	var geom wkt.Geom
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		geom, err = models.LoadStrokeAreaAsWKT(ctx, conn, userID)
		return err
	})
	if err != nil {
		SendFailure(rw, err.Error())
		return
	}
	if geom == nil {
		http.NotFound(rw, req)
		return
	}

	flusher, closeNotifier, ok := setupStreaming(rw)
	if !ok {
		return
	}

	queue := broadcast.NewQueue()

	strokeBroker.RegisterClient(s.Key(), geom, queue)

	snd := sender{
		eventType:     "lightning",
		queue:         queue,
		flusher:       flusher,
		closeNotifier: closeNotifier,
		unregister:    func() { strokeBroker.UnregisterClient(s.Key(), queue) },
	}
	snd.sendTo(rw)
}

// eventclose: GET
func strokeStreamClose(rw http.ResponseWriter, req *http.Request) {
	log.Println("called pointstream/eventclose: GET")
	// XXX: Is this too extreme?
	s, ok := session.FromContext(req.Context())
	if !ok {
		SendEmptyJSON(rw, http.StatusBadRequest)
	}
	strokeBroker.SessionDied(s.Key())
	SendEmptyJSON(rw, http.StatusOK)
}

func alarmStream(rw http.ResponseWriter, req *http.Request) {
	log.Println("called alarmstream/alarmEvent: GET")

	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		SendFailure(rw, "No session found")
		return
	}

	userID, ok := s.GetUser()
	if !ok {
		SendFailure(rw, "No user found")
		return
	}

	var user *models.User
	var err error
	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		user, err = models.LoadUser(ctx, conn, userID)
		return err
	})
	if err != nil {
		SendFailure(rw, err.Error())
		return
	}
	if user == nil {
		http.NotFound(rw, req)
		return
	}

	flusher, closeNotifier, ok := setupStreaming(rw)
	if !ok {
		return
	}

	queue := broadcast.NewQueue()

	alarmBroker.RegisterClient(ctx, s.Key(), *user.GroupID, queue)

	snd := sender{
		eventType:     "lightning",
		queue:         queue,
		flusher:       flusher,
		closeNotifier: closeNotifier,
		unregister:    func() { alarmBroker.UnregisterClient(s.Key(), queue) },
	}
	snd.sendTo(rw)
}

func alarmStreamClose(rw http.ResponseWriter, req *http.Request) {
	log.Println("called alarmstream/alarmEventClose: GET")
	// XXX: Is this too extreme?
	s, ok := session.FromContext(req.Context())
	if !ok {
		SendEmptyJSON(rw, http.StatusBadRequest)
	}
	alarmBroker.SessionDied(s.Key())
	SendEmptyJSON(rw, http.StatusOK)
}

func setupStreaming(rw http.ResponseWriter) (http.Flusher, http.CloseNotifier, bool) {
	// Make sure that the writer supports flushing.
	flusher, ok := rw.(http.Flusher)
	if !ok {
		http.Error(rw, "Streaming unsupported!", http.StatusInternalServerError)
		return nil, nil, false
	}

	closeNotifier, ok := rw.(http.CloseNotifier)
	if !ok {
		http.Error(rw, "CloseNotifier unsupported!", http.StatusInternalServerError)
		return nil, nil, false
	}

	rw.Header().Set("Content-Type", "text/event-stream")
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Connection", "keep-alive")
	rw.Header().Set("Access-Control-Allow-Origin", "*")

	return flusher, closeNotifier, true
}

type sender struct {
	eventType     string
	queue         *broadcast.Queue
	flusher       http.Flusher
	closeNotifier http.CloseNotifier
	unregister    func()
}

var sseSuffix = []byte("\n\n")

func (s *sender) sendTo(rw http.ResponseWriter) {
	type msg struct {
		msg []byte
		ok  bool
	}

	notify := s.closeNotifier.CloseNotify()
	msgs := make(chan msg)

	go func() {
		for {
			var m msg
			m.msg, m.ok = s.queue.Remove()
			msgs <- m
			if !m.ok {
				break
			}
		}
		close(msgs)
	}()

	prefix := []byte("event: " + s.eventType + "\ndata: ")

	write := func(msg []byte) (err error) {
		if _, err = rw.Write(prefix); err != nil {
			return
		}
		if _, err = rw.Write(msg); err != nil {
			return
		}
		_, err = rw.Write(sseSuffix)
		return
	}

	var dying bool
	for {
		select {
		case m := <-msgs:
			if !m.ok {
				return
			}

			if !dying {
				if write(m.msg) != nil {
					dying = true
					s.unregister()
				} else {
					s.flusher.Flush()
				}
			}

		case <-notify:
			dying = true
			s.unregister()
		}
	}
}
