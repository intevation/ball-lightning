// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
)

// getStrokeArea controls the endpoint if /group/$ID/strokearea (GET) and
// returns all stroke areas associated with the specified group.
func getStrokeArea(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var groupID int64
	var err error
	var area *models.StrokeArea

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			area, err = models.LoadStrokeAreaByGroup(ctx, conn, groupID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: area,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// updateStrokeArea controls the endpoint of /group/$ID/strokearea (PUT) and
// updates the stroke area in the database with the attributes given in the
// reqest body.
func updateStrokeArea(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error
	var groupID int64

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return m.JSONResult{Result: struct{}{}, Code: 400}, nil
	}

	input, _ := m.JSONInput(req).(*models.StrokeArea)

	if groupID == input.GroupID {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			id, err = models.UpdateStrokeArea(ctx, conn, input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: struct {
				ID int64 `json:"id"`
			}{
				ID: id,
			},
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createStrokeArea controls the endpoint of /group/$ID/strokearea (POST) and
// inserts the stroke area given in the request body into the database.
func createStrokeArea(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error
	var groupID int64

	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return m.JSONResult{Result: struct{}{}, Code: 400}, nil
	}

	input, _ := m.JSONInput(req).(*models.StrokeArea)

	if groupID == input.GroupID {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			id, err = models.CreateStrokeArea(ctx, conn, input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: struct {
				ID int64 `json:"id"`
			}{
				ID: id,
			},
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getAreaByAlarm controls the endpoint of /user/$ID/alarm/$alarmID/area (GET)
// and returns the alarm area specified by the userID and the alarmID.
func getAreaByAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarmID int64
	var areas []models.AlarmArea

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			areas, err = models.LoadAlarmAreas(ctx, conn, alarmID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		if areas == nil {
			areas = make([]models.AlarmArea, 0)
		}
		return m.JSONResult{
			Result: areas,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getAreaByID controls the endpoint of /user/$ID/alarm/$AlarmID/area/$AreaID
// (GET) and returns a single alarm area specified by the IDs given in the
// request URL.
func getAreaByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var areaID int64
	var err error
	var area *models.AlarmArea

	if areaID, err = strconv.ParseInt(vars["aaid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			area, err = models.LoadAlarmAreaByID(ctx, conn, areaID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: area,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

func getAlarmAreaByGroup(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarmID int64
	var areas []models.AlarmArea

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			areas, err = models.LoadAlarmAreas(ctx, conn, alarmID)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		if areas == nil {
			areas = make([]models.AlarmArea, 0)
		}
		return m.JSONResult{
			Result: areas,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createAlarmArea controls the endpoint of /group/$ID/alarm/$AlarmID/area
// (POST) and inserts the alarm area given in the request body into the
// database.
func createAlarmArea(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarmID int64
	var areaID int64

	input, _ := m.JSONInput(req).(*models.AlarmArea)

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			areaID, err = models.CreateAlarmArea(ctx, conn, alarmID, *input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: struct {
				ID int64 `json:"id"`
			}{
				ID: areaID,
			},
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// updateAlarmArea controls the endpoint of /group/$ID/alarm/$AlarmID/area
// (PUT) and inserts the alarm area given in the request body into the
// database.
func updateAlarmArea(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var err error
	var areaID int64

	input, _ := m.JSONInput(req).(*models.AlarmArea)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		areaID, err = models.UpdateAlarmArea(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	vars := mux.Vars(req)
	var alarmID int64
	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			if hasArea := models.AlarmHasArea(ctx, conn, input.ID, areaID); !hasArea {
				return models.AddAreaToUnit(ctx, conn, alarmID, *input)
			}
			return nil
		})
	}
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: areaID,
		},
		Code: http.StatusOK,
	}, nil
}

// addAreaToAlarm controls the endpoint of /user/$ID/alarm/$AlarmID/area (PUT)
// and maps the specified alarm to the area given in the request body.
func addAreaToAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarmID int64

	input, _ := m.JSONInput(req).(*models.AlarmArea)

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			err = models.AddAreaToUnit(ctx, conn, alarmID, *input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// deleteAreaFromAlarm controls the endpoint of /user/$ID/alarm/$AlarmID/area/$AreaID
// (DELETE) and removes the mapping between the given alarm and area.
func deleteAreaFromAlarm(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var alarmID int64

	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err == nil {
		if areaID, err := strconv.ParseInt(vars["aaid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				err = models.RemoveAreaFromUnit(ctx, conn, alarmID, areaID)
				return err
			})
			if err != nil {
				return m.JSONResult{
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
					Code: http.StatusNotFound,
				}, nil
			}

			return m.JSONResult{
				Code: http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}
