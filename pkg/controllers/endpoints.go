// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"reflect"
	"sort"
	"strings"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

// REST represents the mapping between the http method and a handler.
type REST struct {
	GET     http.Handler
	PUT     http.Handler
	POST    http.Handler
	DELETE  http.Handler
	Queries map[string]string
}

const (
	groupIDSSQL = `
SELECT group_id FROM users.users WHERE id = $1
`

	parentIDSQL = `
SELECT parent FROM users.groups WHERE id = $1
`

	roleIDSQL = `
SELECT role_id FROM users.group_roles
WHERE user_id = $1 AND group_id = $2
`

	roleHasActivitySQL = `
SELECT
  EXISTS (SELECT 1 FROM
      users.activity a
      JOIN users.permission_activity pa ON pa.activity_id = a.id
      JOIN users.permissions p          ON pa.permission_id = p.id
      JOIN users.role_permissions rp    ON rp.permission_id = p.id
      JOIN users.roles r                ON rp.role_id = r.id
    WHERE
      r.id = 1
    )
`
)

// BindREST makes a lookup in the database to configure the REST endpoint
// and binds the handler to the given route
func BindREST(m *mux.Router, rests map[string]*REST) error {

	ctx := context.Background()

	var activities []*models.Activity

	if err := database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		var err error
		activities, err = models.LoadAllActivities(ctx, conn)
		return err
	}); err != nil {
		return errors.WithStack(err)
	}

	pathToAct := make(map[string][]*models.Activity)

	for _, act := range activities {
		pathToAct[act.URL] = append(pathToAct[act.URL], act)
	}

	for path, rest := range rests {
		acts := pathToAct[path]
		if len(acts) == 0 {
			log.Printf("WARN: Cannot configure REST '%s'. Missing in DB.\n", path)
			continue
		}
		// Delete to find out later what's missing.
		delete(pathToAct, path)

		find := func(method string) *models.Activity {
			for _, act := range acts {
				if act.Method == method {
					return act
				}
			}
			return nil
		}

		for _, r := range []struct {
			method  string
			handler http.Handler
		}{
			{"GET", rest.GET},
			{"PUT", rest.PUT},
			{"POST", rest.POST},
			{"DELETE", rest.DELETE},
		} {
			if r.handler == nil {
				continue
			}
			act := find(r.method)
			if act == nil {
				log.Printf("WARN: Cannot configure %s '%s'. Missing in DB.\n", r.method, path)
				continue
			}
			if rest.Queries != nil && r.method == "GET" {
				keys := reflect.ValueOf(rest.Queries).MapKeys()
				var queries []string
				for _, k := range keys {
					queries = append(queries, k.String())
					queries = append(queries, rest.Queries[k.String()])
				}
				m.Handle(path, r.handler).Methods(r.method).Queries(queries...)
			} else {
				m.Handle(path, r.handler).Methods(r.method)
			}
		}
	}

	// Produce warnings for unbound handlers.

	if n := len(pathToAct); n > 0 {
		missing := make([]string, n)
		var i int
		for k := range pathToAct {
			missing[i] = "'" + k + "'"
			i++
		}
		sort.Strings(missing)
		log.Printf("WARN: Missing endpoints for DB activity paths: %s\n",
			strings.Join(missing, ", "))
	}

	return nil
}
