// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package controllers

import (
	"context"
	"database/sql"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	m "bitbucket.org/intevation/ball-lightning/pkg/middleware"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
)

// getLayers controls the endpoint of /layer (GET) and returns all map layers
func getLayers(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var layers []models.Layer
	var err error

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		layers, err = models.LoadMapLayers(ctx, conn)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: layers,
		Code:   http.StatusOK,
	}, nil
}

// getLayerByID controls the endpoint of /role/$ID (GET) and returns a single
// role identified by id.
func getLayerByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		var layer *models.Layer
		var err error

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			layer, err = models.LoadMapLayerByID(ctx, conn, id)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: layer,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createLayer controls the endpoint of /layer (POST) and inserts the layer given
// in the request body into the database.
func createLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Layer)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.CreateMapLayer(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusNotFound,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// updateLayer controls the endpoint of /layer (PUT) and updates the attributes
// of a database item with the values given in the request body.
func updateLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.Layer)

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		id, err = models.UpdateMapLayer(ctx, conn, input)
		return err
	})
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteLayer controls the endpoint of /layer/$ID (DELETE) and removes the layer
// specified by id from the database.
func deleteLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			return models.DeleteMapLayer(ctx, conn, id)
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
		return m.JSONResult{
			Code: http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getGroupLayers controls the endpoint of /layer (GET) and returns all map layers
func getGroupLayers(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		var layers []models.LayerForGroup
		var err error

		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			layers, err = models.LoadGroupMapLayers(ctx, conn, id)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}

		return m.JSONResult{
			Result: layers,
			Code:   http.StatusOK,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// getGroupLayerByID controls the endpoint of /role/$ID (GET) and returns a single
// role identified by id.
func getGroupLayerByID(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id, lid int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if lid, err = strconv.ParseInt(vars["lid"], 10, 64); err == nil {
			var layer *models.LayerForGroup
			var err error

			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				layer, err = models.LoadGroupMapLayerByID(ctx, conn, id, lid)
				return err
			})
			if err != nil {
				return m.JSONResult{
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
					Code: http.StatusNotFound,
				}, nil
			}
			return m.JSONResult{
				Result: layer,
				Code:   http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}

// createGroupLayer controls the endpoint of /layer (POST) and inserts the layer given
// in the request body into the database.
func createGroupLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id, lid int64
	var err error

	input, _ := m.JSONInput(req).(*models.LayerForGroup)

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			lid, err = models.CreateGroupMapLayer(ctx, conn, id, input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
	}
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: lid,
		},
		Code: http.StatusOK,
	}, nil
}

// updateGroupLayer controls the endpoint of /layer (PUT) and updates the attributes
// of a database item with the values given in the request body.
func updateGroupLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var id int64
	var err error

	input, _ := m.JSONInput(req).(*models.LayerForGroup)

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
			id, err = models.UpdateGroupMapLayer(ctx, conn, input)
			return err
		})
		if err != nil {
			return m.JSONResult{
				Result: struct {
					Message string `json:"message"`
				}{
					Message: err.Error(),
				},
				Code: http.StatusNotFound,
			}, nil
		}
	}
	if err != nil {
		return m.JSONResult{
			Result: struct {
				Message string `json:"message"`
			}{
				Message: err.Error(),
			},
			Code: http.StatusBadRequest,
		}, nil
	}

	return m.JSONResult{
		Result: struct {
			ID int64 `json:"id"`
		}{
			ID: id,
		},
		Code: http.StatusOK,
	}, nil
}

// deleteGroupLayer controls the endpoint of /layer/$ID (DELETE) and removes the layer
// specified by id from the database.
func deleteGroupLayer(req *http.Request) (m.JSONResult, error) {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var id, lid int64

	if id, err = strconv.ParseInt(vars["id"], 10, 64); err == nil {
		if lid, err = strconv.ParseInt(vars["lid"], 10, 64); err == nil {
			err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
				return models.DeleteGroupMapLayer(ctx, conn, id, lid)
			})
			if err != nil {
				return m.JSONResult{
					Result: struct {
						Message string `json:"message"`
					}{
						Message: err.Error(),
					},
					Code: http.StatusNotFound,
				}, nil
			}
			return m.JSONResult{
				Code: http.StatusOK,
			}, nil
		}
	}

	return m.JSONResult{
		Result: struct {
			Message string `json:"message"`
		}{
			Message: err.Error(),
		},
		Code: http.StatusBadRequest,
	}, nil
}
