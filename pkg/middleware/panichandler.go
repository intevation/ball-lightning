// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"fmt"
	"log"
	"net/http"
	"runtime/debug"
)

// PanicHandler middleware recovers from panic in underlying handlers
// and sends HTTP 500 to the client when panic happens.
func PanicHandler(next http.Handler) http.Handler {
	h := func(rw http.ResponseWriter, r *http.Request) {
		defer func() {
			if re := recover(); re != nil {
				log.Printf("%v", string(debug.Stack()))
				err := fmt.Sprintf("%v", re)
				http.Error(rw,
					err,
					http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(rw, r) // panic here will be recovered
	}

	return http.HandlerFunc(h)
}
