// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
)

const (
	checkGroupPath = `
WITH RECURSIVE search_graph(id, parent) AS (
	SELECT id, parent
	FROM users.groups
	WHERE id = $2
	UNION ALL
	SELECT g.id, g.parent
	FROM users.groups g
	JOIN search_graph p ON g.id = p.parent
)
SELECT
  EXISTS (SELECT 1 FROM search_graph WHERE parent = $1);
`
)

const (
	urlRegex      = `^/?(\w+)/?(\d+)?/?.*`
	subrouteRegex = `^/?(\w+)/(\d+)/(\w+)/(\d+).*`
)

// GroupHandler defines a middleware that checks, if the group of the logged in
// user is a the same or a parent group of the requested object.
func GroupHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if !checkGroupTree(req) {
			SendJSON(rw, http.StatusUnauthorized, "No permission on this user/group")
			return
		}
		next.ServeHTTP(rw, req)
	})
}

func checkGroupTree(req *http.Request) bool {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return false
	}

	userID, ok := s.GetUser()
	if !ok {
		return false
	}

	switch req.Method {
	case "GET":
		return permissionURL(ctx, req, userID)
	case "DELETE":
		return permissionURL(ctx, req, userID)
	case "PUT":
		return permissionBody(ctx, req, userID)
	case "POST":
		return permissionBody(ctx, req, userID)
	default:
		return false
	}
}

func checkTree(ctx context.Context, req *http.Request, userGroupID, itemGroupID int64) bool {
	allowed := false
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if userGroupID == itemGroupID {
				allowed = true
				return nil
			}
			if err := conn.QueryRowContext(ctx, checkGroupPath, userGroupID, itemGroupID).Scan(&allowed); err != nil {
				return err
			}
			return nil
		}); err != nil {
		return false
	}

	return allowed
}

func permissionURL(ctx context.Context, req *http.Request, userID int64) bool {
	reg := regexp.MustCompile(urlRegex)
	match := reg.FindStringSubmatch(req.URL.Path)

	var id int64
	var err error
	if len(match) == 3 && match[2] == "" {
		// The request has no futher parameters. User is allowed to use its own content.
		return true
	} else if len(match) > 2 {
		// Route has at least one id as second path part
		id, err = strconv.ParseInt(match[2], 10, 64)
		if err != nil {
			return false
		}
	} else {
		return false
	}

	var itemGroupID int64
	var ok bool
	if match[1] == "user" {
		if itemGroupID, ok = userIDtoGroupID(ctx, id); !ok {
			return false
		}
	} else if match[1] == "group" {
		itemGroupID = id
	}

	var userGroupID int64
	if userGroupID, ok = userIDtoGroupID(ctx, userID); !ok {
		return false
	}

	return checkTree(ctx, req, userGroupID, itemGroupID)
}

func permissionBody(ctx context.Context, req *http.Request, userID int64) bool {
	var input map[string]interface{}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return false
	}

	err = json.NewDecoder(bytes.NewReader(body)).Decode(&input)
	if err != nil {
		return false
	}

	req.Body = ioutil.NopCloser(bytes.NewReader(body))
	groupIDName := "groupId"
	if req.URL.Path == "/group" {
		if req.Method == "POST" {
			groupIDName = "parent"
		} else if req.Method == "PUT" {
			groupIDName = "id"
		}
	} else if strings.HasPrefix(req.URL.Path, "/group/") &&
		strings.Contains(req.URL.Path, "/alarm") {
		// We have a subroute with alarm, so the alarm has the group id in the url path
		reg := regexp.MustCompile(subrouteRegex)
		match := reg.FindStringSubmatch(req.URL.Path)
		if len(match) > 3 {
			input = make(map[string]interface{})
			input["groupId"], err = strconv.ParseFloat(match[4], 64)
			if err != nil {
				return false
			}
		}
	}

	if itemGroupID, hasGroup := input[groupIDName].(float64); hasGroup {
		var userGroupID int64
		var ok bool
		if userGroupID, ok = userIDtoGroupID(ctx, userID); !ok {
			return false
		}
		return checkTree(ctx, req, userGroupID, int64(itemGroupID))
	}

	return false
}

func userIDtoGroupID(ctx context.Context, userID int64) (int64, bool) {
	var user *models.User
	var err error
	if err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		user, err = models.LoadUser(ctx, conn, userID)
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		return -1, false
	}

	return *user.GroupID, true
}
