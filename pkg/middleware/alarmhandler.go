// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/models"
	"github.com/gorilla/mux"
)

const ()

// AlarmHandler represents a middleware that ensures the association user to
// alarm exists.
func AlarmHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		reg := regexp.MustCompile(urlRegex)
		match := reg.FindStringSubmatch(req.URL.Path)

		var allow bool
		if match[1] == "user" {
			allow = checkUserAlarmPermission(req)
		} else if match[1] == "group" {
			allow = checkGroupAlarmPermission(req)
		}

		if !allow {
			SendJSON(rw, http.StatusBadRequest, struct{}{})
			return
		}

		next.ServeHTTP(rw, req)
	})
}

// Check for the existance of a relation between user and alarm.
func checkUserAlarmPermission(req *http.Request) bool {
	ctx := req.Context()
	vars := mux.Vars(req)

	var err error
	var userID int64

	// Allow PUT since this method adds an alarm to the given user.
	// Permission is handled by the route
	if req.Method == "PUT" {
		return true
	}

	if userID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return false
	}

	var alarmID int64
	if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err != nil {
		return false
	}

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		if !models.HasAlarm(ctx, conn, userID, alarmID) {
			return fmt.Errorf("The user %d has no alarm %d", userID, alarmID)
		}
		return nil
	})
	if err != nil {
		return false
	}

	return true
}

// Check for the existance of a relation between group and alarm.
func checkGroupAlarmPermission(req *http.Request) bool {
	ctx := req.Context()
	vars := mux.Vars(req)

	// On a POST no alarm exists we can make a permission check with.
	// Permission is handled by the route.
	if req.Method == "POST" {
		return true
	}

	var err error
	var groupID int64
	if groupID, err = strconv.ParseInt(vars["id"], 10, 64); err != nil {
		return false
	}

	var alarmID int64
	if req.Method == "PUT" {
		var input map[string]interface{}

		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			return false
		}

		err = json.NewDecoder(bytes.NewReader(body)).Decode(&input)
		if err != nil {
			return false
		}

		req.Body = ioutil.NopCloser(bytes.NewReader(body))
		alarmID = int64(input["id"].(float64))
	} else if req.Method == "DELETE" {
		if alarmID, err = strconv.ParseInt(vars["aid"], 10, 64); err != nil {
			return false
		}
	} else if req.Method == "GET" {
		if aID, ok := vars["aid"]; ok {
			if alarmID, err = strconv.ParseInt(aID, 10, 64); err != nil {
				return false
			}
		} else {
			return true
		}
	}

	err = database.Execute(ctx, func(ctx context.Context, conn *sql.Conn) error {
		if !models.HasAlarmByGroup(ctx, conn, groupID, alarmID) {
			return fmt.Errorf("The group %d has no alarm %d", groupID, alarmID)
		}
		return nil
	})
	if err != nil {
		return false
	}

	return true
}
