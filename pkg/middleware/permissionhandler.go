// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"context"
	"database/sql"
	"net/http"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

const (
	destinationSQL = `
SELECT a.destination_url FROM
      users.activity a
      JOIN users.permission_activity pa ON pa.activity_id = a.id
      JOIN users.permissions p          ON pa.permission_id = p.id
      JOIN users.role_permissions rp    ON rp.permission_id = p.id
      JOIN users.roles r                ON rp.role_id = r.id
      JOIN users.user_roles ur          ON r.id = ur.role_id
    WHERE
      a.method = $1
      AND ur.user_id = $2
`
)

// PermissionHandler defines a middleware that matches the logged in user and the
// requested route against the role - permission - activity path for this user.
func PermissionHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if !checkPermission(req) {
			SendJSON(rw, http.StatusUnauthorized, "No permission on this route")
			return
		}
		next.ServeHTTP(rw, req)
	})
}

func checkPermission(req *http.Request) bool {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return false
	}

	userID, ok := s.GetUser()
	if !ok {
		return false
	}

	allowed := false
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			database.Query(ctx, conn, func(rows *sql.Rows) error {

				var destURL string
				if err := rows.Scan(&destURL); err != nil {
					return nil
				}

				route := mux.CurrentRoute(req)
				if reg, err := route.GetPathTemplate(); err == nil {
					if !allowed {
						allowed = reg == destURL
					}
				}

				return nil
			}, destinationSQL, req.Method, userID)
			return nil
		},
	); err != nil {
		return false
	}

	return allowed
}
