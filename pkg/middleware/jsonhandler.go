// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

// JSONResult defines the return type of JSONHandler handler function.
type JSONResult struct {
	// Code is the HTTP status code to be set which defaults to http.StatusOK (200).
	Code int
	// Result is serialized to JSON.
	// If the type is an io.Reader its copied through.
	Result interface{}
}

// JSONError is an error if returned by the JSONHandler.Handle function
// which ends up encoded as a JSON document.
type JSONError struct {
	// Code is the HTTP status code of the result defaults
	// to http.StatusInternalServerError if not set.
	Code int
	// The message of the error.
	Message string
}

// JSONDefaultLimit is default size limit in bytes of an accepted
// input document.
const JSONDefaultLimit = 2048

// JSONHandler implements a middleware to ease the handing JSON input
// streams and return JSON documents as output.
type JSONHandler struct {
	// Input (if not nil) is called to fill a data structure
	// returned by this function.
	Input func(*http.Request) interface{}
	// Authorized checks if the predicates are fullfilled to call the Handle
	// function. If not http.StatusUnauthorized is send back.
	Authorized func(*http.Request) bool
	// Handle is called to handle the incoming HTTP request.
	// in is the data structure returned by Input. Its nil if Input is nil.
	Handle func(rep *http.Request) (JSONResult, error)
	// Limit overides the default size of accepted input documents.
	// Set to a negative value to allow an arbitrary size.
	// Handle with care!
	Limit int64
}

// Error implements the error interface.
func (je JSONError) Error() string {
	return fmt.Sprintf("%d: %s", je.Code, je.Message)
}

type jsonHandlerType int

const jsonHandlerInputKey jsonHandlerType = 0

// JSONInput extracts the de-serialized input from the context of the request.
func JSONInput(req *http.Request) interface{} {
	return req.Context().Value(jsonHandlerInputKey)
}

func (j *JSONHandler) ChainAuthorize(fn func(*http.Request) bool) {
	if j.Authorized == nil {
		j.Authorized = fn
	} else {
		next := j.Authorized
		j.Authorized = func(req *http.Request) bool {
			return fn(req) && next(req)
		}
	}
}

// ServeHTTP makes the JSONHandler a middleware.
func (j *JSONHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {

	if j.Input != nil {
		input := j.Input(req)
		defer req.Body.Close()
		var r io.Reader
		switch {
		case j.Limit == 0:
			r = io.LimitReader(req.Body, JSONDefaultLimit)
		case j.Limit > 0:
			r = io.LimitReader(req.Body, j.Limit)
		default:
			r = req.Body
		}
		if err := json.NewDecoder(r).Decode(input); err != nil {
			http.Error(rw, "error: "+err.Error(), http.StatusBadRequest)
			return
		}
		parent := req.Context()
		ctx := context.WithValue(parent, jsonHandlerInputKey, input)
		req = req.WithContext(ctx)
	}

	jr, err := j.Handle(req)

	if err != nil {
		switch e := err.(type) {
		case JSONError:
			if e.Code == 0 {
				e.Code = http.StatusInternalServerError
			}
			var res = struct {
				Message string `json:"message"`
			}{
				Message: e.Message,
			}
			SendJSON(rw, e.Code, &res)
		default:
			http.Error(rw,
				"error: "+err.Error(),
				http.StatusInternalServerError)
		}
		return
	}

	if jr.Code == 0 {
		jr.Code = http.StatusOK
	}

	if jr.Code != http.StatusNoContent {
		rw.Header().Set("Content-Type", "application/json")
	}
	rw.Header().Set("X-Content-Type-Options", "nosniff")

	rw.WriteHeader(jr.Code)
	if jr.Code != http.StatusNoContent {
		var err error
		if r, ok := jr.Result.(io.Reader); ok {
			_, err = io.Copy(rw, r)
		} else {
			err = json.NewEncoder(rw).Encode(jr.Result)
		}
		if err != nil {
			log.Printf("error: %v\n", err)
		}
	}
}

// SendJSON sends data JSON encoded to the response writer
// with a given HTTP status code.
func SendJSON(rw http.ResponseWriter, code int, data interface{}) {
	rw.Header().Set("Content-Type", "application/json")
	rw.Header().Set("X-Content-Type-Options", "nosniff")
	rw.WriteHeader(code)
	if err := json.NewEncoder(rw).Encode(data); err != nil {
		log.Printf("error: %v\n", err)
	}
}
