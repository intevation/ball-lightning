// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
)

const (
	checkValidSQL = `
SELECT ST_IsValid(ST_SetSrid(ST_GeomFromText($1), 4326))
`

	checkAreaWithinTextSQL = `
SELECT ST_Within(
 ST_SetSrid(ST_GeomFromText($1), 4326),
	(SELECT geom FROM blitz.stroke_areas sa
	JOIN users.groups g ON g.id = sa.group_id
	JOIN users.users u ON g.id = u.group_id
	WHERE u.id = $2
))
`

	checkArchiveDaysSQL = `
SELECT (($1 * interval'1 second' - $2 * interval'1 second') < (
	SELECT archive_days FROM users.app_settings s
	JOIN users.users u ON u.group_id = s.group_id
	WHERE u.id = $3) * interval'1 day')
`

	checkArchiveSinceSQL = `
SELECT ((now() - $1 * interval'1 second') > (
	SELECT archive_since FROM users.app_settings s
	JOIN users.users u ON u.group_id = s.group_id
	WHERE u.id = $2)
)

`
)

// StrokeHandler defines a middleware that checks if the requested data is in
// the allowed area for the user and if the time is in the allowed range.
func StrokeHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if !checkArea(req) {
			SendJSON(rw, http.StatusUnauthorized, struct{}{})
			return
		}
		if !checkTime(req) {
			SendJSON(rw, http.StatusUnauthorized, struct{}{})
			return
		}
		next.ServeHTTP(rw, req)
	})
}

func checkArea(req *http.Request) bool {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return false
	}

	//userID
	userID, ok := s.GetUser()
	if !ok {
		return false
	}

	switch req.Method {
	case "GET":
		return checkAreaWithURL(ctx, req, userID)
	case "PUT":
		return false
	case "POST":
		return false
	default:
		return false
	}
}

func checkTime(req *http.Request) bool {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return false
	}

	userID, ok := s.GetUser()
	if !ok {
		return false
	}

	switch req.Method {
	case "GET":
		return checkArchiveDays(ctx, req, userID) && checkArchiveSince(ctx, req, userID)
	case "DELETE":
		return false
	case "PUT":
		return false
	case "POST":
		return false
	default:
		return false
	}
}

func checkAreaWithURL(ctx context.Context, req *http.Request, userID int64) bool {
	var coordinates struct {
		left   float64
		top    float64
		right  float64
		bottom float64
	}
	var err error
	for _, co := range []struct {
		c *float64
		k string
	}{
		{&coordinates.left, "left"},
		{&coordinates.top, "top"},
		{&coordinates.right, "right"},
		{&coordinates.bottom, "bottom"},
	} {
		if *co.c, err = strconv.ParseFloat(req.FormValue(co.k), 64); err != nil {
			return false
		}
	}
	poly := fmt.Sprintf("POLYGON((%f %f, %f %f, %f %f, %f %f, %f %f))",
		coordinates.left,
		coordinates.top,
		coordinates.right,
		coordinates.top,
		coordinates.right,
		coordinates.bottom,
		coordinates.left,
		coordinates.bottom,
		coordinates.left,
		coordinates.top,
	)
	var allowed bool
	log.Println(poly)
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, checkValidSQL, poly).Scan(&allowed); err != nil {
				return err
			}
			if err := conn.QueryRowContext(ctx, checkAreaWithinTextSQL, poly, userID).Scan(&allowed); err != nil {
				log.Println(err)
				return err
			}
			return nil
		}); err != nil {
		log.Println(err)
		return false
	}
	return allowed
}

func checkArchiveDays(ctx context.Context, req *http.Request, userID int64) bool {
	start, end, err := getTimerange(req)
	if err != nil {
		log.Println(err)
		return false
	}
	var allowed bool
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, checkArchiveDaysSQL, start, end, userID).Scan(&allowed); err != nil {
				log.Println(err)
				return err
			}
			return nil
		}); err != nil {
		log.Println(err)
		return false
	}
	return allowed
}

func checkArchiveSince(ctx context.Context, req *http.Request, userID int64) bool {
	start, end, err := getTimerange(req)
	if err != nil {
		log.Println(err)
		return false
	}
	if start < end {
		log.Println("start before end")
		return false
	}
	var allowed bool
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, checkArchiveSinceSQL, start, userID).Scan(&allowed); err != nil {
				log.Println(err)
				return err
			}
			return nil
		}); err != nil {
		log.Println(err)
		return false
	}
	return allowed
}

func getTimerange(req *http.Request) (int64, int64, error) {
	var timerange struct {
		start int64
		end   int64
	}
	var err error
	for _, tr := range []struct {
		t *int64
		k string
	}{
		{&timerange.start, "start"},
		{&timerange.end, "end"},
	} {
		if *tr.t, err = strconv.ParseInt(req.FormValue(tr.k), 10, 64); err != nil {
			return 0, 0, fmt.Errorf("parser error")
		}
	}
	return timerange.start, timerange.end, nil
}
