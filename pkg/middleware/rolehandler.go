// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package middleware

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"bitbucket.org/intevation/ball-lightning/pkg/session"
	"github.com/gorilla/mux"
)

const (
	roleByID = `
WITH RECURSIVE search_graph(id, parent, name) AS (
	SELECT id, parent, name
	FROM users.roles
	WHERE id = (
		SELECT ur.role_id
		FROM users.user_roles ur
		JOIN users.users u ON u.id = ur.user_id
		WHERE u.id = $1)
	UNION ALL
	SELECT g.id, g.parent, g.name
	FROM users.roles g
	JOIN search_graph p ON g.parent = p.id
)
SELECT
	EXISTS(SELECT 1 FROM search_graph WHERE id = $2)
`

	userRoleSQL = `
SELECT EXISTS(
	SELECT 1 FROM users.user_roles
	WHERE user_id = $1 AND role_id = $2
)
`

	newPermissionSQL = `
SELECT EXISTS(
	SELECT rp.permission_id
	FROM users.users u
	JOIN users.groups g ON g.id = u.group_id
	JOIN users.roles r ON g.role_id = r.id
	JOIN users.role_permissions rp ON r.id = rp.role_id
	WHERE u.id = $1 and rp.permission_id = $2)
`

	permissionSubsetSQL = `
SELECT ARRAY(
	SELECT DISTINCT p.id
	FROM users.permissions p
	JOIN users.permission_activity pa ON pa.permission_id = p.id
	JOIN users.role_permissions rp ON p.id = rp.permission_id
	WHERE rp.role_id = $2)
<@ ARRAY(
	SELECT DISTINCT p.id
	FROM users.permissions p
	JOIN users.permission_activity pa ON pa.permission_id = p.id
	JOIN users.role_permissions rp ON p.id = rp.permission_id
	WHERE rp.role_id IN (SELECT r.id
			FROM users.roles r
			JOIN users.groups g ON g.role_id = r.id
			JOIN users.users u ON u.group_id = g.id
			WHERE u.id = $1
	)
)
`
)

// RoleHandler defines a middleware that checks, if the role of the logged in
// user has apropriate permissions for a request and checks if the role the user
// wants to use has not more permissions than his/her own role.
func RoleHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if !checkRoleTree(req) {
			SendJSON(rw, http.StatusUnauthorized, struct{}{})
			return
		}
		next.ServeHTTP(rw, req)
	})
}

func checkRoleTree(req *http.Request) bool {
	ctx := req.Context()

	s, ok := session.FromContext(ctx)
	if !ok {
		return false
	}

	userID, ok := s.GetUser()
	if !ok {
		return false
	}

	switch req.Method {
	case "GET":
		return roleURL(ctx, req, userID)
	case "DELETE":
		return roleURL(ctx, req, userID)
	case "PUT":
		return roleBody(ctx, req, userID)
	case "POST":
		return roleBody(ctx, req, userID)
	default:
		return false
	}
}

func checkTreeForRole(ctx context.Context, req *http.Request, userID, roleID int64) bool {
	var allowed bool
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, roleByID, userID, roleID).Scan(&allowed); err != nil {
				return err
			}
			return nil
		}); err != nil {
		return false
	}

	return allowed
}

func checkPermissionSubset(ctx context.Context, req *http.Request, userID, roleID int64) bool {
	isSubset := false
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, permissionSubsetSQL, userID, roleID).Scan(&isSubset); err != nil {
				return err
			}
			return nil
		}); err != nil {
		return false
	}

	return isSubset
}

func checkNewPermission(ctx context.Context, req *http.Request, userID, roleID int64) bool {
	isSubset := false
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, newPermissionSQL, userID, roleID).Scan(&isSubset); err != nil {
				return err
			}
			return nil
		}); err != nil {
		return false
	}

	return isSubset
}

func checkOwnRole(ctx context.Context, req *http.Request, userID, roleID int64) bool {
	isUsersRole := true
	if err := database.Execute(
		ctx,
		func(ctx context.Context, conn *sql.Conn) error {
			if err := conn.QueryRowContext(ctx, userRoleSQL, userID, roleID).Scan(&isUsersRole); err != nil {
				return err
			}
			return nil
		}); err != nil {
		return true
	}

	return isUsersRole
}

func roleURL(ctx context.Context, req *http.Request, userID int64) bool {
	reg := regexp.MustCompile(urlRegex)
	match := reg.FindStringSubmatch(req.URL.Path)

	var id int64
	var err error
	if len(match) == 3 && match[2] == "" {
		// The request has no futher parameters. User is allowed to use its own content.
		return true
	} else if len(match) > 2 {
		// Route has at least one id as second path part
		id, err = strconv.ParseInt(match[2], 10, 64)
		if err != nil {
			return false
		}
	} else {
		return false
	}

	if match[1] == "role" {
		if req.Method == "DELETE" {
			return checkTreeForRole(ctx, req, userID, id) &&
				!checkOwnRole(ctx, req, userID, id)
		}
		return checkTreeForRole(ctx, req, userID, id)
	}

	return false
}

func roleBody(ctx context.Context, req *http.Request, userID int64) bool {
	var input map[string]interface{}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return false
	}

	err = json.NewDecoder(bytes.NewReader(body)).Decode(&input)
	if err != nil {
		return false
	}

	req.Body = ioutil.NopCloser(bytes.NewReader(body))
	roleIDName := "parent"
	if strings.HasPrefix(req.URL.Path, "/user") {
		roleIDName = "id"
		if itemRoleID, hasRole := input[roleIDName].(float64); hasRole {
			return checkTreeForRole(ctx, req, userID, int64(itemRoleID)) &&
				checkPermissionSubset(ctx, req, userID, int64(itemRoleID))
		}
	}

	if req.Method == "PUT" && strings.HasSuffix(req.URL.Path, "/permission") {
		vars := mux.Vars(req)
		if itemRoleID, err := strconv.ParseInt(vars["id"], 10, 64); err == nil {
			return checkTreeForRole(ctx, req, userID, int64(itemRoleID)) &&
				checkPermissionSubset(ctx, req, userID, int64(itemRoleID))
		}
	}

	if itemRoleID, hasRole := input[roleIDName].(float64); hasRole {
		return checkTreeForRole(ctx, req, userID, int64(itemRoleID))
	}

	return false
}
