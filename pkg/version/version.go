// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license

package version

// Version is the version string returned by the version controller.Version
// Can be set via 'go build -ldflags="-X 'bitbucket.org/intevation/ball-lightning/pkg/pkg/version.Version=$MY_GIT_VERSION_STRING'"
var Version = "dev"
