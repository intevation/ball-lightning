// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package database

import (
	"context"
	"database/sql"
	"log"
	"strconv"
	"sync"
)

// Configuration represents a set of configuration key/value pairs.
type Configuration map[string]string

var muConfiguration sync.Mutex

var configurations = map[string]Configuration{}

// GetConfiguration returns a named configuration from the
// database. If this fails an error is returned.
// The returned configuration even if fetching was not successful
// can be used to lookup specific values for given keys.
func GetConfiguration(name string) (Configuration, error) {
	muConfiguration.Lock()
	muConfiguration.Unlock()

	cfg := configurations[name]
	if cfg != nil {
		return cfg, nil
	}
	cfg, err := LoadConfiguration(name)
	if err != nil {
		return nil, err
	}
	configurations[name] = cfg
	return cfg, nil
}

var (
	loadConfigurationSQL = `
    SELECT key, value FROM configuration_kv kv JOIN configuration c
    ON c.id = kv.configuration_id WHERE c.name = $1`

	loadConfigurationPrefixSQL = `
    SELECT key, value FROM configuration_kv kv JOIN configuration c
    ON c.id = kv.configuration_id WHERE c.name = $1 AND kv.key LIKE $2`
)

// LoadConfiguration loads an uncached configuration from the database.
func LoadConfiguration(name string) (Configuration, error) {
	var cfg Configuration
	err := Execute(context.Background(), func(ctx context.Context, conn *sql.Conn) error {
		rows, err := conn.QueryContext(ctx, loadConfigurationSQL, name)
		if err != nil {
			return err
		}
		defer rows.Close()
		cfg = make(Configuration)
		for rows.Next() {
			var key, value string
			if err := rows.Scan(&key, &value); err != nil {
				return err
			}
			cfg[key] = value
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

// LoadConfiguration loads an uncached configuration from the database
// where the keys have a given prefix.
func LoadConfigurationPrefix(name, prefix string) (Configuration, error) {
	var cfg Configuration
	err := Execute(context.Background(), func(ctx context.Context, conn *sql.Conn) error {
		rows, err := conn.QueryContext(ctx, loadConfigurationPrefixSQL, name, prefix+"%")
		if err != nil {
			return err
		}
		defer rows.Close()
		cfg = make(Configuration)
		for rows.Next() {
			var key, value string
			if err := rows.Scan(&key, &value); err != nil {
				return err
			}
			cfg[key] = value
		}
		return rows.Err()
	})
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

// Int64 returns the int64 value for a given key.
// If not found the given default value is returned.
// This methods works on uninitialized configurations, too.
func (cfg Configuration) Int64(key string, def int64) int64 {
	if cfg == nil {
		return def
	}
	value := cfg[key]
	if value == "" {
		return def
	}
	v, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		log.Printf("config error: %s -> %s (int64 value expected)", key, value)
		return def
	}
	return v
}

// String returns the string value for a given key.
// If not found the given default value is returned.
// This methods works on uninitialized configurations, too.
func (cfg Configuration) String(key, def string) string {
	if cfg == nil {
		return def
	}
	if value, found := cfg[key]; found {
		return value
	}
	return def
}
