// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package database

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
)

func Query(
	ctx context.Context,
	conn *sql.Conn,
	fn func(*sql.Rows) error,
	query string,
	args ...interface{},
) error {
	rows, err := conn.QueryContext(ctx, query, args...)
	if err != nil {
		return errors.WithStack(err)
	}
	defer rows.Close()
	for rows.Next() {
		if err := fn(rows); err != nil {
			return err
		}
	}
	return rows.Err()
}

func TransactionExec(
	ctx context.Context,
	tx *sql.Tx,
	query string,
	args ...interface{},
) error {
	result, err := tx.ExecContext(ctx, query, args...)
	if err != nil {
		return err
	}
	count, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func QueryWithoutCtx(
	fn func(*sql.Rows) error,
	query string,
	arguments ...interface{},
) error {
	rows, err := DB.Query(query, arguments...)
	if err != nil {
		return errors.WithStack(err)
	}
	defer rows.Close()
	for rows.Next() {
		if err := fn(rows); err != nil {
			return err
		}
	}
	return rows.Err()
}
