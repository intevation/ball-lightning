// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package session

import (
	"crypto/rand"
	"io"
	"log"
	"math/big"
)

const base62alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

var (
	zero  = big.NewInt(0)
	div62 = big.NewInt(62)
)

func encodeToString(src []byte) string {
	v := new(big.Int)
	v.SetBytes(src)
	m := new(big.Int)
	z := new(big.Int)
	out := make([]byte, 0, 24)
	for {
		z.DivMod(v, div62, m)
		// reverse order but it doesnt matter.
		out = append(out, base62alphabet[m.Int64()])
		if z.Cmp(zero) == 0 {
			break
		}
		v, z = z, v
	}
	return string(out)
}

func generateSessionKey() string {
	var data [16]byte
	if _, err := io.ReadFull(rand.Reader, data[:]); err != nil {
		log.Fatalf("cannot generate key: %v\n", err)
	}
	return encodeToString(data[:])
}
