// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"log"
	"time"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// Group represents the persistence layer
type Group struct {
	ID                *int64  `json:"id"`
	Name              *string `json:"name"`
	Parent            *int64  `json:"parent"`
	RoleID            *int64  `json:"roleId"`
	ThemeID           *int64  `json:"themeId"`
	MaxAlarmArea      *int64  `json:"maxAlarmArea"`
	DataAreaID        *int64  `json:"dataAreaId"`
	AlarmConfigID     *int64  `json:"alarmConfigId"`
	ChangeAlarmConfig bool    `json:"changeAlarmConfig"`
}

const (
	groupSQL = `
SELECT id,
	name,
	parent,
	role_id,
	theme_id,
	max_alarmarea,
	data_area_id,
	alarm_config_id,
	change_alarm_config
FROM users.groups
WHERE id = $1
`
	groupChildrenSQL = `
SELECT id,
	name,
	parent,
	role_id,
	theme_id,
	max_alarmarea,
	data_area_id,
	alarm_config_id,
	change_alarm_config
FROM users.groups
WHERE parent = $1
`
	insertGroupSQL = `
INSERT INTO users.groups (
	parent, name, role_id, theme_id, max_alarmarea, data_area_id, alarm_config_id, change_alarm_config
) VALUES (
	$1,	$2,	$3,	$4,	$5,	$6,	$7, $8
) RETURNING id
`
	updateGroupSQL = `
UPDATE users.groups
	SET
	(parent, name, role_id, theme_id, max_alarmarea, data_area_id, alarm_config_id, change_alarm_config) = 
	($1, $2, $3, $4, $5, $6, $7, $8)
	WHERE id = $9
RETURNING id
`
	deleteGroupSQL = `
DELETE FROM users.groups
WHERE id = $1
`
)

// LoadGroupByID fetches the group with the given groupID from the database.
func LoadGroupByID(ctx context.Context, conn *sql.Conn, groupID int64) (*Group, error) {
	var group Group
	if err := conn.QueryRowContext(ctx, groupSQL, groupID).Scan(
		&group.ID,
		&group.Name,
		&group.Parent,
		&group.RoleID,
		&group.ThemeID,
		&group.MaxAlarmArea,
		&group.DataAreaID,
		&group.AlarmConfigID,
		&group.ChangeAlarmConfig); err != nil {
		return nil, errors.WithStack(err)
	}

	return &group, nil
}

// LoadGroupChildren fetches all group childs of the group identified by groupID parameter.
func LoadGroupChildren(ctx context.Context, conn *sql.Conn, groupID int64) ([]*Group, error) {
	var children []*Group
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		var group Group
		if err := row.Scan(
			&group.ID,
			&group.Name,
			&group.Parent,
			&group.RoleID,
			&group.ThemeID,
			&group.MaxAlarmArea,
			&group.DataAreaID,
			&group.AlarmConfigID,
			&group.ChangeAlarmConfig); err != nil {
			log.Println("error: ", err)
			return err
		}
		children = append(children, &group)
		return nil
	}, groupChildrenSQL, groupID)
	if err != nil {
		log.Println("error: ", err)
		return nil, err
	}
	return children, nil
}

// CreateGroup persists a new group in the database and creates empty app settings.
func CreateGroup(ctx context.Context, conn *sql.Conn, group *Group) (int64, error) {
	var id int64

	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return -1, err
	}
	defer tx.Rollback()

	err = tx.QueryRowContext(ctx,
		insertGroupSQL,
		group.Parent,
		group.Name,
		group.RoleID,
		group.ThemeID,
		group.MaxAlarmArea,
		group.DataAreaID,
		group.AlarmConfigID,
		group.ChangeAlarmConfig).Scan(&id)
	if err != nil {
		return -1, err
	}

	settings := AppSettings{
		ArchiveSince: pgtype.Timestamptz{
			Time:   time.Now(),
			Status: pgtype.Present},
	}
	err = database.TransactionExec(
		ctx,
		tx,
		insertAppSettingsSQL,
		id,
		settings.MaxDisplayTime,
		settings.ArchiveDays,
		settings.ArchiveSince.Time,
		settings.MaxZoom,
		settings.MinZoom,
		settings.LiveBlids,
		settings.Animation,
		settings.Sound,
		settings.Path,
		settings.MaxAlarms,
		settings.MaxAlarmAreas,
		settings.MaxAlarmUser,
		settings.StatisticWindowed,
	)
	if err != nil {
		return -1, err
	}

	tx.Commit()
	return id, nil
}

// UpdateGroup persists changes for a group.
func UpdateGroup(ctx context.Context, conn *sql.Conn, group *Group) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, updateGroupSQL,
		group.Parent,
		group.Name,
		group.RoleID,
		group.ThemeID,
		group.MaxAlarmArea,
		group.DataAreaID,
		group.AlarmConfigID,
		group.ChangeAlarmConfig,
		group.ID)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteGroup removes an empty group from the database.
func DeleteGroup(ctx context.Context, conn *sql.Conn, groupID int64) error {
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		return nil
	}, deleteGroupSQL, groupID)
	if err != nil {
		return err
	}
	return nil
}
