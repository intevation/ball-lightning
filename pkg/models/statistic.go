// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"time"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// StatisticItem represents a single entry in statistics.
type StatisticItem struct {
	Timestamp time.Time `json:"timestamp"`
	Count     float64   `json:"count"`
}

// Statistic represents the summary over stroke statistics in a specific
// time range and in a specific bbox.
type Statistic struct {
	PEarth      []StatisticItem `json:"pEarth"`
	NEarth      []StatisticItem `json:"nEarth"`
	Cloud       []StatisticItem `json:"cloud"`
	CountTotal  int64           `json:"countTotal"`
	CountPEarth int64           `json:"countPEarth"`
	CountNEarth int64           `json:"countNEarth"`
	CountCloud  int64           `json:"countCloud"`
}

const (
	statisticPrefix = `
SELECT
  to_char(x AT TIME ZONE 'UTC','YYYY-MM-DD HH24:MI:SS') AS date,
  count(statistic.*)::float*60 / $1                     AS count
FROM
  generate_series(
    now() - $2 * interval'1 second',
    now() - $3 * interval'1 second',
    $1         * interval'1 second') AS x
  LEFT JOIN statistic(
    CAST($2 AS integer),
    CAST($3 AS integer),
    CAST($4 AS numeric),
    CAST($5 AS numeric),
    CAST($6 AS numeric),
    CAST($7 AS numeric))
  ON (statistic.time BETWEEN x - $1 * interval'1 second' AND x 
`
	statisticCloudFilter = `
AND type = 4	
`
	statisticPosFilter = `
AND type = 1 AND amperage > 0
`
	statisticNegFilter = `
AND type = 1 AND amperage < 0
`
	statisticSuffix = `
) GROUP BY 1 ORDER BY date	
`
	countPrefix = `
SELECT count(*)
FROM statistic(
  CAST($1 AS integer),
  CAST($2 AS integer),
  CAST($3 AS numeric),
  CAST($4 AS numeric),
  CAST($5 AS numeric),
  CAST($6 AS numeric))
WHERE `
)

var (
	countStmt       = countPrefix + `1 = 1`
	countStmtCloud  = countPrefix + `type = 4`
	countStmtPEarth = countPrefix + `type = 1 AND amperage > 0`
	countStmtNEarth = countPrefix + `type = 1 AND amperage < 0`
)

var (
	statisticStmtCloud  = statisticPrefix + statisticCloudFilter + statisticSuffix
	statisticStmtPEarth = statisticPrefix + statisticPosFilter + statisticSuffix
	statisticStmtNEarth = statisticPrefix + statisticNegFilter + statisticSuffix
)

// LoadStatistics fetches stroke statististics from the database.
func LoadStatistics(
	ctx context.Context,
	conn *sql.Conn,
	start, stop int64,
	left, right, top, bottom float64,
) (*Statistic, error) {
	interval := (start - stop) / 60
	var result Statistic
	var err error
	for _, x := range []struct {
		stat *[]StatisticItem
		stmt string
	}{
		{&result.Cloud, statisticStmtCloud},
		{&result.PEarth, statisticStmtPEarth},
		{&result.NEarth, statisticStmtNEarth},
	} {
		if *x.stat, err = loadStatistic(
			ctx,
			conn,
			x.stmt,
			start,
			stop,
			interval,
			left,
			right,
			top,
			bottom); err != nil {
			return nil, err
		}
	}
	for _, x := range []struct {
		count *int64
		stmt  string
	}{
		{&result.CountTotal, countStmt},
		{&result.CountCloud, countStmtCloud},
		{&result.CountPEarth, countStmtPEarth},
		{&result.CountNEarth, countStmtNEarth},
	} {
		if *x.count, err = loadCount(
			ctx,
			conn,
			x.stmt,
			start,
			stop,
			interval,
			left,
			right,
			top,
			bottom); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func loadCount(
	ctx context.Context,
	conn *sql.Conn,
	stmt string,
	start, stop, interval int64,
	left, right, top, bottom float64,
) (int64, error) {
	var count int64
	if err := conn.QueryRowContext(ctx, stmt,
		start, stop, left, right, top, bottom).Scan(&count); err != nil {
		return -1, err
	}
	return count, nil
}

func loadStatistic(
	ctx context.Context,
	conn *sql.Conn,
	stmt string,
	start, stop, interval int64,
	left, right, top, bottom float64,
) ([]StatisticItem, error) {
	var statistics []StatisticItem
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var statistic StatisticItem
		var itemTime pgtype.Timestamp
		if err := rows.Scan(
			&itemTime,
			&statistic.Count,
		); err != nil {
			return err
		}
		statistic.Timestamp = itemTime.Time
		statistics = append(statistics, statistic)
		return nil
	},
		stmt, interval, start, stop, left, right, top, bottom); err != nil {
		return nil, errors.WithStack(err)
	}
	return statistics, nil
}
