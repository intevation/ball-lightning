// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"fmt"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// Alarm represents the datastructure for alarm_area_units in the db.
type Alarm struct {
	ID             int64               `json:"id"`
	Name           string              `json:"name"`
	GroupID        int64               `json:"groupId"`
	Alarm          bool                `json:"alarm"`
	AlarmTimestamp *pgtype.Timestamptz `json:"alarmTimestamp"`
	IsExtended     bool                `json:"isExtended"`
	IsNotified     bool                `json:"isNotified"`
}

const (
	alarmByUserSQL = `
SELECT 
	au.id,
	au.name,
	au.group_id,
	au.alarm,
	au.alarm_timestamp,
	au.is_extended,
	au.notified
FROM alarm.alarms a
JOIN alarm.alarm_area_unit au ON a.alarm_area_unit_id = au.id
WHERE a.user_id = $1
`

	alarmByIDSQL = `
SELECT 
	au.id,
	au.name,
	au.group_id,
	au.alarm,
	au.alarm_timestamp,
	au.is_extended,
	au.notified
FROM alarm.alarms a
JOIN alarm.alarm_area_unit au ON a.alarm_area_unit_id = au.id
WHERE au.id = $1
`
	alarmByGroupSQL = `
SELECT 
	id,
	name,
	group_id,
	alarm,
	alarm_timestamp,
	is_extended,
	notified
FROM alarm.alarm_area_unit
WHERE group_id = $1
`

	alarmByGroupByIDSQL = `
SELECT 
	id,
	name,
	group_id,
	alarm,
	alarm_timestamp,
	is_extended,
	notified
FROM alarm.alarm_area_unit
WHERE group_id = $1 AND id = $2
`

	userHasAlarmSQL = `
SELECT EXISTS(
	SELECT 1
	FROM alarm.alarms a
	JOIN alarm.alarm_area_unit au ON a.alarm_area_unit_id = au.id
	WHERE a.user_id = $1 AND au.id = $2
)
`

	groupHasAlarmSQL = `
SELECT EXISTS(
	SELECT 1
	From alarm.alarm_area_unit au
	WHERE au.group_id = $1 AND au.id = $2
)
`

	insertAlarmSQL = `
INSERT INTO alarm.alarms (alarm_area_unit_id, user_id)
VALUES($1, $2)
RETURNING id
`

	insertAlarmAreaUnitSQL = `
INSERT INTO alarm.alarm_area_unit (name, group_id, is_extended)
VALUES ($1, $2, $3)
RETURNING id
`
	updateAlarmAreaUnitSQL = `
UPDATE alarm.alarm_area_unit SET (name, group_id, is_extended) =
($1, $2, $3) WHERE id = $4
RETURNING id
`

	deleteAlarmSQL = `
DELETE FROM alarm.alarms where alarm_area_unit_id = $1 AND user_id = $2
`

	deleteAlarmAreaUnitSQL = `
DELETE FROM alarm.alarm_area_unit where id = $1
`

	deleteAlarmGroupIDSQL = `
DELETE FROM alarm.alarm_area_unit where id = $1 AND group_id = $2
`
)

// LoadAlarms fetches the alarms associated to a user from db using the userID parameter
func LoadAlarms(ctx context.Context, conn *sql.Conn, userID int64) ([]Alarm, error) {
	var alarms []Alarm
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var alarm Alarm
		if err := rows.Scan(
			&alarm.ID,
			&alarm.Name,
			&alarm.GroupID,
			&alarm.Alarm,
			&alarm.AlarmTimestamp,
			&alarm.IsExtended,
			&alarm.IsNotified,
		); err != nil {
			return err
		}
		alarms = append(alarms, alarm)
		return nil
	},
		alarmByUserSQL, userID); err != nil {
		return nil, errors.WithStack(err)
	}
	return alarms, nil
}

// LoadAlarm fetches n alarm identified by its id from the database.
func LoadAlarm(ctx context.Context, conn *sql.Conn, unitID int64) (*Alarm, error) {
	var alarm Alarm
	if err := conn.QueryRowContext(ctx, alarmByIDSQL, unitID).Scan(
		&alarm.ID,
		&alarm.Name,
		&alarm.GroupID,
		&alarm.Alarm,
		&alarm.AlarmTimestamp,
		&alarm.IsExtended,
		&alarm.IsNotified,
	); err != nil {
		return nil, err
	}
	return &alarm, nil
}

// LoadAlarmByGroup fetches alarms associated to a group from db using the groupID parameter
func LoadAlarmByGroup(ctx context.Context, conn *sql.Conn, groupID int64) ([]Alarm, error) {
	var alarms []Alarm
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var alarm Alarm
		if err := rows.Scan(
			&alarm.ID,
			&alarm.Name,
			&alarm.GroupID,
			&alarm.Alarm,
			&alarm.AlarmTimestamp,
			&alarm.IsExtended,
			&alarm.IsNotified,
		); err != nil {
			return err
		}
		alarms = append(alarms, alarm)
		return nil
	},
		alarmByGroupSQL, groupID); err != nil {
		return nil, errors.WithStack(err)
	}
	return alarms, nil
}

// LoadAlarmByGroupByID fetches a single alram identified by its id for a group from db.
func LoadAlarmByGroupByID(ctx context.Context, conn *sql.Conn, groupID, alarmID int64) ([]Alarm, error) {
	var alarms []Alarm
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var alarm Alarm
		if err := rows.Scan(
			&alarm.ID,
			&alarm.Name,
			&alarm.GroupID,
			&alarm.Alarm,
			&alarm.AlarmTimestamp,
			&alarm.IsExtended,
			&alarm.IsNotified,
		); err != nil {
			return err
		}
		alarms = append(alarms, alarm)
		return nil
	},
		alarmByGroupByIDSQL, groupID, alarmID); err != nil {
		return nil, errors.WithStack(err)
	}
	return alarms, nil
}

// HasAlarm checks if the alarm identified by alarm is associated to a user identified by userID
func HasAlarm(ctx context.Context, conn *sql.Conn, userID, alarmID int64) bool {
	var allowed bool
	if err := conn.QueryRowContext(ctx, userHasAlarmSQL, userID, alarmID).Scan(&allowed); err != nil {
		return false
	}
	return allowed
}

// HasAlarmByGroup checks if the group has the alarm.
func HasAlarmByGroup(ctx context.Context, conn *sql.Conn, groupID, alarmID int64) bool {
	var allowed bool
	if err := conn.QueryRowContext(ctx, groupHasAlarmSQL, groupID, alarmID).Scan(&allowed); err != nil {
		return false
	}
	return allowed
}

// CreateAlarm persists the given alarm and connects it to the user.
func CreateAlarm(ctx context.Context, conn *sql.Conn, alarm Alarm) (alarmID int64, err error) {
	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return -1, err
	}
	defer tx.Rollback()

	var unitID int64
	err = tx.QueryRow(
		insertAlarmAreaUnitSQL,
		alarm.Name,
		alarm.GroupID,
		false,
	).Scan(&unitID)
	if err != nil {
		return -1, err
	}

	err = tx.Commit()
	if err != nil {
		return -1, err
	}

	return unitID, nil
}

// AddAlarmToUser connects an existing alarm to the specified user.
func AddAlarmToUser(ctx context.Context, conn *sql.Conn, userID int64, alarm Alarm) (alarmID int64, err error) {
	if err := conn.QueryRowContext(
		ctx,
		insertAlarmSQL,
		alarm.ID,
		userID,
	).Scan(&alarmID); err != nil {
		return -1, err
	}
	return alarmID, nil
}

// UpdateAlarm persists new attributes in an existing alarm.
func UpdateAlarm(ctx context.Context, conn *sql.Conn, alarm Alarm) (alarmID int64, err error) {
	var id int64
	if err := conn.QueryRowContext(
		ctx,
		updateAlarmAreaUnitSQL,
		alarm.Name,
		alarm.GroupID,
		alarm.IsExtended,
		alarm.ID).Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteAlarm removes the association between the user and the alarm. Does not remove the alarm itself.
func DeleteAlarm(ctx context.Context, conn *sql.Conn, alarmID, userID int64) error {
	res, err := conn.ExecContext(ctx, deleteAlarmSQL, alarmID, userID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("No alarm deleted")
	}
	return nil
}

// DeleteAlarmByGroupID removes an alarm from the database.
func DeleteAlarmByGroupID(ctx context.Context, conn *sql.Conn, alarmID, groupID int64) error {
	res, err := conn.ExecContext(ctx, deleteAlarmGroupIDSQL, alarmID, groupID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("No alarm deleted")
	}
	return nil
}
