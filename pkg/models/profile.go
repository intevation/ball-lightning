// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"

	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// Profile represents the persitence layer of a user
type Profile struct {
	ID   *int64       `json:"id"`
	Data *pgtype.JSON `json:"data"`
}

const (
	userProfileSQL = `
SELECT p.id,
	client_config
FROM users.profile p
JOIN users.users u ON u.id = p.user_id
WHERE u.id = $1
`
	updateUserProfileSQL = `
UPDATE users.profile
SET client_config = $1::JSON
WHERE user_id = $2
`
	createUserProfileSQL = `
INSERT INTO users.profile (client_config, user_id)
VALUES('{}'::JSON, $1) RETURNING id
`
)

// LoadProfileForUser fetches a profile for a user from db using the userID parameter
func LoadProfileForUser(ctx context.Context, conn *sql.Conn, userID int64) (*Profile, error) {
	var profile Profile
	if err := conn.QueryRowContext(ctx, userProfileSQL, userID).Scan(
		&profile.ID,
		&profile.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return &profile, nil
}

// UpdateProfile sets the new profile data for a user.
func UpdateProfile(ctx context.Context, conn *sql.Conn, userID int64, profile *interface{}) error {
	json, err := json.Marshal(profile)
	if err != nil {
		return err
	}
	log.Println(string(json))
	result, err := conn.ExecContext(ctx, updateUserProfileSQL, string(json), userID)
	if err != nil {
		log.Println("error: ", err)
		return err
	}
	if rows, err := result.RowsAffected(); rows == 0 || err != nil {
		return errors.New("Not updated")
	}
	return nil
}

// CreateProfile persists a new empty profile for a user.
func CreateProfile(ctx context.Context, conn *sql.Conn, userID int64) (int64, error) {
	var id int64
	if err := conn.QueryRowContext(ctx, createUserProfileSQL, userID).Scan(&id); err != nil {
		log.Println("error: ", err)
		return -1, err
	}
	return id, nil
}
