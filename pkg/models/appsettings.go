// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// AppSettings represents the persistence layer
type AppSettings struct {
	ID                *int64             `json:"id"`
	GroupID           int64              `json:"groupId"`
	MaxDisplayTime    *int64             `json:"maxDisplayTime"`
	ArchiveDays       int64              `json:"archiveDays"`
	ArchiveSince      pgtype.Timestamptz `json:"archiveSince"`
	MaxZoom           int64              `json:"maxZoom"`
	MinZoom           int64              `json:"minZoom"`
	LiveBlids         bool               `json:"liveBlids"`
	Animation         bool               `json:"animation"`
	Sound             bool               `json:"sound"`
	Path              string             `json:"path"`
	MaxAlarms         int64              `json:"maxAlarms"`
	MaxAlarmAreas     int64              `json:"maxAlarmAreas"`
	MaxAlarmUser      int64              `json:"maxAlarmUser"`
	StatisticWindowed bool               `json:"statisticWindowed"`
}

const (
	appSettingsSQL = `
SELECT a.id,
    a.group_id,
	max_display_time,
	archive_days,
	archive_since,
	max_zoom,
	min_zoom,
	live_blids,
	animation,
	sound,
	path,
	max_alarms,
	max_alarm_areas,
	max_alarm_users,
	statistic_windowed
FROM users.app_settings a
WHERE a.group_id = $1
`

	insertAppSettingsSQL = `
INSERT INTO users.app_settings (
	group_id,
	max_display_time,
	archive_days,
	archive_since,
	max_zoom,
	min_zoom,
	live_blids,
	animation,
	sound,
	path,
	max_alarms,
	max_alarm_areas,
	max_alarm_users,
	statistic_windowed
) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
`
	updateAppSettingsSQL = `
UPDATE users.app_settings SET (
	max_display_time,
	archive_days,
	archive_since,
	max_zoom,
	min_zoom,
	live_blids,
	animation,
	sound,
	path,
	max_alarms,
	max_alarm_areas,
	max_alarm_users,
	statistic_windowed
) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
WHERE group_id = $14
RETURNING id
`
)

// LoadAppSettings fetches settings for a group from db using the groupID parameter
func LoadAppSettings(ctx context.Context, conn *sql.Conn, groupID int64) (*AppSettings, error) {
	var settings AppSettings
	if err := conn.QueryRowContext(ctx, appSettingsSQL, groupID).Scan(
		&settings.ID,
		&settings.GroupID,
		&settings.MaxDisplayTime,
		&settings.ArchiveDays,
		&settings.ArchiveSince,
		&settings.MaxZoom,
		&settings.MinZoom,
		&settings.LiveBlids,
		&settings.Animation,
		&settings.Sound,
		&settings.Path,
		&settings.MaxAlarms,
		&settings.MaxAlarmAreas,
		&settings.MaxAlarmUser,
		&settings.StatisticWindowed,
	); err != nil {
		return nil, errors.WithStack(err)
	}

	return &settings, nil
}

// CreateAppSettings persists new app settings for the specified group.
func CreateAppSettings(ctx context.Context, conn *sql.Conn, groupID int64, settings AppSettings) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, insertAppSettingsSQL,
		groupID,
		settings.MaxDisplayTime,
		settings.ArchiveDays,
		settings.ArchiveSince.Get(),
		settings.MaxZoom,
		settings.MinZoom,
		settings.LiveBlids,
		settings.Animation,
		settings.Sound,
		settings.Path,
		settings.MaxAlarms,
		settings.MaxAlarmAreas,
		settings.MaxAlarmUser,
		settings.StatisticWindowed,
	)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateAppSettings persists changes in app settings.
func UpdateAppSettings(ctx context.Context, conn *sql.Conn, groupID int64, settings AppSettings) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, updateAppSettingsSQL,
		settings.MaxDisplayTime,
		settings.ArchiveDays,
		settings.ArchiveSince.Get(),
		settings.MaxZoom,
		settings.MinZoom,
		settings.LiveBlids,
		settings.Animation,
		settings.Sound,
		settings.Path,
		settings.MaxAlarms,
		settings.MaxAlarmAreas,
		settings.MaxAlarmUser,
		settings.StatisticWindowed,
		groupID,
	)
	if err != nil {
		return -1, err
	}
	return id, nil
}
