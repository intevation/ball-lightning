// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/mb0/wkt"
	"github.com/pkg/errors"
)

// StrokeArea represents the persistence layer
type StrokeArea struct {
	ID      *int64      `json:"id"`
	GroupID int64       `json:"groupId"`
	Geom    interface{} `json:"geom"`
}

// AlarmArea represents the persistence layer
type AlarmArea struct {
	ID              int64               `json:"id"`
	Geom            interface{}         `json:"geom"`
	AlarmDuration   *int64              `json:"alarmDuration"`
	Radius          *float64            `json:"radius"`
	Center          *interface{}        `json:"center"`
	Icon            *string             `json:"icon"`
	Area            *float64            `json:"area"`
	InnerRadius     *float64            `json:"innerRadius"`
	StrokeThreshold *int64              `json:"strokeThreshold"`
	StrokeCounter   *int64              `json:"strokeCounter"`
	LastStroke      *pgtype.Timestamptz `json:"lastStroke"`
}

const (
	strokeAreaSQL = `
SELECT st_astext(s.geom) FROM users.users u
JOIN users.groups g ON u.group_id = g.id
JOIN blitz.stroke_areas s on s.group_id = g.id
WHERE u.id = $1
`
	strokeAreaByUserSQL = `
SELECT sa.id,
sa.group_id,
ST_AsGeoJSON(sa.geom)
FROM blitz.stroke_areas sa
JOIN users.users u ON sa.group_id = u.group_id
WHERE u.id = $1
`
	strokeAreaByGroupSQL = `
SELECT sa.id,
group_id,
ST_AsGeoJSON(sa.geom)
FROM blitz.stroke_areas sa
WHERE group_id = $1
`
	createStrokeAreaSQL = `
INSERT INTO blitz.stroke_areas (group_id, geom)
VALUES($1, ST_SetSrid(ST_GeomFromGeoJSON($2), 4326))
RETURNING id;
`

	updateStrokeAreaSQL = `
UPDATE blitz.stroke_areas SET geom = ST_SetSrid(ST_GeomFromGeoJSON($1), 4326) WHERE id = $2 RETURNING id
`

	areaByUnitSQL = `
SELECT
	aa.id,
	ST_AsGeoJSON(aa.geom),
	aa.alarm_duration,
	aa.radius,
	ST_AsGeoJSON(aa.center),
	aa.icon,
	aa.area,
	aa.inner_radius,
	aa.stroke_threshold,
	sc.stroke_counter,
	sc.last_stroke
FROM alarm.alarm_area_map am
JOIN alarm.stroke_counter sc ON am.alarm_area_id = sc.id
JOIN alarm.alarm_areas aa on aa.id = am.alarm_area_id
WHERE am.alarm_area_unit_id = $1
`

	areaByIDSQL = `
SELECT
	aa.id,
	ST_AsGeoJSON(aa.geom),
	aa.alarm_duration,
	aa.radius,
	ST_AsGeoJSON(aa.center),
	aa.icon,
	aa.area,
	aa.inner_radius,
	aa.stroke_threshold,
	sc.stroke_counter,
	sc.last_stroke
FROM alarm.alarm_areas aa
JOIN alarm.stroke_counter sc ON aa.id = sc.id
WHERE aa.id = $1
`

	insertAlarmArea = `
INSERT INTO alarm.alarm_areas (geom, alarm_duration, radius, center, icon, area, inner_radius, stroke_threshold)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
RETURNING id
`

	updateAlarmAreaSQL = `
UPDATE alarm.alarm_areas SET (geom, alarm_duration, radius, center, icon, area, inner_radius, stroke_threshold) =
 (ST_SetSrid(ST_GeomFromGeoJSON($1), 4326), $2, $3, $4, $5, $6, $7, $8) WHERE id = $9 RETURNING id
`

	insertAlarmAreaMapSQL = `
INSERT INTO alarm.alarm_area_map (alarm_area_unit_id, alarm_area_id)
VALUES ($1, $2)
`

	insertStrokeCounterSQL = `
INSERT INTO alarm.stroke_counter (id) VALUES ($1)
`

	deleteAreaFromAlarmSQL = `
DELETE FROM alarm.alarm_area_map a where a.alarm_area_id = $1 AND a.alarm_area_unit_id = $2
`

	alarmHasAreaSQL = `
SELECT EXISTS(
	SELECT * FROM alarm.alarm_area_map WHERE alarm_area_id = $1 AND alarm_area_unit_id = $2
)
`
)

// LoadStrokeArea fetches a stroke area for a user from db using the userID parameter
func LoadStrokeArea(ctx context.Context, conn *sql.Conn, userID int64) (*StrokeArea, error) {
	var area StrokeArea
	if err := conn.QueryRowContext(ctx, strokeAreaByUserSQL, userID).Scan(
		&area.ID,
		&area.GroupID,
		&area.Geom,
	); err != nil {
		return nil, errors.WithStack(err)
	}

	return &area, nil
}

// LoadStrokeAreaByGroup fetches a single stroke area for a group.
func LoadStrokeAreaByGroup(ctx context.Context, conn *sql.Conn, groupID int64) (*StrokeArea, error) {
	var area StrokeArea
	if err := conn.QueryRowContext(ctx, strokeAreaByGroupSQL, groupID).Scan(
		&area.ID,
		&area.GroupID,
		&area.Geom,
	); err != nil {
		return nil, errors.WithStack(err)
	}

	return &area, nil
}

// LoadStrokeAreaAsWKT returns the geometry of a given userID.
func LoadStrokeAreaAsWKT(ctx context.Context, conn *sql.Conn, userID int64) (wkt.Geom, error) {
	var txt sql.NullString
	if err := conn.QueryRowContext(ctx, strokeAreaSQL, userID).Scan(&txt); err != nil {
		return nil, errors.WithStack(err)
	}

	if !txt.Valid {
		return nil, nil
	}
	return wkt.Parse([]byte(txt.String))
}

// UpdateStrokeArea persists changed attributes of a stroke area.
func UpdateStrokeArea(ctx context.Context, conn *sql.Conn, area *StrokeArea) (int64, error) {
	geomString, err := json.Marshal(area.Geom)
	if err != nil {
		return -1, err
	}
	res, err := conn.ExecContext(ctx, updateStrokeAreaSQL, string(geomString), area.ID)
	if err != nil {
		return -1, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return -1, err
	}
	if count == 0 {
		return -1, fmt.Errorf("No area updated")
	}

	return *area.ID, nil
}

// CreateStrokeArea persists a new stroke area.
func CreateStrokeArea(ctx context.Context, conn *sql.Conn, area *StrokeArea) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, createStrokeAreaSQL, area.GroupID, area.Geom)
	if err != nil {
		log.Println("error: ", err)
		return -1, err
	}
	return id, nil
}

// LoadAlarmAreas fetches areas for a specified alarm.
func LoadAlarmAreas(ctx context.Context, conn *sql.Conn, alarmID int64) ([]AlarmArea, error) {
	var areas []AlarmArea
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var area AlarmArea
		if err := rows.Scan(
			&area.ID,
			&area.Geom,
			&area.AlarmDuration,
			&area.Radius,
			&area.Center,
			&area.Icon,
			&area.Area,
			&area.InnerRadius,
			&area.StrokeThreshold,
			&area.StrokeCounter,
			&area.LastStroke,
		); err != nil {
			return err
		}
		areas = append(areas, area)
		return nil
	}, areaByUnitSQL, alarmID); err != nil {
		return nil, errors.WithStack(err)
	}
	return areas, nil
}

// LoadAlarmAreaByID fetches a single alarm area specified by id.
func LoadAlarmAreaByID(ctx context.Context, conn *sql.Conn, areaID int64) (*AlarmArea, error) {
	var area AlarmArea
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		if err := rows.Scan(
			&area.ID,
			&area.Geom,
			&area.AlarmDuration,
			&area.Radius,
			&area.Center,
			&area.Icon,
			&area.Area,
			&area.InnerRadius,
			&area.StrokeThreshold,
			&area.StrokeCounter,
			&area.LastStroke,
		); err != nil {
			return err
		}
		return nil
	}, areaByIDSQL, areaID); err != nil {
		return nil, errors.WithStack(err)
	}
	return &area, nil
}

// CreateAlarmArea persists a new alarm area for an existing alarm_area_unit.
func CreateAlarmArea(ctx context.Context, conn *sql.Conn, unitID int64, area AlarmArea) (areaID int64, err error) {
	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return -1, err
	}
	defer tx.Rollback()

	err = tx.QueryRow(
		insertAlarmArea,
		area.Geom,
		area.AlarmDuration,
		area.Radius,
		area.Center,
		area.Icon,
		area.Area,
		area.InnerRadius,
		area.StrokeThreshold,
	).Scan(&areaID)
	if err != nil {
		return -1, err
	}
	_, err = tx.Exec(
		insertStrokeCounterSQL,
		areaID,
	)
	if err != nil {
		return -1, err
	}

	_, err = tx.Exec(insertAlarmAreaMapSQL, unitID, areaID)
	if err != nil {
		return -1, err
	}
	return areaID, nil
}

// UpdateAlarmArea persists changed attributes of a stroke area.
func UpdateAlarmArea(ctx context.Context, conn *sql.Conn, area *AlarmArea) (int64, error) {
	geomString, err := json.Marshal(area.Geom)
	if err != nil {
		return -1, err
	}
	res, err := conn.ExecContext(
		ctx,
		updateAlarmAreaSQL,
		string(geomString),
		area.AlarmDuration,
		area.Radius,
		area.Center,
		area.Icon,
		area.Area,
		area.InnerRadius,
		area.StrokeThreshold,
		area.ID)
	if err != nil {
		return -1, err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return -1, err
	}
	if count == 0 {
		return -1, fmt.Errorf("No area updated")
	}

	return area.ID, nil
}

// AddAreaToUnit associates an existing area with an alarm_area_unit.
func AddAreaToUnit(ctx context.Context, conn *sql.Conn, unitID int64, area AlarmArea) error {
	res, err := conn.ExecContext(ctx, insertAlarmAreaMapSQL, unitID, area.ID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("Could not add Area to Alarm")
	}
	return nil
}

// RemoveAreaFromUnit deletes the connection between an alarm area and the unit.
func RemoveAreaFromUnit(ctx context.Context, conn *sql.Conn, unitID, areaID int64) error {
	res, err := conn.ExecContext(ctx, deleteAreaFromAlarmSQL, areaID, unitID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("Could not remove Area from Alarm")
	}
	return nil
}

func AlarmHasArea(ctx context.Context, conn *sql.Conn, unitID, areaID int64) bool {
	var exists bool
	if err := conn.QueryRowContext(ctx, alarmHasAreaSQL, areaID, unitID).Scan(&exists); err != nil {
		return false
	}
	return exists
}
