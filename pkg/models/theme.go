// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// Theme represents the persitence layer of a user
type Theme struct {
	ID   *int64       `json:"id"`
	Name *string      `json:"name"`
	Data *pgtype.JSON `json:"data"`
}

type HostTheme struct {
	ID   *int64       `json:"id"`
	Host *string      `json:"host"`
	Data *pgtype.JSON `json:"data"`
}

const (
	themeSQL = `
SELECT id,
	name,
	data
FROM users.themes t
`

	userThemeSQL = `
SELECT t.id,
	t.name,
	t.data
FROM users.themes t
JOIN users.groups g ON g.theme_id = t.id
JOIN users.users u ON u.group_id = g.id
WHERE u.id = $1
`

	hostThemeSQL = `
SELECT id, host, data
FROM users.host_themes ht
WHERE ht.host = $1	
`
)

// LoadThemeForUser fetches a theme for a user from db using the userID parameter
func LoadThemeForUser(ctx context.Context, conn *sql.Conn, userID int64) (*Theme, error) {
	var theme Theme
	if err := conn.QueryRowContext(ctx, userThemeSQL, userID).Scan(
		&theme.ID,
		&theme.Name,
		&theme.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return &theme, nil
}

// LoadThemes fetches all themes from db
func LoadThemes(ctx context.Context, conn *sql.Conn) ([]Theme, error) {
	var themes []Theme
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		var theme Theme
		if err := row.Scan(
			&theme.ID,
			&theme.Name,
			&theme.Data); err != nil {
			return err
		}
		themes = append(themes, theme)
		return nil
	}, themeSQL)
	if err != nil {
		return nil, err
	}

	return themes, nil
}

// LoadHostTheme fetches a host theme from db.
func LoadHostTheme(ctx context.Context, conn *sql.Conn, host string) (*HostTheme, error) {
	var theme HostTheme
	if err := conn.QueryRowContext(ctx, hostThemeSQL, host).Scan(
		&theme.ID,
		&theme.Host,
		&theme.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return &theme, nil
}
