// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"log"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
)

// Permission represents the persistence layer.
type Permission struct {
	ID          int64   `json:"id"`
	Name        string  `json:"name"`
	ActivityIDs []int64 `json:"activities,omitempty"`
}

const (
	userPermissonsSQL = `
SELECT id, name
FROM users.permissions p
JOIN users.role_permissions rp ON rp.permission_id = p.id
JOIN users.user_roles ur ON rp.role_id = ur.role_id
WHERE ur.user_id = $1
`
	rolePermissonsSQL = `
SELECT id, name
FROM users.permissions p
JOIN users.role_permissions rp ON rp.permission_id = p.id
WHERE rp.role_id = $1
`
	permissionByIDSQL = `
SELECT id, name
FROM users.permissions
WHERE id = $1
`
)

// LoadUserPermissions fetches the permissions for a specified user.
func LoadUserPermissions(ctx context.Context, conn *sql.Conn, userID int64) ([]*Permission, error) {
	var permissions []*Permission
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		var permission Permission
		if err := row.Scan(
			&permission.ID,
			&permission.Name); err != nil {
			log.Println("error: ", err)
			return err
		}
		permissions = append(permissions, &permission)
		return nil
	}, userPermissonsSQL, userID)
	if err != nil {
		log.Println("error: ", err)
		return nil, err
	}
	log.Println("returning permissions.")
	return permissions, nil
}

// LoadPermissionsByRole fetches the permissions for a specified user.
func LoadPermissionsByRole(ctx context.Context, conn *sql.Conn, roleID int64) ([]*Permission, error) {
	var permissions []*Permission
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		var permission Permission
		if err := row.Scan(
			&permission.ID,
			&permission.Name); err != nil {
			log.Println("error: ", err)
			return err
		}
		permissions = append(permissions, &permission)
		return nil
	}, rolePermissonsSQL, roleID)
	if err != nil {
		log.Println("error: ", err)
		return nil, err
	}
	return permissions, nil
}

// LoadPermissionByID fetches a single permission.
func LoadPermissionByID(ctx context.Context, conn *sql.Conn, permissionID int64) (*Permission, error) {
	log.Println("load permission: ", permissionID)
	var permission Permission
	row := conn.QueryRowContext(ctx, permissionByIDSQL, permissionID)
	log.Println("row found: ", row != nil)
	if err := row.Scan(&permission.ID, &permission.Name); err != nil {
		return nil, err
	}
	return &permission, nil
}
