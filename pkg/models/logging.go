// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// Logging represents the datastructure in the db.
type Logging struct {
	ID        int64               `json:"id"`
	User      string              `json:"user"`
	Text      string              `json:"text"`
	Timestamp *pgtype.Timestamptz `json:"timestamp"`
	IP        string              `json:"ip"`
}

const (
	// TODO: Add date filter
	// TODO: What data should be available, all logs group, subgroup, users?!
	userLogsSQL = `
SELECT 
	l.id,
	u.name,
	l.text,
	l.date,
	l.ip
FROM users.logging l
JOIN users.users u ON u.id = l.user_id
WHERE u.id = $1
`

	insertUserLogSQL = `
INSERT INTO users.logging (user_id, text, date, ip)
VALUES($1, $2, $3, $4)
RETURNING id
`
)

// LoadUserLogs fetches the logs associated to a users group from db using the userID parameter
func LoadUserLogs(ctx context.Context, conn *sql.Conn, userID int64) ([]Logging, error) {
	var logs []Logging
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var log Logging
		if err := rows.Scan(
			&log.ID,
			&log.User,
			&log.Text,
			&log.Timestamp,
			&log.IP,
		); err != nil {
			return err
		}
		logs = append(logs, log)
		return nil
	},
		userLogsSQL, userID); err != nil {
		return nil, errors.WithStack(err)
	}
	return logs, nil
}

// CreateUserLog persists the given alarm and connects it to the user.
func CreateUserLog(log Logging, userID int64) (logID int64, err error) {
	if err := database.QueryWithoutCtx(
		func(rows *sql.Rows) error {
			return rows.Scan(&logID)
		},
		insertUserLogSQL,
		userID,
		log.Text,
		log.Timestamp.Time,
		log.IP,
	); err != nil {
		return -1, err
	}
	return logID, nil
}
