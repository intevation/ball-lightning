// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/pkg/errors"
)

type UserRole struct {
	RoleID int64 `json:"role_id"`
	UserID int64 `json:"user_id"`
}

type RolePermission struct {
	RoleID       int64 `json:"role_id"`
	PermissionID int64 `json:"permission_id"`
}

type PermissionActivity struct {
	PermissionID int64 `json:"permission_id"`
	ActivityID   int64 `json:"activity_id"`
}

type Activity struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	Method string `json:"method"`
	URL    string `json:"url"`
}

type UserActivities struct {
	Roles       map[int64]*Role       `json:"roles"`
	Permissions map[int64]*Permission `json:"permissions"`
	Activities  map[int64]*Activity   `json:"activities"`
}

const (
	userRolesSQL = `
SELECT role_id
FROM users.user_roles
WHERE user_id = $1`

	roleSQL = `
SELECT name
FROM users.roles WHERE id = $1`

	rolePermissionsSQL = `
SELECT permission_id
FROM users.role_permissions
WHERE role_id = $1`

	PermissionActivitiesSQL = `
SELECT activity_id
FROM users.permission_activity
WHERE permission_id = $1`

	PermissionSQL = `
SELECT name
FROM users.permissions
WHERE id = $1`

	ActivitySQL = `
SELECT name, method, destination_url
FROM users.activity
WHERE id = $1`

	AllActivitiesSQL = `
SELECT id, name, method, destination_url FROM users.activity`
)

func LoadAllActivities(ctx context.Context, conn *sql.Conn) ([]*Activity, error) {

	var activities []*Activity

	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var a Activity
		if err := rows.Scan(&a.ID, &a.Name, &a.Method, &a.URL); err != nil {
			return err
		}
		activities = append(activities, &a)
		return nil
	}, AllActivitiesSQL); err != nil {
		return nil, errors.WithStack(err)
	}

	return activities, nil
}

func LoadUserRoles(
	ctx context.Context,
	conn *sql.Conn,
	userID int64,
) ([]UserRole, error) {

	var userRoles []UserRole

	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		ur := UserRole{UserID: userID}
		if err := rows.Scan(&ur.RoleID); err != nil {
			return err
		}
		userRoles = append(userRoles, ur)
		return nil
	}, userRolesSQL, userID); err != nil {
		return nil, errors.WithStack(err)
	}

	return userRoles, nil
}

func LoadRoles(
	ctx context.Context,
	conn *sql.Conn,
	userRoles []UserRole,
) (map[int64]*Role, error) {
	roles := map[int64]*Role{}

	for i := range userRoles {
		if roles[userRoles[i].RoleID] != nil {
			continue
		}
		r := Role{ID: userRoles[i].RoleID}
		if err := conn.QueryRowContext(ctx, roleSQL, r.ID).Scan(&r.Name); err != nil {
			return nil, errors.WithStack(err)
		}
		roles[r.ID] = &r
	}
	return roles, nil
}

func LoadRolePermissions(
	ctx context.Context,
	conn *sql.Conn,
	roles map[int64]*Role,
) ([]RolePermission, error) {
	var rolePermissions []RolePermission

	for roleID := range roles {
		if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
			rp := RolePermission{RoleID: roleID}
			if err := rows.Scan(&rp.PermissionID); err != nil {
				return err
			}
			rolePermissions = append(rolePermissions, rp)
			return nil
		}, rolePermissionsSQL, roleID); err != nil {
			return nil, err
		}
	}

	return rolePermissions, nil
}

func LoadPermissions(
	ctx context.Context,
	conn *sql.Conn,
	rolePermissions []RolePermission,
	permissions map[int64]*Permission,
) error {
	for i := range rolePermissions {
		permID := rolePermissions[i].PermissionID
		if permissions[permID] != nil {
			continue
		}
		if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
			p := Permission{ID: permID}
			if err := rows.Scan(&p.Name); err != nil {
				return err
			}
			permissions[permID] = &p
			return nil
		}, PermissionSQL, permID); err != nil {
			return err
		}
	}
	return nil
}

func LoadPermissionActivities(
	ctx context.Context,
	conn *sql.Conn,
	rolePermissions []RolePermission,
) ([]PermissionActivity, error) {

	var permissionActivities []PermissionActivity

	for i := range rolePermissions {
		permID := rolePermissions[i].PermissionID
		if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
			pa := PermissionActivity{PermissionID: permID}
			if err := rows.Scan(&pa.ActivityID); err != nil {
				return err
			}
			permissionActivities = append(permissionActivities, pa)
			return nil
		}, PermissionActivitiesSQL, permID); err != nil {
			return nil, err
		}
	}

	return permissionActivities, nil
}

func LoadActivities(
	ctx context.Context,
	conn *sql.Conn,
	permissionActivities []PermissionActivity,
) (map[int64]*Activity, error) {

	activities := map[int64]*Activity{}

	for i := range permissionActivities {
		actID := permissionActivities[i].ActivityID
		if activities[actID] != nil {
			continue
		}
		if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
			a := Activity{ID: actID}
			if err := rows.Scan(&a.Name, &a.Method, &a.URL); err != nil {
				return err
			}
			activities[actID] = &a
			return nil
		}, ActivitySQL, actID); err != nil {
			return nil, err
		}
	}

	return activities, nil
}

func LoadActivitiesForUser(
	ctx context.Context,
	conn *sql.Conn,
	userID int64,
) (*UserActivities, error) {

	userRoles, err := LoadUserRoles(ctx, conn, userID)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	roles, err := LoadRoles(ctx, conn, userRoles)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	rolePermissions, err := LoadRolePermissions(ctx, conn, roles)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	permissionActivities, err := LoadPermissionActivities(ctx, conn, rolePermissions)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	permissions := map[int64]*Permission{}

	if err := LoadPermissions(ctx, conn, rolePermissions, permissions); err != nil {
		return nil, errors.WithStack(err)
	}

	activities, err := LoadActivities(ctx, conn, permissionActivities)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// place activities as indices directly into the permissions.
	for i := range permissionActivities {
		pa := &permissionActivities[i]
		if p := permissions[pa.PermissionID]; p != nil {
			p.ActivityIDs = append(p.ActivityIDs, pa.ActivityID)
		}
	}

	// place permissions as indices directly into the roles.
	for i := range rolePermissions {
		rp := &rolePermissions[i]
		if r := roles[rp.RoleID]; r != nil {
			r.PermissionIDs = append(r.PermissionIDs, rp.PermissionID)
		}
	}

	return &UserActivities{
		Activities:  activities,
		Permissions: permissions,
		Roles:       roles,
	}, nil
}
