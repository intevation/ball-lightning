// Copyright 2018 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"math"

	"github.com/mb0/wkt"
)

// BBox is a bounding box of an area.
type BBox struct {
	MinX float64
	MinY float64
	MaxX float64
	MaxY float64
}

// Contains checks if a given point is inside these bounding box.
func (bb *BBox) Contains(p *wkt.Point) bool {
	return p.X >= bb.MinX && p.X <= bb.MaxX &&
		p.Y >= bb.MinY && p.Y <= bb.MaxY
}

// GeomContainsPoint checks if a point is inside a given geometry.
func GeomContainsPoint(g wkt.Geom, point *wkt.Point) bool {
	switch o := g.(type) {
	case *wkt.Polygon:
		return PolygonContainsPoint(o, point)
	case *wkt.MultiPolygon:
		return MultiPolygonContainsPoint(o, point)
	}
	// TODO: Is it worth to implement the others?
	return false
}

// PolygonContainsPoint checks if a polygon contains a given point.
func PolygonContainsPoint(poly *wkt.Polygon, point *wkt.Point) bool {
	return ringsContainsPoint(poly.Rings, point)
}

// MultiPolygonContainsPoint checks if a multi-polygon contains a given point.
func MultiPolygonContainsPoint(poly *wkt.MultiPolygon, point *wkt.Point) bool {
	for _, polygon := range poly.Polygons {
		if ringsContainsPoint(polygon, point) {
			return true
		}
	}
	return false
}

func ringsContainsPoint(rings [][]wkt.Coord, point *wkt.Point) bool {

	// Test if in shell
	if len(rings) == 0 || !ringContainsPoint(rings[0], &point.Coord) {
		return false
	}

	// Test if its in a hole.
	for _, hole := range rings[1:] {
		// Does the reverse order of the holes matter?
		if ringContainsPoint(hole, &point.Coord) {
			return false
		}
	}

	return true
}

func ringIsClosed(ring []wkt.Coord) bool {
	return len(ring) >= 3
}

func ringContainsPoint(ring []wkt.Coord, point *wkt.Coord) bool {
	if !ringIsClosed(ring) {
		return false
	}

	start := len(ring) - 1
	end := 0

	contains := intersectsWithRaycast(ring, point, &ring[start], &ring[end])

	for i := 1; i < len(ring); i++ {
		if intersectsWithRaycast(ring, point, &ring[i-1], &ring[i]) {
			contains = !contains
		}
	}

	return contains
}

// Using the raycast algorithm, this returns whether or not the passed in point
// Intersects with the edge drawn by the passed in start and end points.
// Original implementation: http://rosettacode.org/wiki/Ray-casting_algorithm#Go
func intersectsWithRaycast(ring []wkt.Coord, point, start, end *wkt.Coord) bool {

	// Always ensure that the the first point
	// has a y coordinate that is less than the second point
	if start.Y > end.Y {
		// Switch the points if otherwise.
		start, end = end, start
	}

	// Move the point's y coordinate
	// outside of the bounds of the testing region
	// so we can start drawing a ray
	for point.Y == start.Y || point.Y == end.Y {
		y := math.Nextafter(point.Y, math.Inf(1))
		point = &wkt.Coord{X: point.X, Y: y}
	}

	// If we are outside of the polygon, indicate so.
	if point.Y < start.Y || point.Y > end.Y {
		return false
	}

	if start.X > end.X {
		if point.X > start.X {
			return false
		}
		if point.X < end.X {
			return true
		}
	} else {
		if point.X > end.X {
			return false
		}
		if point.X < start.X {
			return true
		}
	}

	raySlope := (point.Y - start.Y) / (point.X - start.X)
	diagSlope := (end.Y - start.Y) / (end.X - start.X)

	return raySlope >= diagSlope
}

func bboxCoords(coords []wkt.Coord, bbox *BBox) {
	for i := range coords {
		x, y := coords[i].X, coords[i].Y
		if x < bbox.MinX {
			bbox.MinX = x
		}
		if x > bbox.MaxX {
			bbox.MaxX = x
		}
		if y < bbox.MinY {
			bbox.MinY = y
		}
		if y > bbox.MaxY {
			bbox.MaxY = y
		}
	}
}

// CalculateBBox return the bounding box of given geometry.
func CalculateBBox(g wkt.Geom) *BBox {
	if o, ok := g.(*wkt.Point); ok {
		return &BBox{
			MinX: o.X,
			MinY: o.Y,
			MaxX: o.X,
			MaxY: o.Y,
		}
	}
	bbox := BBox{
		MinX: math.MaxFloat64,
		MaxX: -math.MaxFloat64,
		MinY: math.MaxFloat64,
		MaxY: -math.MaxFloat64,
	}
	switch o := g.(type) {
	case *wkt.Polygon:
		if len(o.Rings) > 0 {
			bboxCoords(o.Rings[0], &bbox)
		}
	case *wkt.MultiPolygon:
		for _, p := range o.Polygons {
			if len(p) > 0 {
				bboxCoords(p[0], &bbox)
			}
		}
	case *wkt.MultiPoint:
		bboxCoords(o.Coords, &bbox)
	case *wkt.LineString:
		bboxCoords(o.Coords, &bbox)
	}
	return &bbox
}
