// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/pkg/errors"
)

// PWResetUser is send to request a password reset for a user.
type PWResetUser struct {
	User string `json:"user"`
}

// User represents the persitence layer of a user
type User struct {
	ID           *int64        `json:"id"`
	Name         *string       `json:"name"`
	Password     *string       `json:"password"`
	GroupID      *int64        `json:"groupId"`
	StartDate    *sql.NullTime `json:"startDate"`
	StopDate     *sql.NullTime `json:"stopDate"`
	FirstName    *string       `json:"firstName"`
	LastName     *string       `json:"lastName"`
	Company      *string       `json:"company"`
	Division     *string       `json:"division"`
	Street       *string       `json:"street"`
	ZIP          *string       `json:"zip"`
	City         *string       `json:"city"`
	PhoneNumber  *string       `json:"phonenumber"`
	Annotation   *string       `json:"annotation"`
	MobileNumber *string       `json:"mobilenumber"`
	EMail        *string       `json:"email"`
}

const (
	userSQL = `
SELECT id,
	name,
	group_id,
	start_date,
	stop_date,
	first_name,
	last_name,
	company, division,
	street,
	zip,
	city,
	phonenumber,
	annotation,
	mobilenumber,
	email
FROM users.users
WHERE id = $1
`
	groupUsersSQL = `
SELECT id,
	name,
	group_id,
	start_date,
	stop_date,
	first_name,
	last_name,
	company, division,
	street,
	zip,
	city,
	phonenumber,
	annotation,
	mobilenumber,
	email
FROM users.users
WHERE group_id = $1
`
	insertUserSQL = `
INSERT INTO users.users values(
	DEFAULT,
	$1,
	$2,
	$3,
	$4,
	$5,
	$6,
	$7,
	$8,
	$9,
	$10,
	$11,
	$12,
	$13,
	$14,
	$15,
	$16
) RETURNING id
`
	updateUserSQL = `
UPDATE users.users
	SET
	(name, group_id, start_date, stop_date, first_name, last_name, company, division, street, zip, city, phonenumber, annotation, mobilenumber, email) = 
	($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)
	WHERE id = $16
RETURNING id
`
	deleteUserSQL = `
DELETE FROM users.users
WHERE id = $1
`
)

// LoadUser fetches a single user from db using the userID parameter
func LoadUser(ctx context.Context, conn *sql.Conn, userID int64) (*User, error) {
	var user User
	if err := conn.QueryRowContext(ctx, userSQL, userID).Scan(
		&user.ID,
		&user.Name,
		&user.GroupID,
		&user.StartDate,
		&user.StopDate,
		&user.FirstName,
		&user.LastName,
		&user.Company,
		&user.Division,
		&user.Street,
		&user.ZIP,
		&user.City,
		&user.PhoneNumber,
		&user.Annotation,
		&user.MobileNumber,
		&user.EMail); err != nil {
		return nil, errors.WithStack(err)
	}

	return &user, nil
}

// LoadUsersByGroup fetches all users of a specified group.
func LoadUsersByGroup(ctx context.Context, conn *sql.Conn, groupID int64) ([]*User, error) {
	var users []*User
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		var user User
		if err := row.Scan(
			&user.ID,
			&user.Name,
			&user.GroupID,
			&user.StartDate,
			&user.StopDate,
			&user.FirstName,
			&user.LastName,
			&user.Company,
			&user.Division,
			&user.Street,
			&user.ZIP,
			&user.City,
			&user.PhoneNumber,
			&user.Annotation,
			&user.MobileNumber,
			&user.EMail); err != nil {
			return err
		}
		users = append(users, &user)
		return nil
	}, groupUsersSQL, groupID)
	if err != nil {
		return nil, err
	}
	return users, nil
}

// CreateUser writes a new user to db.
func CreateUser(ctx context.Context, conn *sql.Conn, user *User) (id int64, err error) {
	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return -1, err
	}
	defer tx.Rollback()

	err = tx.QueryRowContext(
		ctx,
		insertUserSQL,
		user.Name,
		user.Password,
		user.GroupID,
		user.StartDate.Time,
		user.StopDate.Time,
		user.FirstName,
		user.LastName,
		user.Company,
		user.Division,
		user.Street,
		user.ZIP,
		user.City,
		user.PhoneNumber,
		user.Annotation,
		user.MobileNumber,
		user.EMail,
	).Scan(&id)
	if err != nil {
		return -1, err
	}

	err = database.TransactionExec(ctx, tx, createUserProfileSQL, id)
	if err != nil {
		return -1, err
	}

	err = database.TransactionExec(ctx, tx, createAlarmSettingsSQL, id)
	if err != nil {
		return -1, err
	}
	err = tx.Commit()
	if err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateUser persists changes in users attributes.
func UpdateUser(ctx context.Context, conn *sql.Conn, user *User) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, updateUserSQL,
		user.Name,
		user.GroupID,
		user.StartDate,
		user.StopDate,
		user.FirstName,
		user.LastName,
		user.Company,
		user.Division,
		user.Street,
		user.ZIP,
		user.City,
		user.PhoneNumber,
		user.Annotation,
		user.MobileNumber,
		user.EMail,
		user.ID,
	)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteUser removes a user from db.
func DeleteUser(ctx context.Context, conn *sql.Conn, userID int64) error {
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		return nil
	}, deleteUserSQL, userID)
	if err != nil {
		return err
	}
	return nil
}
