// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"encoding/json"
	"time"
)

const (
	// TimeFormat is the default format
	TimeFormat = time.RFC3339
	// TimeFormatMicro includes micro seconds
	TimeFormatMicro = "2006-01-02T15:04:05.999Z07:00"
	// TimeFormatMicroLocal is in local time
	TimeFormatMicroLocal = "2006-01-02T15:04:05.000"
	// DateFormat is for dates without time.
	DateFormat = "2006-01-02"
)

// TimeParser is a list of time formats.
type TimeParser []string

// Date is a wrapper for time.Time
type Date struct{ time.Time }

// Time is a wrapper for time.Time
type Time struct{ time.Time }

// ParseTime parses the time formats.
var ParseTime = TimeParser{
	TimeFormat,
	TimeFormatMicro,
	TimeFormatMicroLocal,
	DateFormat,
}.Parse

// Parse tries to parse a given string by the entries of the layout
// list one after another. The first matching time is returned.
// If no layout matches the last error is returned or time zero
// if the layout list is empty.
func (tg TimeParser) Parse(s string) (time.Time, error) {
	var err error
	var t time.Time
	for _, layout := range tg {
		if t, err = time.Parse(layout, s); err == nil {
			break
		}
	}
	return t, err
}

// MarshalJSON returns the JSON encoding of Date
func (d Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(d.Format(DateFormat))
}

// UnmarshalJSON parses the JSON encoded Date.
func (d *Date) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	d2, err := time.Parse(DateFormat, s)
	if err == nil {
		*d = Date{d2}
	}
	return err
}

// MarshalJSON returns the JSON encoding of Time
func (t Time) MarshalJSON() ([]byte, error) {
	return json.Marshal(t.Format(TimeFormat))
}

// UnmarshalJSON pases the JSON encodes Time
func (t *Time) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	t2, err := time.Parse(TimeFormat, s)
	if err == nil {
		*t = Time{t2}
	}
	return err
}
