// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
)

// Template represents the persitence layer of all types of templates
type Template struct {
	ID              int64   `json:"id"`
	AlarmSubject    *string `json:"alarmSubject"`
	AllClearSubject *string `json:"allClearSubject"`
	AlarmText       *string `json:"alarmText"`
	AllClearText    *string `json:"allClearText"`
}

const (
	templateFaxSQL = `
SELECT id, alarm_subject, allclear_subject, alarm_text, allclear_text
FROM users.alarm_templates_fax
`
	templateMailSQL = `
SELECT id, alarm_subject, allclear_subject, alarm_text, allclear_text
FROM users.alarm_templates_mail
`
	templateMQTTSQL = `
SELECT id, alarm_subject, allclear_subject, alarm_text, allclear_text
FROM users.alarm_templates_mqtt
`
	templatePhoneSQL = `
SELECT id, alarm_subject, allclear_subject, alarm_text, allclear_text
FROM users.alarm_templates_phone
`
	templateSMSSQL = `
SELECT id, alarm_subject, allclear_subject, alarm_text, allclear_text
FROM users.alarm_templates_sms
`
)

func loadTemplate(ctx context.Context, conn *sql.Conn, query string) ([]Template, error) {
	var templates []Template

	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var template Template
		if err := rows.Scan(
			&template.ID,
			&template.AlarmSubject,
			&template.AllClearSubject,
			&template.AlarmText,
			&template.AllClearText,
		); err != nil {
			return err
		}
		templates = append(templates, template)
		return nil
	}, query); err != nil {
		return nil, err
	}
	return templates, nil
}

// LoadTemplateFax fetches all templates of type fax.
func LoadTemplateFax(ctx context.Context, conn *sql.Conn) ([]Template, error) {
	return loadTemplate(ctx, conn, templateFaxSQL)
}

// LoadTemplateMail fetches all templates of type mail.
func LoadTemplateMail(ctx context.Context, conn *sql.Conn) ([]Template, error) {
	return loadTemplate(ctx, conn, templateMailSQL)
}

// LoadTemplateMQTT fetches all templates of type mqtt.
func LoadTemplateMQTT(ctx context.Context, conn *sql.Conn) ([]Template, error) {
	return loadTemplate(ctx, conn, templateMQTTSQL)
}

// LoadTemplatePhone fetches all templates of type phone.
func LoadTemplatePhone(ctx context.Context, conn *sql.Conn) ([]Template, error) {
	return loadTemplate(ctx, conn, templatePhoneSQL)
}

// LoadTemplateSMS fetches all templates of type sms.
func LoadTemplateSMS(ctx context.Context, conn *sql.Conn) ([]Template, error) {
	return loadTemplate(ctx, conn, templateSMSSQL)
}
