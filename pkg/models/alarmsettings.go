// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"time"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/jackc/pgx/pgtype"
	"github.com/pkg/errors"
)

// AlarmSettings represents the persistence layer
type AlarmSettings struct {
	ID                  *int64              `json:"id"`
	AlarmTemplateMailID *int64              `json:"alarmTemplateMailId"`
	AlarmTemplateSmsID  *int64              `json:"alarmTemplateSmsId"`
	AlarmTemplateMqttID *int64              `json:"alarmTemplateMqttId"`
	AlarmTemplateFaxID  *int64              `json:"alarmTemplateFaxId"`
	Sms                 *string             `json:"sms"`
	Email               *string             `json:"email"`
	Phone               *string             `json:"phone"`
	Fax                 *string             `json:"fax"`
	StartDate           *pgtype.Timestamptz `json:"startDate"`
	StopDate            *pgtype.Timestamptz `json:"stopDate"`
	StartTime           *time.Time          `json:"startTime"`
	StopTime            *time.Time          `json:"stopTime"`
	Monday              *bool               `json:"monday"`
	Tuesday             *bool               `json:"tuesday"`
	Wednesday           *bool               `json:"wednesday"`
	Thursday            *bool               `json:"thursday"`
	Friday              *bool               `json:"friday"`
	Saturday            *bool               `json:"saturday"`
	Sunday              *bool               `json:"sunday"`
	OnlyLog             bool                `json:"onlyLog"`
	StrokeProtocol      bool                `json:"strokeProtocol"`
	StrokeAsCSV         bool                `json:"strokeAsCSV"`
}

const (
	alarmSettingsSQL = `
SELECT id,
	alarm_template_mail_id,
	alarm_template_sms_id,
	alarm_template_mqtt_id,
	alarm_template_fax_id,
	sms,
	email,
	phone,
	fax,
	start_date,
	stop_date,
	start_time,
	stop_time,
	mon,
	tue,
	wed,
	thu,
	fri,
	sat,
	sun,
	only_log,
	stroke_protocol,
	stroke_as_csv
FROM users.alarm_settings a
WHERE a.id = $1
`

	createAlarmSettingsSQL = `
INSERT INTO users.alarm_settings (
	id,
	start_date,
	stop_date,
	mon,
	tue,
	wed,
	thu,
	fri,
	sat,
	sun
) VALUES ($1, NOW(), NOW(), false, false, false, false, false, false, false)
`
	updateAlarmSettingsSQL = `
UPDATE users.alarm_settings SET (
	alarm_template_mail_id,
	alarm_template_sms_id,
	alarm_template_mqtt_id,
	alarm_template_fax_id,
	sms,
	email,
	phone,
	fax,
	start_date,
	stop_date,
	start_time,
	stop_time,
	mon,
	tue,
	wed,
	thu,
	fri,
	sat,
	sun,
	only_log,
	stroke_protocol,
	stroke_as_csv
) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22)
WHERE id = $23
RETURNING id
`
)

// LoadAlarmSettings fetches alarm settings for a user from db using the userID parameter
func LoadAlarmSettings(ctx context.Context, conn *sql.Conn, userID int64) (*AlarmSettings, error) {
	var settings AlarmSettings
	if err := conn.QueryRowContext(ctx, alarmSettingsSQL, userID).Scan(
		&settings.ID,
		&settings.AlarmTemplateMailID,
		&settings.AlarmTemplateSmsID,
		&settings.AlarmTemplateMqttID,
		&settings.AlarmTemplateFaxID,
		&settings.Sms,
		&settings.Email,
		&settings.Phone,
		&settings.Fax,
		&settings.StartDate,
		&settings.StopDate,
		&settings.StartTime,
		&settings.StopTime,
		&settings.Monday,
		&settings.Tuesday,
		&settings.Wednesday,
		&settings.Thursday,
		&settings.Friday,
		&settings.Saturday,
		&settings.Sunday,
		&settings.OnlyLog,
		&settings.StrokeProtocol,
		&settings.StrokeAsCSV,
	); err != nil {
		return nil, errors.WithStack(err)
	}

	return &settings, nil
}

// CreateAlarmSettings persists new and empty alarm settings for a user.
func CreateAlarmSettings(ctx context.Context, conn *sql.Conn, userID int64) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, createAlarmSettingsSQL,
		userID,
	)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateAlarmSettings persists changes in alarmsettings.
func UpdateAlarmSettings(ctx context.Context, conn *sql.Conn, settings AlarmSettings) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, updateAlarmSettingsSQL,
		settings.AlarmTemplateMailID,
		settings.AlarmTemplateSmsID,
		settings.AlarmTemplateMqttID,
		settings.AlarmTemplateFaxID,
		settings.Sms,
		settings.Email,
		settings.Phone,
		settings.Fax,
		settings.StartDate,
		settings.StopDate,
		settings.StartTime,
		settings.StopTime,
		settings.Monday,
		settings.Tuesday,
		settings.Wednesday,
		settings.Thursday,
		settings.Friday,
		settings.Saturday,
		settings.Sunday,
		settings.OnlyLog,
		settings.StrokeProtocol,
		settings.StrokeAsCSV,
		settings.ID,
	)
	if err != nil {
		return -1, err
	}
	return id, nil
}
