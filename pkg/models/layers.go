// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/pkg/errors"
)

// Layer represents the datastructure for map_layers in the db.
type Layer struct {
	ID                int64           `json:"id"`
	GeoserverName     *string         `json:"geoserverName"`
	Name              *string         `json:"name"`
	Reload            bool            `json:"reload"`
	BaseLayer         bool            `json:"baseLayer"`
	TimeLayer         bool            `json:"timeLayer"`
	TickWidth         *int64          `json:"tickWidth"`
	LayerType         string          `json:"layerType"`
	SingleTile        bool            `json:"singleTile"`
	DefaultName       *string         `json:"defaultName"`
	ExternalServer    *string         `json:"externalServer"`
	ExternalParameter json.RawMessage `json:"externalParameter"`
}

// LayerForGroup represents the datastructure for group_map_layers in the db.
type LayerForGroup struct {
	ID            int64           `json:"id"`
	GroupID       int64           `json:"groupId"`
	LayerID       int64           `json:"layerID"`
	ReloadTime    int64           `json:"reloadTime"`
	DisplayName   *string         `json:"displayName"`
	FeatureInfo   bool            `json:"featureInfo"`
	CheckOnLogin  bool            `json:"checkedOnLogin"`
	Permanent     bool            `json:"permanent"`
	CQLFilter     json.RawMessage `json:"cqlFilter"`
	PopupTemplate *string         `json:"popupTemplate"`
	ZIndex        *int64          `json:"zIndex"`
}

const (
	mapLayerSQL = `
SELECT 
	l.id,
	l.geoserver_name,
	l.name,
	l.reload,
	l.baselayer,
	l.timelayer,
	l.tick_width,
	l.layer_type,
	l.single_tile,
	l.default_name,
	l.external_server,
	l.external_parameter
FROM users.map_layers l
`

	mapLayerByIDSQL = `
SELECT 
	l.id,
	l.geoserver_name,
	l.name,
	l.reload,
	l.baselayer,
	l.timelayer,
	l.tick_width,
	l.layer_type,
	l.single_tile,
	l.default_name,
	l.external_server,
	l.external_parameter
FROM users.map_layers l
WHERE l.id = $1
`

	insertLayerSQL = `
INSERT INTO users.map_layers (
	geoserver_name,
	name,
	reload,
	baselayer,
	timelayer,
	tick_width,
	layer_type,
	single_tile,
	default_name,
	external_server,
	external_parameter
) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11::JSON)
RETURNING id
`

	updateLayerSQL = `
UPDATE users.map_layers SET (
	geoserver_name,
	name,
	reload,
	baselayer,
	timelayer,
	tick_width,
	layer_type,
	single_tile,
	default_name,
	external_server,
	external_parameter
) = ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11::json)
WHERE id = $12
RETURNING id
`

	deleteLayerSQL = `
DELETE FROM users.map_layers where id = $1
`

	groupLayerSQL = `
SELECT
	id,
	group_id,
	map_layer_id,
	reload_time,
	display_name,
	feature_info,
	checked_on_login,
	permanent,
	cql_filter,
	popup_template,
	zindex
FROM users.group_map_layer
WHERE group_id = $1
`
	groupLayerForUserSQL = `
SELECT
	gl.id,
	gl.group_id,
	gl.map_layer_id,
	gl.reload_time,
	gl.display_name,
	gl.feature_info,
	gl.checked_on_login,
	gl.permanent,
	gl.cql_filter,
	gl.popup_template,
	gl.zindex
FROM users.group_map_layer gl
JOIN users.users u ON u.group_id = gl.group_id
WHERE u.id = $1
`

	groupLayerByIDSQL = `
SELECT
	id,
	group_id,
	map_layer_id,
	reload_time,
	display_name,
	feature_info,
	checked_on_login,
	permanent,
	cql_filter,
	popup_template,
	zindex
FROM users.group_map_layer
WHERE group_id = $1 AND id = $2
`

	insertGroupLayerSQL = `
INSERT INTO users.group_map_layer (
	group_id,
	map_layer_id,
	reload_time,
	display_name,
	feature_info,
	checked_on_login,
	permanent,
	cql_filter,
	popup_template,
	zindex
) VALUES ($1, $2, $3, $4, $5, $6, $7, $8::JSON, $9, $10)
RETURNING id
`

	updateGroupLayerSQL = `
UPDATE users.group_map_layer SET (
	group_id,
	map_layer_id,
	reload_time,
	display_name,
	feature_info,
	checked_on_login,
	permanent,
	cql_filter,
	popup_template,
	zindex
) = ($1, $2, $3, $4, $5, $6, $7, $8::JSON, $9, $10)
WHERE id = $11
RETURNING id
`

	deleteGroupLayerSQL = `
DELETE FROM users.group_map_layer where id = $1
`
)

// LoadMapLayers fetches the map layers from db
func LoadMapLayers(ctx context.Context, conn *sql.Conn) ([]Layer, error) {
	var layers []Layer
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var layer Layer
		var tmp *string
		if err := rows.Scan(
			&layer.ID,
			&layer.GeoserverName,
			&layer.Name,
			&layer.Reload,
			&layer.BaseLayer,
			&layer.TimeLayer,
			&layer.TickWidth,
			&layer.LayerType,
			&layer.SingleTile,
			&layer.DefaultName,
			&layer.ExternalServer,
			&tmp,
		); err != nil {
			return err
		}
		if tmp != nil {
			layer.ExternalParameter = json.RawMessage(*tmp)
		} else {
			layer.ExternalParameter = nil
		}

		layers = append(layers, layer)
		return nil
	},
		mapLayerSQL); err != nil {
		return nil, errors.WithStack(err)
	}
	return layers, nil
}

// LoadMapLayerByID fetches a layer identified by its id from the database.
func LoadMapLayerByID(ctx context.Context, conn *sql.Conn, layerID int64) (*Layer, error) {
	var layer Layer
	var tmp *string
	if err := conn.QueryRowContext(ctx, mapLayerByIDSQL, layerID).Scan(
		&layer.ID,
		&layer.GeoserverName,
		&layer.Name,
		&layer.Reload,
		&layer.BaseLayer,
		&layer.TimeLayer,
		&layer.TickWidth,
		&layer.LayerType,
		&layer.SingleTile,
		&layer.DefaultName,
		&layer.ExternalServer,
		&tmp,
	); err != nil {
		return nil, err
	}
	if tmp != nil {
		layer.ExternalParameter = json.RawMessage(*tmp)
	} else {
		layer.ExternalParameter = nil
	}

	return &layer, nil
}

// CreateMapLayer persists the given layer.
func CreateMapLayer(ctx context.Context, conn *sql.Conn, layer *Layer) (layerID int64, err error) {
	var id int64
	if err := conn.QueryRowContext(
		ctx,
		insertLayerSQL,
		layer.GeoserverName,
		layer.Name,
		layer.Reload,
		layer.BaseLayer,
		layer.TimeLayer,
		layer.TickWidth,
		layer.LayerType,
		layer.SingleTile,
		layer.DefaultName,
		layer.ExternalServer,
		string(layer.ExternalParameter),
	).Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateMapLayer persists new attributes in an existing layer.
func UpdateMapLayer(ctx context.Context, conn *sql.Conn, layer *Layer) (layerID int64, err error) {
	var id int64
	if err := conn.QueryRowContext(
		ctx,
		updateLayerSQL,
		layer.GeoserverName,
		layer.Name,
		layer.Reload,
		layer.BaseLayer,
		layer.TimeLayer,
		layer.TickWidth,
		layer.LayerType,
		layer.SingleTile,
		layer.DefaultName,
		layer.ExternalServer,
		string(layer.ExternalParameter),
		layer.ID,
	).Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteMapLayer removes the the maplayer freom db.
func DeleteMapLayer(ctx context.Context, conn *sql.Conn, layerID int64) error {
	res, err := conn.ExecContext(ctx, deleteLayerSQL, layerID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("No layer deleted")
	}
	return nil
}

// LoadGroupMapLayers fetches the map layers from db
func LoadGroupMapLayers(ctx context.Context, conn *sql.Conn, groupID int64) ([]LayerForGroup, error) {
	var layers []LayerForGroup
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var layer LayerForGroup
		var tmp *string
		if err := rows.Scan(
			&layer.ID,
			&layer.GroupID,
			&layer.LayerID,
			&layer.ReloadTime,
			&layer.DisplayName,
			&layer.FeatureInfo,
			&layer.CheckOnLogin,
			&layer.Permanent,
			&tmp,
			&layer.PopupTemplate,
			&layer.ZIndex,
		); err != nil {
			return err
		}
		if tmp != nil {
			layer.CQLFilter = json.RawMessage(*tmp)
		} else {
			layer.CQLFilter = nil
		}
		layers = append(layers, layer)
		return nil
	},
		groupLayerSQL, groupID); err != nil {
		return nil, errors.WithStack(err)
	}
	return layers, nil
}

// LoadMapLayersUser fetches the map layers from db
func LoadMapLayersUser(ctx context.Context, conn *sql.Conn, userID int64) ([]LayerForGroup, error) {
	var layers []LayerForGroup
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var layer LayerForGroup
		var tmp *string
		if err := rows.Scan(
			&layer.ID,
			&layer.GroupID,
			&layer.LayerID,
			&layer.ReloadTime,
			&layer.DisplayName,
			&layer.FeatureInfo,
			&layer.CheckOnLogin,
			&layer.Permanent,
			&tmp,
			&layer.PopupTemplate,
			&layer.ZIndex,
		); err != nil {
			return err
		}
		if tmp != nil {
			layer.CQLFilter = json.RawMessage(*tmp)
		} else {
			layer.CQLFilter = nil
		}
		layers = append(layers, layer)
		return nil
	},
		groupLayerForUserSQL, userID); err != nil {
		return nil, errors.WithStack(err)
	}
	return layers, nil
}

// LoadGroupMapLayerByID fetches a group layer identified by its id from the database.
func LoadGroupMapLayerByID(ctx context.Context, conn *sql.Conn, groupID, layerID int64) (*LayerForGroup, error) {
	var layer LayerForGroup
	var tmp *string
	if err := conn.QueryRowContext(ctx, groupLayerByIDSQL, groupID, layerID).Scan(
		&layer.ID,
		&layer.GroupID,
		&layer.LayerID,
		&layer.ReloadTime,
		&layer.DisplayName,
		&layer.FeatureInfo,
		&layer.CheckOnLogin,
		&layer.Permanent,
		&tmp,
		&layer.PopupTemplate,
		&layer.ZIndex,
	); err != nil {
		return nil, err
	}
	if tmp != nil {
		layer.CQLFilter = json.RawMessage(*tmp)
	} else {
		layer.CQLFilter = nil
	}
	return &layer, nil
}

// CreateGroupMapLayer persists the given layer.
func CreateGroupMapLayer(ctx context.Context, conn *sql.Conn, groupID int64, layer *LayerForGroup) (layerID int64, err error) {
	var id int64
	if err := conn.QueryRowContext(
		ctx,
		insertGroupLayerSQL,
		groupID,
		layer.LayerID,
		layer.ReloadTime,
		layer.DisplayName,
		layer.FeatureInfo,
		layer.CheckOnLogin,
		layer.Permanent,
		string(layer.CQLFilter),
		layer.PopupTemplate,
		layer.ZIndex,
	).Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateGroupMapLayer persists new attributes in an existing layer.
func UpdateGroupMapLayer(ctx context.Context, conn *sql.Conn, layer *LayerForGroup) (layerID int64, err error) {
	var id int64
	if err := conn.QueryRowContext(
		ctx,
		updateGroupLayerSQL,
		layer.GroupID,
		layer.LayerID,
		layer.ReloadTime,
		layer.DisplayName,
		layer.FeatureInfo,
		layer.CheckOnLogin,
		layer.Permanent,
		string(layer.CQLFilter),
		layer.PopupTemplate,
		layer.ZIndex,
		layer.ID,
	).Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteGroupMapLayer removes the the maplayer freom db.
func DeleteGroupMapLayer(ctx context.Context, conn *sql.Conn, groupID, layerID int64) error {
	res, err := conn.ExecContext(ctx, deleteGroupLayerSQL, layerID)
	if err != nil {
		return err
	}
	count, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("No layer deleted")
	}
	return nil
}
