// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package models

import (
	"context"
	"database/sql"

	"bitbucket.org/intevation/ball-lightning/pkg/database"
	"github.com/pkg/errors"
)

// Role represents the persitence layer of a role
type Role struct {
	ID            int64   `json:"id"`
	Parent        *int64  `json:"parent"`
	Name          *string `json:"name"`
	PermissionIDs []int64 `json:"permissions,omitempty"`
}

const (
	roleUserSQL = `
WITH RECURSIVE search_graph(id, parent, name) AS (
	SELECT id, parent, name
	FROM users.roles
	WHERE id = (
		SELECT ur.role_id
		FROM users.user_roles ur
		JOIN users.users u ON u.id = ur.user_id
		WHERE u.id = $1)
	UNION ALL
	SELECT g.id, g.parent, g.name
	FROM users.roles g
	JOIN search_graph p ON g.parent = p.id
)
SELECT id, parent, name FROM search_graph;
`
	roleByIDSQL = `
SELECT id, parent, name
FROM users.roles
WHERE id = $1
`
	insertRoleSQL = `
INSERT INTO users.roles VALUES (
	DEFAULT, $1, $2
) RETURNING id
`
	updateRoleSQL = `
UPDATE users.roles SET (
	parent, name) = (
	$1, $2
) WHERE id = $3
RETURNING id
`
	deleteRoleSQL = `
DELETE FROM users.roles where id = $1
`
	deletePermissionRecursiveSQL = `
WITH RECURSIVE search_graph(id, parent, name) AS (
	SELECT id, parent, name
	FROM users.roles
	WHERE id = $1
	UNION ALL
	SELECT g.id, g.parent, g.name
	FROM users.roles g
	JOIN search_graph p ON g.parent = p.id
)
DELETE from users.role_permissions
WHERE permission_id = $2
	AND role_id in (
		SELECT DISTINCT p.role_id
		FROM users.role_permissions p
		JOIN search_graph s ON s.id = role_id
	)
 `
	insertRolePermissionSQL = `
INSERT INTO users.role_permissions
VALUES ($1, $2)
`
	deleteRolePermissionSQL = `
DELETE FROM users.role_permissions
WHERE role_id = $1 AND permission_id = $2
`
)

// LoadRolesForUser fetches roles for a user from db using the userID parameter
func LoadRolesForUser(ctx context.Context, conn *sql.Conn, userID int64) (*[]Role, error) {
	var roles []Role
	if err := database.Query(ctx, conn, func(rows *sql.Rows) error {
		var role Role
		if err := rows.Scan(
			&role.ID,
			&role.Parent,
			&role.Name); err != nil {
			return err
		}
		roles = append(roles, role)
		return nil
	},
		roleUserSQL, userID); err != nil {
		return nil, errors.WithStack(err)
	}

	return &roles, nil
}

// LoadRoleByID retches a role from db specified by roleID.
func LoadRoleByID(ctx context.Context, conn *sql.Conn, roleID int64) (*Role, error) {
	var role Role
	if err := conn.QueryRowContext(ctx, roleByIDSQL, roleID).Scan(
		&role.ID,
		&role.Parent,
		&role.Name); err != nil {
		return nil, err
	}
	return &role, nil
}

// CreateRole writes a new role to db.
func CreateRole(ctx context.Context, conn *sql.Conn, role *Role) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, insertRoleSQL,
		role.Parent,
		role.Name)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// UpdateRole persists changes in role attributes.
func UpdateRole(ctx context.Context, conn *sql.Conn, role *Role) (int64, error) {
	var id int64
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		err := row.Scan(&id)
		if err != nil {
			return err
		}
		return nil
	}, updateRoleSQL,
		role.Parent,
		role.Name,
		role.ID)
	if err != nil {
		return -1, err
	}
	return id, nil
}

// DeleteRole removes a role from db.
func DeleteRole(ctx context.Context, conn *sql.Conn, roleID int64) error {
	err := database.Query(ctx, conn, func(row *sql.Rows) error {
		return nil
	}, deleteRoleSQL, roleID)
	if err != nil {
		return err
	}
	return nil
}

// DeleteRolePermission removes the association between a role and a permission.
func DeleteRolePermission(ctx context.Context, conn *sql.Conn, roleID, permissionID int64) error {
	tx, err := conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelDefault})
	if err != nil {
		return err
	}
	defer tx.Rollback()

	err = database.TransactionExec(ctx, tx, deleteRolePermissionSQL, roleID, permissionID)
	if err != nil {
		return err
	}

	err = database.TransactionExec(ctx, tx, deletePermissionRecursiveSQL, roleID, permissionID)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	tx.Commit()
	return nil
}

// CreateRolePermission associates a permission with a role.
func CreateRolePermission(ctx context.Context, conn *sql.Conn, roleID, permissionID int64) error {
	var res sql.Result
	var err error
	if res, err = conn.ExecContext(ctx, insertRolePermissionSQL, roleID, permissionID); err != nil {
		return err
	}
	if rows, err := res.RowsAffected(); err != nil || rows == 0 {
		return errors.New("no role permission inserted")
	}
	return nil
}
