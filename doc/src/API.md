# Server Endpoints

Documentation of the REST API. Implementation started at <https://bitbucket.org/intevation/ball-lightning/src/master/pkg/controllers/routes.go#lines-149>.

- [Server Endpoints](#server-endpoints)
  - [/api/auth/login](#apiauthlogin)
    - [POST - Authenticate with user credentials](#post---authenticate-with-user-credentials)
  - [/api/auth/logout](#apiauthlogout)
    - [GET - Performs a logout for the current user](#get---performs-a-logout-for-the-current-user)
  - [/user](#user)
    - [GET - Returns the currently logged in user](#get---returns-the-currently-logged-in-user)
    - [PUT - Update the currently logged in user](#put---update-the-currently-logged-in-user)
    - [POST - Create a new User](#post---create-a-new-user)
  - [/user/`{id:[1-9]+}`](#userid1-9)
    - [GET - Returns the user specified by the URL path part `id`](#get---returns-the-user-specified-by-the-url-path-part-id)
    - [DELETE - Removes the user specified by the URL path part `id`](#delete---removes-the-user-specified-by-the-url-path-part-id)
  - [/user/`{id:[0-9]+}`/profile](#userid0-9profile)
    - [PUT - Update user profile](#put---update-user-profile)
  - [/user/`{id:[0-9]+}`/alarmsettings](#userid0-9alarmsettings)
    - [GET - Get alarmsettings for a user spcified by the URL path part `id`](#get---get-alarmsettings-for-a-user-spcified-by-the-url-path-part-id)
    - [PUT - Update alarmsettings of a user specified by the URL path part `id`](#put---update-alarmsettings-of-a-user-specified-by-the-url-path-part-id)
  - [/user/{id:[0-9]+}/alarm](#userid0-9alarm)
    - [GET](#get)
    - [PUT](#put)
  - [/user/{id:[0-9]+}/alarm/{aid:[0-9]+}](#userid0-9alarmaid0-9)
    - [GET](#get-1)
    - [DELETE](#delete)
  - [/group](#group)
    - [GET](#get-2)
    - [POST](#post)
    - [PUT](#put-1)
  - [/group/{id:[0-9]+}](#groupid0-9)
    - [GET](#get-3)
    - [DELETE](#delete-1)
  - [/group/{id:[0-9]+}/settings](#groupid0-9settings)
    - [GET](#get-4)
    - [PUT](#put-2)
  - [/group/{id:[0-9]+}/strokearea](#groupid0-9strokearea)
    - [GET](#get-5)
    - [PUT](#put-3)
    - [POST](#post-1)
  - [/group/{id:[0-9]+}/alarm](#groupid0-9alarm)
    - [GET](#get-6)
    - [POST](#post-2)
    - [PUT](#put-4)
  - [/group/{id:[0-9]+}/alarm/{aid:[0-9]+}](#groupid0-9alarmaid0-9)
    - [GET](#get-7)
    - [DELETE](#delete-2)
  - [/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area](#groupid0-9alarmaid0-9area)
    - [GET](#get-8)
    - [PUT](#put-5)
    - [POST](#post-3)
  - [/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area/{aaid:[0-9]+}](#groupid0-9alarmaid0-9areaaaid0-9)
    - [GET](#get-9)
    - [DELETE](#delete-3)
  - [/role](#role)
    - [GET](#get-10)
    - [POST](#post-4)
    - [PUT](#put-6)
  - [/role/{id:[0-9]+}](#roleid0-9)
    - [GET](#get-11)
    - [DELETE](#delete-4)
  - [/permission](#permission)
    - [GET](#get-12)
  - [/permission/{pid:[0-9]+}](#permissionpid0-9)
    - [GET](#get-13)
  - [/role/{id:[0-9]+}/permission](#roleid0-9permission)
    - [GET](#get-14)
    - [PUT](#put-7)
  - [/role/{id:[0-9]+}/permission/{pid:[0-9]+}](#roleid0-9permissionpid0-9)
    - [GET](#get-15)
    - [DELETE](#delete-5)
  - [/layer](#layer)
    - [GET](#get-16)
    - [PUT](#put-8)
    - [POST](#post-5)
  - [/layer/{id:[0-9]+}](#layerid0-9)
    - [GET](#get-17)
    - [DELETE](#delete-6)
  - [/group/{id:[0-9]+}/maplayer](#groupid0-9maplayer)
    - [GET](#get-18)
    - [PUT](#put-9)
    - [POST](#post-6)
  - [/group/{id:[0-9]+}/maplayer/{lid:[0-9]+}](#groupid0-9maplayerlid0-9)
    - [GET](#get-19)
    - [DELETE](#delete-7)
  - [/statistic](#statistic)
    - [GET](#get-20)
  - [/template/fax](#templatefax)
    - [GET](#get-21)
  - [/template/mail](#templatemail)
    - [GET](#get-22)
  - [/template/mqtt](#templatemqtt)
    - [GET](#get-23)
  - [/template/phone](#templatephone)
    - [GET](#get-24)
  - [/template/sms](#templatesms)
    - [GET](#get-25)
  - [/userlog](#userlog)
    - [GET](#get-26)
  - [/version](#version)
    - [GET](#get-27)
  - [/hosttheme](#hosttheme)
    - [GET](#get-28)
  - [/theme](#theme)
    - [GET](#get-29)

## /api/auth/login

### POST - Authenticate with user credentials

<details>
<summary>Request</summary>

```shell
curl --request POST 'http://localhost:8080/api/auth/login' \
--data-raw '{"name":"lisa","password":"XXXXXX"}'
```

</details>

<details>
<summary>Request Payload</summary>

```json
{
    "name": "lisa",
    "password": "secret"
}
```

</details>
<details>
<summary>Response payload</summary>

The response contains information about settings, visible area and permissions.

```json
{
    "User": {
        "id": 4,
        "name": "lisa",
        "password": null,
        "groupId": 2,
        "startDate": {
            "Time": "2020-01-27T15:08:53.158398Z",
            "Valid": true
        },
        "stopDate": {
            "Time": "2021-01-27T15:08:53.158398Z",
            "Valid": true
        },
        "firstName": "Lisa",
        "lastName": "Simpson",
        "company": "",
        "division": "",
        "street": "",
        "zip": "",
        "city": "",
        "phonenumber": "",
        "annotation": "",
        "mobilenumber": "",
        "email": ""
    },
    "UserActivities": {
        "roles": {
            "3": {
                "id": 3,
                "parent": 0,
                "name": "Alarmadmin",
                "permissions": [
                    0,
                    1
                ]
            }
        },
        "permissions": [
            "0": {
                "id": 0,
                "name": "login",
                "activities": [
                    0,
                    22
                ]
            },
            "1": {
                "id": 1,
                "name": "edit users",
                "activities": [
                    2,
                    3,
                    4
                ]
            },
            "2": {
                "id": 2,
                "name": "list users",
                "activities": [
                    14,
                    1
                ]
            }
        },
        "activities": [
            "0": {
                "id": 0,
                "name": "login",
                "method": "POST",
                "url": "/login"
            },
            "1": {
                "id": 1,
                "name": "get user",
                "method": "GET",
                "url": "/user"
            },
            "14": {
                "id": 14,
                "name": "get user id",
                "method": "GET",
                "url": "/user/{id:[0-9]+}"
            },
            "2": {
                "id": 2,
                "name": "create user",
                "method": "POST",
                "url": "/user"
            },
            "22": {
                "id": 22,
                "name": "update profile",
                "method": "PUT",
                "url": "/user/{id:[0-9]+}/profile"
            },
            "3": {
                "id": 3,
                "name": "update user",
                "method": "PUT",
                "url": "/user"
            },
            "4": {
                "id": 4,
                "name": "delete user",
                "method": "DELETE",
                "url": "/user/{id:[0-9]+}"
            }
        }
    },
    "theme": null,
    "profile": null,
    "strokeArea": "{\"type\":\"Polygon\",\"coordinates\":[[[5.61375,53.98875],[13.98375,53.98875],[13.98375,47.95875],[5.68875,48.01875],[5.61375,53.98875]]]}",
    "settings
        "id": 2,
        "groupId": 2,
        "maxDisplayTime": 10,
        "archiveDays": 365,
        "archiveSince": {
            "Time": "2010-01-01T00:00:00Z",
            "Status": 2,
            "InfinityModifier": 0
        },
        "maxZoom": 14,
        "minZoom": 6,
        "liveBlids": false,
        "animation": false,
        "sound": true,
        "path": "1",
        "maxAlarms": 1,
        "maxAlarmAreas": 100,
        "maxAlarmUser": 100,
        "statisticWindowed": true
    }
}
```

</details>

<details>
<summary>Status Code</summary>
200
</details>

<details>
<summary>Response Headers</summary>

```text
HTTP/1.1 200 OK
X-Powered-By: Express
content-type: application/json
set-cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX; Path=/; HttpOnly
x-content-type-options: nosniff
date: Thu, 30 Jan 2020 14:51:11 GMT
connection: close
transfer-encoding: chunked
```

</details>

## /api/auth/logout

### GET - Performs a logout for the current user

<details>
<summary>Request</summary>

```shell
curl --request POST 'http://localhost:8080/api/auth/logout'
```

</details>

<details>
<summary>Request headers</summary>

```text
Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

</details>

<details>
<summary>Response payload</summary>

```json
{}
```

</details>

## /user

### GET - Returns the currently logged in user

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/user' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response Payload</summary>

```json
{
  "id": 4,
  "name": "lisa",
  "password": null,
  "groupId": 2,
  "startDate": {
      "Time": "2020-01-27T15:08:53.158398Z",
      "Valid": true
  },
  "stopDate": {
      "Time": "2021-01-27T15:08:53.158398Z",
      "Valid": true
  },
  "firstName": "Lisa",
  "lastName": "Simpson",
  "company": "",
  "division": "",
  "street": "",
  "zip": "",
  "city": "",
  "phonenumber": "",
  "annotation": "",
  "mobilenumber": "",
  "email": ""
}
```

</details>

### PUT - Update the currently logged in user

<details>
<summary>Request</summary>

```shell
curl --request PUT 'http://localhost:8080/api/user' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @user.json
```

</details>

<details>
<summary>Request payload</summary>
The password can not be updated using PUT.

```json
{
    "id": 4,
    "name": "lisa",
    "password": null,
    "groupId": 2,
    "startDate": {
        "Time": "2020-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "stopDate": {
        "Time": "2021-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "firstName": "Lisa",
    "lastName": "Simpson",
    "company": "",
    "division": "",
    "street": "",
    "zip": "",
    "city": "",
    "phonenumber": "",
    "annotation": "",
    "mobilenumber": "",
    "email": ""
}
```

</details>
<details>
<summary>Response payload</summary>

```json
{
    "id": 4
}
```

</details>

### POST - Create a new User

<details>
<summary>Request</summary>

```shell
curl --request POST 'http://localhost:8080/api/user' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @user.json
```

</details>

<details>
<summary>Request payload</summary>
The password can not be set using `POST`.

```json
{
    "name": "lisa",
    "password": null,
    "groupId": 2,
    "startDate": {
        "Time": "2020-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "stopDate": {
        "Time": "2021-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "firstName": "Lisa",
    "lastName": "Simpson",
    "company": "",
    "division": "",
    "street": "",
    "zip": "",
    "city": "",
    "phonenumber": "",
    "annotation": "",
    "mobilenumber": "",
    "email": ""
}
```

</details>
<details>
<summary>Response payload</summary>
Returns the new id for the created user.

```json
{
    "id": 28
}
```

</details>

## /user/`{id:[1-9]+}`

### GET - Returns the user specified by the URL path part `id`

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/user/30' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response payload</summary>

```json
{
    "id": 4,
    "name": "lisa",
    "password": null,
    "groupId": 2,
    "startDate": {
        "Time": "2020-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "stopDate": {
        "Time": "2021-01-27T15:08:53.158398Z",
        "Valid": true
    },
    "firstName": "Lisa",
    "lastName": "Simpson",
    "company": "",
    "division": "",
    "street": "",
    "zip": "",
    "city": "",
    "phonenumber": "",
    "annotation": "",
    "mobilenumber": "",
    "email": ""
}
```

</details>

### DELETE - Removes the user specified by the URL path part `id`

<details>
<summary>Request</summary>

```shell
curl --request DELETE 'http://localhost:8080/api/user/30' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response payload</summary>

```json
{}
```

</details>

## /user/`{id:[0-9]+}`/profile

### PUT - Update user profile

- Note: Every user can update his/her own profile only!
- Important: The profile must exist before it can be updated!
- The request payload can be any valid json.

<details>
<summary>Request</summary>

```shell
curl --request PUT 'http://localhost:8080/api/user/4/profile' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @profile.json
```

</details>

<details>
<summary>Request payload</summary>

```json
{
    "layers": ["https://maps.dwd.de/geoserver/dwd/ows?service=WMS&version=1.3&request=GetMap&layers=dwd:Warnungen_Landkreise&bbox=6.15,51.76,14.90,55.01&width=512&height=418&srs=EPSG:4326&format=image%2Fjpeg&CQL_FILTER=EC_II%20IN%20('51','52'"]
}
```

</details>

<details>
<summary>Response payload</summary>

```json
{
    "id": 4
}
```

</details>

## /user/`{id:[0-9]+}`/alarmsettings

### GET - Get alarmsettings for a user spcified by the URL path part `id`

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/user/4/alarmsettings' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response payload</summary>

```json
{
    "id": 4,
    "alarmTemplateMailId": 1,
    "alarmTemplateSmsId": null,
    "alarmTemplateMqttId": null,
    "alarmTemplateFaxId": null,
    "sms": "",
    "email": "lisa@example.org",
    "phone": "555-444-333",
    "fax": "",
    "startDate": {
        "Time": "2020-01-01T00:00:00Z",
        "Status": 2,
        "InfinityModifier": 0
    },
    "stopDate": {
        "Time": "2020-02-28T13:49:53.540387Z",
        "Status": 2,
        "InfinityModifier": 0
    },
    "startTime": "0000-01-01T07:00:00Z",
    "stopTime": "0000-01-01T22:00:00Z",
    "monday": true,
    "tuesday": true,
    "wednesday": true,
    "thursday": true,
    "friday": true,
    "saturday": true,
    "sunday": true,
    "onlyLog": false,
    "strokeProtocol": false,
    "strokeAsCSV": false
}
```

</details>

### PUT - Update alarmsettings of a user specified by the URL path part `id`

<details>
<summary>Request</summary>

```shell
curl --request PUT 'http://localhost:8080/api/user/4/alarmsettings' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @alarmsettings.json
```

</details>

<details>
<summary>Request payload</summary>

```json
{
    "id": 4,
    "alarmTemplateMailId": 1,
    "alarmTemplateSmsId": null,
    "alarmTemplateMqttId": null,
    "alarmTemplateFaxId": null,
    "sms": "",
    "email": "lisa@example.org",
    "phone": "555-444-333",
    "fax": "",
    "startDate": {
        "Time": "2020-01-01T00:00:00Z",
        "Status": 2,
        "InfinityModifier": 0
    },
    "stopDate": {
        "Time": "2020-02-28T13:49:53.540387Z",
        "Status": 2,
        "InfinityModifier": 0
    },
    "startTime": "0000-01-01T07:00:00Z",
    "stopTime": "0000-01-01T22:00:00Z",
    "monday": true,
    "tuesday": true,
    "wednesday": true,
    "thursday": true,
    "friday": true,
    "saturday": true,
    "sunday": true,
    "onlyLog": false,
    "strokeProtocol": false,
    "strokeAsCSV": false
}
```

</details>

<details>
<summary>Response</summary>

```json
{"id":4}
```

</details>

## /user/{id:[0-9]+}/alarm

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/user/4/alarm' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 1,
        "isExtended": true,
        "isNotified": false,
        "name": "Alle Alarme Ruhrgebiet"
    },
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 3,
        "isExtended": false,
        "isNotified": false,
        "name": "Wuppertal"
    },
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 4,
        "isExtended": false,
        "isNotified": false,
        "name": "Dortmund Umkreis"
    }
]
```

</details>

### PUT

<details>
<summary>Request</summary>

```shell
curl --request PUT 'http://localhost:8080/api/user/4/alarm' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @alarm.json
```

</details>

<details>
<summary>Request payload</summary>
Note: You must put an existing alarm object.

```json
{
    "alarm": false,
    "alarmTimestamp": null,
    "groupId": 4,
    "id": 1,
    "isExtended": true,
    "isNotified": false,
    "name": "Alle Alarme Ruhrgebiet"
}
```

</details>

<details>
<summary>Response</summary>

```json
TODO
```

</details>

## /user/{id:[0-9]+}/alarm/{aid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/user/4/alarm/4' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "alarm": false,
    "alarmTimestamp": null,
    "groupId": 4,
    "id": 4,
    "isExtended": false,
    "isNotified": false,
    "name": "Dortmund Umkreis"
}
```

</details>


### DELETE

<details>
<summary>Request</summary>

```shell
curl --request DELETE 'http://localhost:8080/api/user/4/alarm/4' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
{}
```

</details>


## /group

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
{
    "alarmConfigId": null,
    "changeAlarmConfig": false,
    "dataAreaId": 1,
    "id": 4,
    "maxAlarmArea": 30000,
    "name": "Alarme West",
    "parent": 2,
    "roleId": 4,
    "themeId": 3
}
```

</details>

### POST

<details>
<summary>Request</summary>

```shell
curl --request POST 'http://localhost:8080/api/group' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @group.json
```

</details>

<details>
<summary>Request payload</summary>

```json
{
    "alarmConfigId": null,
    "changeAlarmConfig": false,
    "dataAreaId": 1,
    "id": 4,
    "maxAlarmArea": 30000,
    "name": "ALARME TEST TEST",
    "parent": 2,
    "roleId": 4,
    "themeId": 3
}
```

</details>

<details>
<summary>Request payload</summary>

```json
{"id":14}
```

</details>


### PUT

<details>
<summary>Request</summary>

```shell
curl --request PUT 'http://localhost:8080/api/group' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' -d @group.json
```

</details>


<details>
<summary>Request payload</summary>

```json
{
    "alarmConfigId": null,
    "changeAlarmConfig": false,
    "dataAreaId": 1,
    "id": 4,
    "maxAlarmArea": 30000,
    "name": "ALARME TEST TEST TESSSSSSSSSSSSSSSSSSSSSST",
    "parent": 2,
    "roleId": 4,
    "themeId": 3
}
```

</details>


<details>
<summary>Response</summary>

```json
{"id":4}
```

</details>

## /group/{id:[0-9]+}

Note: You only have permission on your groups!

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "children": null,
    "group": {
        "alarmConfigId": null,
        "changeAlarmConfig": false,
        "dataAreaId": 1,
        "id": 4,
        "maxAlarmArea": 30000,
        "name": "Alarme West",
        "parent": 2,
        "roleId": 4,
        "themeId": 3
    },
    "users": [
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Homer",
            "groupId": 4,
            "id": 3,
            "lastName": "Simpson",
            "mobilenumber": "",
            "name": "homer",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Bart",
            "groupId": 4,
            "id": 6,
            "lastName": "Simpson",
            "mobilenumber": "",
            "name": "bart",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Abraham Jebediah",
            "groupId": 4,
            "id": 8,
            "lastName": "Simpson",
            "mobilenumber": "",
            "name": "abe",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Patty",
            "groupId": 4,
            "id": 9,
            "lastName": "Bouvier",
            "mobilenumber": "",
            "name": "patty",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Selma",
            "groupId": 4,
            "id": 10,
            "lastName": "Bouvier-Terwilliger-Hutz-McClure-Stu-Simpson",
            "mobilenumber": "",
            "name": "selma",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Herschel Shmoikel Pinkus Yerucham",
            "groupId": 4,
            "id": 15,
            "lastName": "Krustofski",
            "mobilenumber": "",
            "name": "krusty",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Apu",
            "groupId": 4,
            "id": 16,
            "lastName": "Nahasapeemapetilon",
            "mobilenumber": "",
            "name": "apu",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        },
        {
            "annotation": "",
            "city": "",
            "company": "",
            "division": "",
            "email": "",
            "firstName": "Nelson",
            "groupId": 4,
            "id": 21,
            "lastName": "Muntz",
            "mobilenumber": "",
            "name": "nelson",
            "password": null,
            "phonenumber": "",
            "startDate": {
                "Time": "2020-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "stopDate": {
                "Time": "2021-03-17T11:30:57.328108Z",
                "Valid": true
            },
            "street": "",
            "zip": ""
        }
    ]
}
```

</details>

### DELETE

<details>
<summary>Request</summary>
Note: You can only delete empty groups.

```shell
curl --request DELETE 'http://localhost:8080/api/group/14' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>
Note: 200 OK

```text
null
```

</details>

## /group/{id:[0-9]+}/settings

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/settings' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "animation": false,
    "archiveDays": 100,
    "archiveSince": {
        "InfinityModifier": 0,
        "Status": 2,
        "Time": "2020-01-01T00:00:00Z"
    },
    "groupId": 4,
    "id": 4,
    "liveBlids": false,
    "maxAlarmAreas": 1,
    "maxAlarmUser": 1,
    "maxAlarms": 1,
    "maxDisplayTime": 10,
    "maxZoom": 14,
    "minZoom": 6,
    "path": "1",
    "sound": true,
    "statisticWindowed": true
}
```

</details>

### PUT

TODO

## /group/{id:[0-9]+}/strokearea

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/strokearea' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "geom": "{\"type\":\"Polygon\",\"coordinates\":[[[5.61375,53.98875],[13.98375,53.98875],[13.98375,47.95875],[5.61375,47.95875],[5.61375,53.98875]]]}",
    "groupId": 4,
    "id": 5
}
```

</details>

### PUT

TODO

### POST

TODO

## /group/{id:[0-9]+}/alarm

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/alarm' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
[
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 1,
        "isExtended": true,
        "isNotified": false,
        "name": "Alle Alarme Ruhrgebiet"
    },
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 3,
        "isExtended": false,
        "isNotified": false,
        "name": "Wuppertal"
    },
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 4,
        "isExtended": false,
        "isNotified": false,
        "name": "Dortmund Umkreis"
    },
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 5,
        "isExtended": false,
        "isNotified": false,
        "name": "Bonn"
    }
]
```

</details>

### POST

TODO

### PUT

TODO

## /group/{id:[0-9]+}/alarm/{aid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/alarm/1' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarm": false,
        "alarmTimestamp": null,
        "groupId": 4,
        "id": 1,
        "isExtended": true,
        "isNotified": false,
        "name": "Alle Alarme Ruhrgebiet"
    }
]
```

</details>

### DELETE

Note: The area must be delete before!

TODO

## /group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/alarm/1/area' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarmDuration": 1,
        "area": 5174064.88584423,
        "center": null,
        "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[7.135675958,51.253673218],[7.154176155,51.25784483],[7.161068385,51.249864353],[7.162338007,51.242065251],[7.155083027,51.234447523],[7.145288806,51.230094535],[7.134406337,51.231545531],[7.127876856,51.243516247],[7.135675958,51.253673218]]]]}",
        "icon": null,
        "id": 1,
        "innerRadius": null,
        "lastStroke": null,
        "radius": 0,
        "strokeCounter": 0,
        "strokeThreshold": null
    },
    {
        "alarmDuration": 5,
        "area": 364718826.904678,
        "center": null,
        "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[7.107925663,51.155277563],[7.198612902,51.156728559],[7.224912201,50.967736353],[6.984409644,50.969912847],[6.94414451,51.164709036],[7.107925663,51.155277563]]]]}",
        "icon": null,
        "id": 2,
        "innerRadius": null,
        "lastStroke": null,
        "radius": 0,
        "strokeCounter": 0,
        "strokeThreshold": null
    },
    {
        "alarmDuration": 10,
        "area": 843182.076911807,
        "center": null,
        "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[6.877370362,51.466799565],[6.877285343,51.468210885],[6.880618099,51.46880602],[6.885260152,51.471084537],[6.886382407,51.471390606],[6.886450422,51.470574421],[6.880431057,51.468448939],[6.878611644,51.467411704],[6.877370362,51.466799565]]],[[[6.899373354,51.473159007],[6.894782312,51.472750915],[6.892928892,51.471509633],[6.896482698,51.471747687],[6.900002496,51.472308815],[6.909031545,51.473686127],[6.910884965,51.476015655],[6.913996671,51.477750049],[6.908963529,51.477682033],[6.904151437,51.475539547],[6.900138527,51.473397061],[6.899373354,51.473159007]]],[[[6.93268391,51.460559149],[6.926715556,51.456138146],[6.933296049,51.452244263],[6.938108141,51.454675815],[6.940284634,51.458654717],[6.93268391,51.460559149]]]]}",
        "icon": null,
        "id": 3,
        "innerRadius": null,
        "lastStroke": null,
        "radius": 0,
        "strokeCounter": 0,
        "strokeThreshold": null
    },
    {
        "alarmDuration": 180,
        "area": 20548305.4895859,
        "center": null,
        "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[7.142630536,51.599004555],[7.126306833,51.56798952],[7.173101448,51.553298187],[7.208469471,51.585401469],[7.185072164,51.60607816],[7.142630536,51.599004555]]]]}",
        "icon": null,
        "id": 4,
        "innerRadius": null,
        "lastStroke": null,
        "radius": 0,
        "strokeCounter": 0,
        "strokeThreshold": null
    },
    {
        "alarmDuration": 1,
        "area": 85185861.7175159,
        "center": null,
        "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[7.395375871,51.550305508],[7.539568581,51.563092409],[7.590988245,51.496165226],[7.576568974,51.440120513],[7.449244091,51.425701242],[7.375243304,51.468686993],[7.370618255,51.514665423],[7.395375871,51.550305508]],[[7.510457977,51.547312829],[7.410339265,51.537246546],[7.389390513,51.511944806],[7.394287624,51.486371005],[7.420949672,51.460525141],[7.447067597,51.445561747],[7.485156237,51.440936698],[7.528141988,51.457260401],[7.548546617,51.46977524],[7.519708075,51.496981411],[7.517803643,51.50949625],[7.562421765,51.527724385],[7.510457977,51.547312829]]]]}",
        "icon": null,
        "id": 5,
        "innerRadius": null,
        "lastStroke": null,
        "radius": 0,
        "strokeCounter": 0,
        "strokeThreshold": null
    }
]
```

</details>

### PUT

TODO

### POST

TODO

## /group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area/{aaid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/alarm/1/area/1' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
{
    "alarmDuration": 1,
    "area": 5174064.88584423,
    "center": null,
    "geom": "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[7.135675958,51.253673218],[7.154176155,51.25784483],[7.161068385,51.249864353],[7.162338007,51.242065251],[7.155083027,51.234447523],[7.145288806,51.230094535],[7.134406337,51.231545531],[7.127876856,51.243516247],[7.135675958,51.253673218]]]]}",
    "icon": null,
    "id": 1,
    "innerRadius": null,
    "lastStroke": null,
    "radius": 0,
    "strokeCounter": 0,
    "strokeThreshold": null
}
```

</details>

### DELETE

<details>
<summary>Request</summary>

```shell
curl --request DELETE 'http://localhost:8080/api/group/4/alarm/1/area/1' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```text
null
```

</details>


## /role

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/role' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "id": 3,
        "name": "Alarmadmin",
        "parent": 1
    },
    {
        "id": 4,
        "name": "Alarmuser",
        "parent": 3
    },
    {
        "id": 6,
        "name": "Alarmonlyuser",
        "parent": 4
    }
]
```

</details>
### POST

TODO

### PUT

TODO

## /role/{id:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/role/3' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

```

</details>

<details>
<summary>Response</summary>

```json
{
    "id": 3,
    "name": "Alarmadmin",
    "parent": 1
}
```

</details>

### DELETE

TODO

## /permission

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/permission ' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "id": 0,
        "name": "login"
    },
    {
        "id": 1,
        "name": "edit users"
    },
    {
        "id": 2,
        "name": "list users"
    },
    {
        "id": 3,
        "name": "edit groups"
    },
    {
        "id": 4,
        "name": "list groups"
    },
    {
        "id": 5,
        "name": "edit roles"
    },
    {
        "id": 6,
        "name": "list roles"
    },
    {
        "id": 7,
        "name": "edit alarms"
    },
    {
        "id": 8,
        "name": "list alarms"
    },
    {
        "id": 9,
        "name": "edit layers"
    },
    {
        "id": 10,
        "name": "list layers"
    },
    {
        "id": 11,
        "name": "statistic"
    },
    {
        "id": 12,
        "name": "strokestream"
    },
    {
        "id": 13,
        "name": "alarmstream"
    },
    {
        "id": 14,
        "name": "userlog"
    }
]
```

</details>


## /permission/{pid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/permission/4' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "id": 4,
    "name": "list groups"
}
```

</details>

## /role/{id:[0-9]+}/permission

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/role/3/permission' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
[
    {
        "id": 0,
        "name": "login"
    },
    {
        "id": 1,
        "name": "edit users"
    },
    {
        "id": 2,
        "name": "list users"
    },
    {
        "id": 3,
        "name": "edit groups"
    },
    {
        "id": 4,
        "name": "list groups"
    },
    {
        "id": 5,
        "name": "edit roles"
    },
    {
        "id": 6,
        "name": "list roles"
    },
    {
        "id": 7,
        "name": "edit alarms"
    },
    {
        "id": 8,
        "name": "list alarms"
    },
    {
        "id": 9,
        "name": "edit layers"
    },
    {
        "id": 10,
        "name": "list layers"
    },
    {
        "id": 11,
        "name": "statistic"
    },
    {
        "id": 12,
        "name": "strokestream"
    },
    {
        "id": 13,
        "name": "alarmstream"
    },
    {
        "id": 14,
        "name": "userlog"
    }
]
```

</details>


### PUT

TODO

## /role/{id:[0-9]+}/permission/{pid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/role/3/permission/1' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
{
    "id": 1,
    "name": "edit users"
}
```

</details>

### DELETE

TODO

## /layer

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/layer' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "baseLayer": true,
        "defaultName": "Google Satellite",
        "externalParameter": null,
        "externalServer": null,
        "geoserverName": "google_sat",
        "id": 1,
        "layerType": "google",
        "name": "google_sat",
        "reload": false,
        "singleTile": false,
        "tickWidth": null,
        "timeLayer": false
    },
    {
        "baseLayer": false,
        "defaultName": "IR_WMST_euro",
        "externalParameter": {
            "format": "image/png",
            "layers": "IR_WMST_euro",
            "transparent": true
        },
        "externalServer": "https://geoserver.blids.net/geoserver/wms?",
        "geoserverName": "IR_WMST_euro",
        "id": 2,
        "layerType": "wmst",
        "name": "IR_WMST_euro",
        "reload": false,
        "singleTile": false,
        "tickWidth": 1800000,
        "timeLayer": true
    },
    {
        "baseLayer": false,
        "defaultName": "wms-T Blitze",
        "externalParameter": {
            "format": "image/png",
            "layers": "blitze:blitzewmst",
            "transparent": true
        },
        "externalServer": "https://geoserver.blids.net/geoserver/wms?",
        "geoserverName": "blitze:blitzewmst",
        "id": 3,
        "layerType": "wmst",
        "name": "blitze:blitzewmst",
        "reload": false,
        "singleTile": false,
        "tickWidth": null,
        "timeLayer": true
    }
]
```

</details>

### PUT

TODO

### POST

TODO

## /layer/{id:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/layer/2' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "baseLayer": false,
    "defaultName": "IR_WMST_euro",
    "externalParameter": {
        "format": "image/png",
        "layers": "IR_WMST_euro",
        "transparent": true
    },
    "externalServer": "https://geoserver.blids.net/geoserver/wms?",
    "geoserverName": "IR_WMST_euro",
    "id": 2,
    "layerType": "wmst",
    "name": "IR_WMST_euro",
    "reload": false,
    "singleTile": false,
    "tickWidth": 1800000,
    "timeLayer": true
}
```

</details>

### DELETE

TODO

## /group/{id:[0-9]+}/maplayer

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/maplayer' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "checkedOnLogin": true,
        "cqlFilter": null,
        "displayName": "Google Satellite",
        "featureInfo": false,
        "groupId": 4,
        "id": 8,
        "layerID": 1,
        "permanent": false,
        "popupTemplate": null,
        "reloadTime": -1,
        "zIndex": null
    },
    {
        "checkedOnLogin": true,
        "cqlFilter": null,
        "displayName": "Blitze",
        "featureInfo": false,
        "groupId": 4,
        "id": 9,
        "layerID": 3,
        "permanent": false,
        "popupTemplate": null,
        "reloadTime": -1,
        "zIndex": null
    }
]
```

</details>
### PUT

TODO

### POST

TODO

## /group/{id:[0-9]+}/maplayer/{lid:[0-9]+}

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/group/4/maplayer/9' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
{
    "checkedOnLogin": true,
    "cqlFilter": null,
    "displayName": "Blitze",
    "featureInfo": false,
    "groupId": 4,
    "id": 9,
    "layerID": 3,
    "permanent": false,
    "popupTemplate": null,
    "reloadTime": -1,
    "zIndex": null
}
```

</details>

### DELETE

<details>
<summary>Request</summary>

```shell
curl --request DELETE 'http://localhost:8080/api/group/4/maplayer/9' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```text
null
```

</details>


## /statistic

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/statistic?start=86418&end=18&left=5.61375&top=53.98875&right=13.98375&bottom=47.9587' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
TOO BIG!
```

</details>

## /template/fax

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/template/fax' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarmSubject": "Ball-Lightning: Alarm",
        "alarmText": "%TYP (%DATUM %ZEIT) : %BREITE\u00b0N, %L\u00c4NGE\u00b0O schlug in %GEBIET ein. Alarm f\u00fcr %GRUPPE, Blitzdichte %DICHTE, %ANZAHL Blitze",
        "allClearSubject": "Ball-Lightning: Entwarnung",
        "allClearText": "Ball-Lightning: Entwarnung nach %MINUTEN Minuten f\u00fcr %GEBIET (%L\u00c4NGE\u00b0O, %BREITE\u00b0N)",
        "id": 1
    },
    {
        "alarmSubject": "Blitz-Alarm",
        "alarmText": "%GEBIET: Gewitter mit Blitzen seit %ZEIT",
        "allClearSubject": "Blitz-Entwarnung",
        "allClearText": "Der Alarm f\u00fcr %GEBIET lief aus, es wurden keine weiteren Blitze registriert.",
        "id": 2
    }
]
```

</details>

## /template/mail

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/template/mail' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarmSubject": "Ball-Lightning: Alarm",
        "alarmText": "%TYP (%DATUM %ZEIT) : %BREITE\u00b0N, %L\u00c4NGE\u00b0O schlug in %GEBIET ein. Alarm f\u00fcr %GRUPPE, Blitzdichte %DICHTE, %ANZAHL Blitze",
        "allClearSubject": "Ball-Lightning: Entwarnung",
        "allClearText": "Ball-Lightning: Entwarnung nach %MINUTEN Minuten f\u00fcr %GEBIET (%L\u00c4NGE\u00b0O, %BREITE\u00b0N)",
        "id": 1
    },
    {
        "alarmSubject": "Blitz-Alarm",
        "alarmText": "%GEBIET: Gewitter mit Blitzen seit %ZEIT",
        "allClearSubject": "Blitz-Entwarnung",
        "allClearText": "Der Alarm f\u00fcr %GEBIET lief aus, es wurden keine weiteren Blitze registriert.",
        "id": 2
    }
]
```

</details>

## /template/mqtt

### GET


<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/template/mqtt' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[]
```

</details>


## /template/phone
### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/template/phone' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[]
```

</details>

## /template/sms

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/template/sms' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "alarmSubject": "BLIDS Alarm",
        "alarmText": "%TYP (%DATUM %ZEIT) : %BREITE\u00b0N, %L\u00c4NGE\u00b0O",
        "allClearSubject": "BLIDS Entwarnung",
        "allClearText": "%TYP (%DATUM %ZEIT) : %BREITE\u00b0N, %L\u00c4NGE\u00b0O",
        "id": 1
    }
]
```

</details>

## /userlog

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/userlog' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
[
    {
        "id": 1,
        "ip": "192.168.96.1",
        "text": "Login successful",
        "timestamp": {
            "InfinityModifier": 0,
            "Status": 2,
            "Time": "2020-03-17T11:36:16.908499Z"
        },
        "user": "lisa"
    },
    {
        "id": 2,
        "ip": "192.168.96.1",
        "text": "Login successful",
        "timestamp": {
            "InfinityModifier": 0,
            "Status": 2,
            "Time": "2020-03-17T11:40:26.569132Z"
        },
        "user": "lisa"
    },
    {
        "id": 3,
        "ip": "192.168.96.1",
        "text": "Login successful",
        "timestamp": {
            "InfinityModifier": 0,
            "Status": 2,
            "Time": "2020-03-17T12:29:51.740126Z"
        },
        "user": "lisa"
    }
]

```

</details>

## /version

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/version' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```text
{"version":"1c617ba"}
```

</details>

## /hosttheme

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/hosttheme' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>

<details>
<summary>Response</summary>

```json
{
    "links": [
        {
            "name": "cookie",
            "url": "https://www.siemens.de/cookie-policy"
        },
        {
            "name": "imprint",
            "url": "https://www.siemens.de/impressum"
        },
        {
            "name": "dataProtection",
            "url": "https://www.siemens.de/Datenschutz"
        },
        {
            "name": "termsOfUse",
            "url": "https://www.siemens.de/nutzungsbedingungen"
        }
    ],
    "logo": "Siemens-logo.svg",
    "theme": {
        "dark": false,
        "themes": {
            "dark": {
                "accent": "#82B1FF",
                "error": "#444444",
                "info": "#33b5e5",
                "primary": {
                    "base": "#009999",
                    "darken1": "#008888",
                    "lighten1": "#a0dcdc"
                },
                "secondary": "#8585F4",
                "success": "#00C851",
                "warning": "#ffbb33"
            },
            "light": {
                "accent": "#82B1FF",
                "error": "#444444",
                "info": "#33b5e5",
                "primary": {
                    "base": "#afafaf",
                    "darken1": "#008888",
                    "lighten1": "#a0dcdc"
                },
                "secondary": "#8585F4",
                "success": "#00C851",
                "warning": "#ffbb33"
            }
        }
    },
    "title": "Blids Live"
}
```

</details>

## /theme

### GET

<details>
<summary>Request</summary>

```shell
curl --request GET 'http://localhost:8080/api/theme' -H 'Cookie: BLIDS_SESSION=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

</details>


<details>
<summary>Response</summary>

```json
[
    {
        "data": {
            "theme": {
                "dark": false,
                "themes": {
                    "dark": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#009999",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    },
                    "light": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#afafaf",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    }
                }
            }
        },
        "id": 1,
        "name": "generic"
    },
    {
        "data": {
            "theme": {
                "dark": false,
                "themes": {
                    "dark": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#009999",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    },
                    "light": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#afafaf",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    }
                }
            }
        },
        "id": 2,
        "name": "Admin-Theme"
    },
    {
        "data": {
            "theme": {
                "dark": true,
                "themes": {
                    "dark": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#009999",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    },
                    "light": {
                        "accent": "#8fB1FF",
                        "error": "#4f4444",
                        "info": "#3fb5e5",
                        "primary": {
                            "base": "#afafff",
                            "darken1": "#0088f8",
                            "lighten1": "#a0dcfc"
                        },
                        "secondary": "#8f85F4",
                        "success": "#0fC851",
                        "warning": "#f0bb33"
                    }
                }
            }
        },
        "id": 3,
        "name": "Theme For Western Users"
    },
    {
        "data": {
            "theme": {
                "dark": false,
                "themes": {
                    "dark": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#009999",
                            "darken1": "#008888",
                            "lighten1": "#a0dcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    },
                    "light": {
                        "accent": "#82B1FF",
                        "error": "#444444",
                        "info": "#33b5e5",
                        "primary": {
                            "base": "#ffafaf",
                            "darken1": "#a08888",
                            "lighten1": "#aadcdc"
                        },
                        "secondary": "#8585F4",
                        "success": "#00C851",
                        "warning": "#ffbb33"
                    }
                }
            }
        },
        "id": 4,
        "name": "Theme For Eastern Users"
    }
]
```

</details>
