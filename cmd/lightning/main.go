// Copyright 2019 by Siemens AG. All rights reserved.
// Engineering by Intevation GmbH.
// Use of this source code is governed by the Apache 2.0 license
// that can be found in the LICENSE file.

package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"

	"bitbucket.org/intevation/ball-lightning/pkg/controllers"
)

func pathToWeb() string {
	web := os.Getenv("WEB")
	if web != "" {
		return web
	}
	path, err := os.Executable()
	if err == nil {
		return filepath.Join(filepath.Dir(path), "web")
	}
	return "web"
}

func env(key, def string) string {
	if value, found := os.LookupEnv(key); found {
		return value
	}
	return def
}

func main() {
	m := mux.NewRouter()
	if err := controllers.BindRoutes(m); err != nil {
		log.Fatalf("error: Binding routes failed: %v\n", err)
	}
	web := pathToWeb()
	dir := http.FileServer(http.Dir(web))

	xframes := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("X-Frame-Options", "sameorigin")
		rw.Header().Set("X-Content-Type-Options", "nosniff")
		dir.ServeHTTP(rw, r)
	})

	m.PathPrefix("/").Handler(xframes)

	controllers.StartStrokeStream()
	controllers.StartAlarmStream()
	port := env("PORT", "5000")
	log.Fatalf("error: %v\n", http.ListenAndServe(":"+port, m))
}
